/*
 * Copyright (C) 2008 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include <hardware_legacy/vibrator.h>
#include "qemu.h"

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate begin
#define ANDROID_IMMERSION_VIBRATOR
#define ANDROID_IMMERSION_DEFAULT_VIBRATOR_INDEX        0
#define ANDROID_IMMERSION_DEFAULT_VIBRATOR_MAGNITUDE    5000

#ifdef ANDROID_IMMERSION_VIBRATOR
#include <hardware_legacy/ImmVibe.h>

VibeInt32 g_hDevice = VIBE_INVALID_DEVICE_HANDLE_VALUE;
#endif
//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate end

#define THE_DEVICE "/sys/class/timed_output/vibrator/enable"

int vibrator_exists()
{

//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate begin
#ifdef ANDROID_IMMERSION_VIBRATOR
    return 1;
#else
//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate end

    int fd;

#ifdef QEMU_HARDWARE
    if (qemu_check()) {
        return 1;
    }
#endif

    fd = open(THE_DEVICE, O_RDWR);
    if(fd < 0)
        return 0;
    close(fd);
    return 1;

//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate begin
#endif
//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate end

}

static int sendit(int timeout_ms)
{
    int nwr, ret, fd;
    char value[20];

#ifdef QEMU_HARDWARE
    if (qemu_check()) {
        return qemu_control_command( "vibrator:%d", timeout_ms );
    }
#endif

    fd = open(THE_DEVICE, O_RDWR);
    if(fd < 0)
        return errno;

    nwr = sprintf(value, "%d\n", timeout_ms);
    ret = write(fd, value, nwr);

    close(fd);

    return (ret == nwr) ? 0 : -1;
}

int vibrator_on(int timeout_ms)
{
    printf("immersion : vibrator_on.\n");
//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate begin
#ifdef ANDROID_IMMERSION_VIBRATOR
    VibeStatus status = VIBE_S_SUCCESS;
    VibeInt32 hEffect = VIBE_INVALID_EFFECT_HANDLE_VALUE;

    if (g_hDevice == VIBE_INVALID_DEVICE_HANDLE_VALUE)
    {
        /* Initialize TSP API */
        status = ImmVibeInitialize(VIBE_CURRENT_VERSION_NUMBER);
        if(VIBE_FAILED(status))
        {
            /* Something unexpected happened. Returning error code */
            return (int)status;
        }

        /* Open device */
        status = ImmVibeOpenDevice(
                    ANDROID_IMMERSION_DEFAULT_VIBRATOR_INDEX,           /* nDeviceIndex */
                    &g_hDevice                                          /* *phDeviceHandle */
                    );

        if(VIBE_FAILED(status))
        {
            /* Something unexpected happened. Returning error code */
            ImmVibeTerminate();
            return (int)status;
        }
    }
    else
    {
        /* Already initialized */

        /* Stop any currently playing effect */
        ImmVibeStopAllPlayingEffects(g_hDevice);
    }

    /* Start MagSweep effect */
    ImmVibePlayMagSweepEffect(
            g_hDevice,                                      /* nDeviceHandle */
            timeout_ms,                                     /* nDuration */
            ANDROID_IMMERSION_DEFAULT_VIBRATOR_MAGNITUDE,   /* nMagnitude */
            VIBE_STYLE_STRONG,                              /* nStyle */
            0,                                              /* nAttackTime */
            0,                                              /* nAttackLevel */
            0,                                              /* nFadeTime */
            0,                                              /* nFadeLevel */
            &hEffect                                        /* nEffectHandle */
            );

    return 0;
#else
    /* constant on, up to maximum allowed time */
    return sendit(timeout_ms);
#endif
//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate end

}

int vibrator_off()
{

//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate begin
#ifdef ANDROID_IMMERSION_VIBRATOR
    /* Stop any currently playing effect */
    ImmVibeStopAllPlayingEffects(g_hDevice);
    return 0;
#else
    return sendit(0);
#endif
//Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate end

}
