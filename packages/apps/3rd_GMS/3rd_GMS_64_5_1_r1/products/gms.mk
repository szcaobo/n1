###############################################################################
# GMS mandatory core packages
PRODUCT_PACKAGES += \
    ConfigUpdater \
    GoogleBackupTransport \
    GoogleFeedback \
    GoogleLoginService \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    GoogleCalendarSyncAdapter \
    GoogleContactsSyncAdapter \
    GmsCore 
    

ifneq ($(findstring Phonesky,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Phonesky
endif


ifneq ($(findstring SetupWizard,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += SetupWizard
endif
PRODUCT_PACKAGES += \
    google.xml \
    google_generic_update.txt \
    com.google.android.maps.jar \
    com.google.android.media.effects.jar

# Overlay For GMS devices
PRODUCT_PACKAGE_OVERLAYS := packages/apps/3rd_GMS/3rd_GMS_$(GN3RD_GMS_SUPPORT)/products/gms_overlay

# Setting PRODUCT_PREBUILT_WEBVIEWCHROMIUM as yes will prevent from building
# webviewchromium from source, and use prebuilt WebViewGoogle.apk in GMS pack
PRODUCT_PREBUILT_WEBVIEWCHROMIUM := yes
#
ifeq ($(PRODUCT_PREBUILT_WEBVIEWCHROMIUM),yes)
PRODUCT_PACKAGES += WebViewGoogle
# The following framework overlay must be included if prebuilt WebViewGoogle.apk is used
PRODUCT_PACKAGE_OVERLAYS += packages/apps/3rd_GMS/3rd_GMS_$(GN3RD_GMS_SUPPORT)/apps/WebViewGoogle/overlay
endif

# Chrome browser selection
# By default, Chrome will be the only preloaded system browser
# Use ChromeWithBrowser, if Chrome is preloaded along with another browser side-by-sde
##PRODUCT_PACKAGES += Chrome
#PRODUCT_PACKAGES += ChromeWithBrowser
ifneq ($(findstring OnlyChrome,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Chrome
else
ifneq ($(findstring Chrome,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += ChromeWithBrowser
endif
endif

#
# Uncomment the following line if customized homepage provider for Chrome should be installed
# For the details, see Android.mk in apps/Chrome directory
#PRODUCT_PACKAGES += ChromeCustomizations

# GMS mandatory application packages
#PRODUCT_PACKAGES += \
#    Books \
#    Drive \
#    Gmail2 \
#    Hangouts \
#    Maps \
#    Music2 \
#    Newsstand \
#    PlayGames \
#    PlusOne \
#    Street \
#    Velvet \
#    Videos \
#    YouTube
#######################################
ifneq ($(findstring Velvet,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += \
    Velvet
endif

ifneq ($(findstring Books,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Books
endif

ifneq ($(findstring Drive,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Drive
endif

ifneq ($(findstring Gmail,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Gmail2
endif

ifneq ($(findstring Hangouts,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Hangouts
endif

ifneq ($(findstring Newsstand,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Newsstand
endif

ifneq ($(findstring Maps,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Maps
endif

ifneq ($(findstring Music2,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Music2
endif

ifneq ($(findstring PlayGames,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += PlayGames
endif

ifneq ($(findstring PlusOne,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += PlusOne
endif

ifneq ($(findstring Street,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Street
endif

ifneq ($(findstring Videos,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Videos
endif

ifneq ($(findstring YouTube,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += YouTube
endif

##################################
# GMS optional application packages
#PRODUCT_PACKAGES += \
#    AndroidForWork \
#    CalendarGoogle \
#    CloudPrint \
#    DeskClockGoogle \
#    EditorsDocs \
#    EditorsSheets \
#    EditorsSlides \
#    FaceLock \
#    GoogleCamera \
#    GoogleHome \
#    GoogleTTS \
#    Keep \
#    LatinImeGoogle \
#    NewsWeather \
#    TagGoogle \
#    talkback

##################################
###new add for Android L begin
ifneq ($(findstring AndroidForWork,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += AndroidForWork
endif
###new add for Android L end

ifneq ($(findstring CalendarGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += CalendarGoogle
endif

ifneq ($(findstring CloudPrint,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += CloudPrint
endif

ifneq ($(findstring DeskClockGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += DeskClockGoogle
endif

###new add for Android L begin
ifneq ($(findstring EditorsDocs,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += EditorsDocs
endif

ifneq ($(findstring EditorsSheets,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += EditorsSheets
endif

ifneq ($(findstring EditorsSlides,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += EditorsSlides
endif
###new add for Android L end
ifneq ($(findstring FaceLock,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += FaceLock
endif

ifneq ($(findstring VideoEditorGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleCamera
endif

ifneq ($(findstring GoogleHome,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleHome
endif


ifneq ($(findstring GoogleTTS,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleTTS
endif

ifneq ($(findstring Keep,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Keep
endif

ifneq ($(findstring LatinImeGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += LatinImeGoogle
endif

ifneq ($(findstring TagGoogle,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += TagGoogle
endif


ifneq ($(findstring Talkback,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += talkback
endif

####################################
PRODUCT_PACKAGES += google_generic_update.txt
PRODUCT_PACKAGES += com.google.android.maps.jar
PRODUCT_PACKAGES += com.google.android.media.effects.jar

# more GMS optional application packages
##PRODUCT_PACKAGES += \
#    DMAgent \
#    GoogleEars \
#    GoogleEarth \
#    GoogleHindiIME \
#    GooglePinyinIME \
#    KoreanIME \
#    Wallet

# Add overlay for ManagedProvisioning if this devices targets AfW
#GIONEE futao modify for CR01480805
#ifneq ($(filter $(PRODUCT_PACKAGES),AndroidForWork),)
# The following framework overlay must be included if AndroidForWork.apk is included
PRODUCT_PACKAGE_OVERLAYS += packages/apps/3rd_GMS/3rd_GMS_$(GN3RD_GMS_SUPPORT)/apps/AndroidForWork/overlay
#endif

ifneq ($(findstring DMAgent,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += DMAgent
endif

ifneq ($(findstring GoogleEars,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleEars
endif

ifneq ($(findstring GoogleEarth,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleEarth
endif

ifneq ($(findstring GoogleHindiIME,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GoogleHindiIME
endif

ifneq ($(findstring GooglePinyinIME,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += GooglePinyinIME
endif

ifneq ($(findstring KoreanIME,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += KoreanIME
endif

ifneq ($(findstring Wallet,$(GN3RD_GMS_APPLICATION)),)
PRODUCT_PACKAGES += Wallet
endif
# Overlay for Google network and fused location providers
$(call inherit-product, device/sample/products/location_overlay.mk)

# Overrides
PRODUCT_PROPERTY_OVERRIDES += \
    ro.setupwizard.mode=OPTIONAL \
    ro.com.google.gmsversion=5.1_r1
