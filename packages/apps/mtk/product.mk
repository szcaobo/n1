ifneq ($(findstring IUNI,$(GN_PROJECT)),)

#$(info aurora_apk)
#ifeq ($(strip $(GN_APK_AMIGO_EXCHANGE_SUPPORT)),yes)
#PRODUCT_PACKAGES += Exchange
#endif

PRODUCT_PACKAGES += Amigo_Compass
PRODUCT_PACKAGES += Aurora_Account
PRODUCT_PACKAGES += Aurora_AccountSetting
PRODUCT_PACKAGES += Aurora_AdBlockPlugin
PRODUCT_PACKAGES += Aurora_AudioProfile
PRODUCT_PACKAGES += AutoMMI
PRODUCT_PACKAGES += Aurora_Browser
PRODUCT_PACKAGES += Calculator
PRODUCT_PACKAGES += Aurora_Calendar
PRODUCT_PACKAGES += Aurora_CalendarProvider
PRODUCT_PACKAGES += Aurora_Camera
#PRODUCT_PACKAGES += Aurora_Changer
PRODUCT_PACKAGES += Contacts
PRODUCT_PACKAGES += ContactsProvider
PRODUCT_PACKAGES += Aurora_DeskClock
PRODUCT_PACKAGES += Aurora_DownLoader
PRODUCT_PACKAGES += Aurora_DownLoadProvider
PRODUCT_PACKAGES += Aurora_Email
PRODUCT_PACKAGES += Aurora_FileManager
PRODUCT_PACKAGES += Aurora_Gallery
PRODUCT_PACKAGES += Aurora_Hook
PRODUCT_PACKAGES += IUNI_Stk
PRODUCT_PACKAGES += Aurora_Launcher
PRODUCT_PACKAGES += Aurora_Launcher_Res
PRODUCT_PACKAGES += Aurora_Market
PRODUCT_PACKAGES += Aurora_MediaScanner
PRODUCT_PACKAGES += Aurora_MMITest
PRODUCT_PACKAGES += Mms
PRODUCT_PACKAGES += Aurora_Music
#PRODUCT_PACKAGES += Aurora_NetManage
PRODUCT_PACKAGES += Aurora_Note
PRODUCT_PACKAGES += PackageInstaller
PRODUCT_PACKAGES += Phone
PRODUCT_PACKAGES += Aurora_PrivacyManage
PRODUCT_PACKAGES += Aurora_Reject
#PRODUCT_PACKAGES += Aurora_Secure
PRODUCT_PACKAGES += Aurora_Settings
PRODUCT_PACKAGES += Aurora_SettingUpdate
PRODUCT_PACKAGES += Aurora_TelephonyProvider
PRODUCT_PACKAGES += Aurora_Torch
PRODUCT_PACKAGES += Aurora_VoiceAssistant
PRODUCT_PACKAGES += Aurora_Weather
PRODUCT_PACKAGES += LBESecV6_IUNIOS
PRODUCT_PACKAGES += SogouInput_iuni
PRODUCT_PACKAGES += YuloreFrameWork
PRODUCT_PACKAGES += Aurora_Launcher_Res
PRODUCT_PACKAGES += CloudGallery

PRODUCT_PACKAGES += Aurora_SetupWizard
PRODUCT_PACKAGES += Aurora_PureManager
PRODUCT_PACKAGES += Amigo_SettingsProvider
PRODUCT_PACKAGES += Aurora_PowerSaveLauncher
PRODUCT_PACKAGES += Aurora_ThemeManager
PRODUCT_PACKAGES += DualWechat
#PRODUCT_PACKAGES += LBESecurity
#PRODUCT_PACKAGES += LBEBootstrap

endif
