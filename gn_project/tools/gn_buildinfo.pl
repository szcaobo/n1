#!/usr/bin/perl

print "persist.sys.timezone=Asia/Shanghai\n";

#Gionee zhangxiaowei 20141231 add for CR01431838 start

if(defined($ENV{GN_RW_GN_MMI_AUTOTEST2})) {
	print "gn.mmi.autotest2=$ENV{GN_RW_GN_MMI_AUTOTEST2}\n";
}
if(defined($ENV{GN_RW_GN_MMI_WCDMA})) {
	print "gn.mmi.wcdma=$ENV{GN_RW_GN_MMI_WCDMA}\n";
}
if(defined($ENV{GN_RW_GN_MMI_TDSCDMA})) {
	print "gn.mmi.tdscdma=$ENV{GN_RW_GN_MMI_TDSCDMA}\n";
}
if(defined($ENV{GN_RW_GN_MMI_LTETDD})) {
	print "gn.mmi.ltetdd=$ENV{GN_RW_GN_MMI_LTETDD}\n";
}
if(defined($ENV{GN_RW_GN_MMI_LTEFDD})) {
	print "gn.mmi.ltefdd=$ENV{GN_RW_GN_MMI_LTEFDD}\n";
}
if(defined($ENV{GN_RW_GN_MMI_LTETDDANT})) {
	print "gn.mmi.ltetddant=$ENV{GN_RW_GN_MMI_LTETDDANT}\n";
}
#GIONEE: lujian 2015-05-18 modify for "CR01483225" begin
if(defined($ENV{GN_RW_GN_MMI_GPS_COCLOCK})) {
	print "gn.mmi.gps.coclock=$ENV{GN_RW_GN_MMI_GPS_COCLOCK}\n";
}
#GIONEE: lujian 2015-05-18 modify for "CR01483225"   end

if(defined($ENV{GN_RW_GN_MMI_KEYTEST_MENU})) {
	print "gn.mmi.keytest.menu=$ENV{GN_RW_GN_MMI_KEYTEST_MENU}\n";
}

if(defined($ENV{GN_RW_GN_MMI_KEYTEST_APP})) {
	print "gn.mmi.keytest.app=$ENV{GN_RW_GN_MMI_KEYTEST_APP}\n";
}

if(defined($ENV{GN_RW_GN_MMI_KEYTEST_SEARCH})) {
	print "gn.mmi.keytest.search=$ENV{GN_RW_GN_MMI_KEYTEST_SEARCH}\n";
}

if(defined($ENV{GN_RW_GN_MMI_KEYTEST_CAMERA})) {
	print "gn.mmi.keytest.camera=$ENV{GN_RW_GN_MMI_KEYTEST_CAMERA}\n";
}
if(defined($ENV{GN_RW_GN_MMI_KEYTEST_FOCUS})) {
	print "gn.mmi.keytest.focus=$ENV{GN_RW_GN_MMI_KEYTEST_FOCUS}\n";
}

if(defined($ENV{GN_RW_GN_MMI_KEYTEST_HALL})) {
	print "gn.mmi.keytest.hall=$ENV{GN_RW_GN_MMI_KEYTEST_HALL}\n";
}
if(defined($ENV{GN_RW_GN_MMI_KEYTEST_BACK})) {
	print "gn.mmi.keytest.back=$ENV{GN_RW_GN_MMI_KEYTEST_BACK}\n";
}
if(defined($ENV{GN_RW_GN_MMI_KEYTEST_HOME})) {
	print "gn.mmi.keytest.home=$ENV{GN_RW_GN_MMI_KEYTEST_HOME}\n";
}

if(defined($ENV{GN_RW_GN_MMI_SENSOR_GYRO})) {
	print "gn.mmi.sensor.gyro=$ENV{GN_RW_GN_MMI_SENSOR_GYRO}\n";
}

if(defined($ENV{GN_RW_GN_MMI_TP_CROSS})) {
	print "gn.mmi.tp.cross=$ENV{GN_RW_GN_MMI_TP_CROSS}\n";
}

if(defined($ENV{GN_RW_GN_MMI_TP_TEN})) {
	print "gn.mmi.tp.ten=$ENV{GN_RW_GN_MMI_TP_TEN}\n";
}

if(defined($ENV{GN_RW_GN_MMI_MIC2})) {
	print "gn.mmi.mic2=$ENV{GN_RW_GN_MMI_MIC2}\n";
}

if(defined($ENV{GN_RW_GN_MMI_RECEIVER2})) {
	print "gn.mmi.receiver2=$ENV{GN_RW_GN_MMI_RECEIVER2}\n";
}

if(defined($ENV{GN_RW_GN_MMI_DOUBLETONE})) {
	print "gn.mmi.dualtone=$ENV{GN_RW_GN_MMI_DOUBLETONE}\n";
}
if(defined($ENV{GN_RW_GN_MMI_OTG})) {
	print "gn.mmi.otg=$ENV{GN_RW_GN_MMI_OTG}\n";
}

if(defined($ENV{GN_RW_GN_MMI_FLASHLIGHT})) {
	print "gn.mmi.flashlight=$ENV{GN_RW_GN_MMI_FLASHLIGHT}\n";
}
if(defined($ENV{GN_RW_GN_MMI_SETCOLOR})) {
	print "gn.mmi.setcolor=$ENV{GN_RW_GN_MMI_SETCOLOR}\n";
}
if(defined($ENV{GN_RW_GN_MMI_IRTEST})) {
	print "gn.mmi.irtest=$ENV{GN_RW_GN_MMI_IRTEST}\n";
}
if(defined($ENV{GN_RW_GN_MMI_WIFI5G})) {
	print "gn.mmi.wifi5g=$ENV{GN_RW_GN_MMI_WIFI5G}\n";
}
if(defined($ENV{GN_RW_GN_MMI_NFC})) {
	print "gn.mmi.nfc=$ENV{GN_RW_GN_MMI_NFC}\n";
}
if(defined($ENV{GN_RW_GN_MMI_NFC2})) {
	print "gn.mmi.nfc2=$ENV{GN_RW_GN_MMI_NFC2}\n";
}
if(defined($ENV{GN_RW_GN_MMI_FLASH})) {
	print "gn.mmi.flash=$ENV{GN_RW_GN_MMI_FLASH}\n";
}
if(defined($ENV{GN_RW_GN_MMI_FLASH2})) {
	print "gn.mmi.flash2=$ENV{GN_RW_GN_MMI_FLASH2}\n";
}
if(defined($ENV{GN_RW_GN_MMI_FINGERPRINTS})) {
	print "gn.mmi.fingerprints=$ENV{GN_RW_GN_MMI_FINGERPRINTS}\n";
}
if(defined($ENV{GN_RW_GN_MMI_TOUCHPRIVPAD})) {
	print "gn.mmi.touch_priv_pad=$ENV{GN_RW_GN_MMI_TOUCHPRIVPAD}\n";
}
if(defined($ENV{GN_RW_GN_MMI_FM})) {
	print "gn.mmi.fm=$ENV{GN_RW_GN_MMI_FM}\n";
}
if(defined($ENV{GN_RW_GN_AUTOMMI_KEYTEST_APP})) {
	print "gn.autommi.keytest.app=$ENV{GN_RW_GN_AUTOMMI_KEYTEST_APP}\n";
}
if(defined($ENV{GN_RW_GN_ATUOMMI_KEYTEST_MENU})) {
	print "gn.autommi.keytest.menu=$ENV{GN_RW_GN_ATUOMMI_KEYTEST_MENU}\n";
}
if(defined($ENV{GN_RW_GN_ATUOMMI_KEYTEST_CAMERA})) {
	print "gn.autommi.keytest.camerea=$ENV{GN_RW_GN_ATUOMMI_KEYTEST_CAMERA}\n";
}
if(defined($ENV{GN_RW_GN_ATUOMMI_KEYTEST_FOCUS})) {
	print "gn.autommi.keytest.focus=$ENV{GN_RW_GN_ATUOMMI_KEYTEST_FOCUS}\n";
}
if(defined($ENV{GN_RW_GN_ATUOMMI_KEYTEST_HALL})) {
	print "gn.autommi.keytest.hall=$ENV{GN_RW_GN_ATUOMMI_KEYTEST_HALL}\n";
}
if(defined($ENV{GN_RW_GN_ATUOMMI_KEYTEST_BACK})) {
	print "gn.autommi.keytest.back=$ENV{GN_RW_GN_ATUOMMI_KEYTEST_BACK}\n";
}
if(defined($ENV{GN_RW_GN_ATUOMMI_KEYTEST_HOME})) {
	print "gn.autommi.keytest.home=$ENV{GN_RW_GN_ATUOMMI_KEYTEST_HOME}\n";
}

#GIONEE: lujian 2015-01-27 modify for "CR01435990" begin
if (exists $ENV{GN_MMI_NEED_CALI} && $ENV{GN_MMI_NEED_CALI} ne "")
{
    print "ro.gn.need.cal=$ENV{GN_MMI_NEED_CALI}\n";
}
#GIONEE: lujian 2015-01-27 modify for "CR01435990"   end


#GIONEE: lujian 2015-01-27 modify for "CR01435990" begin
if (exists $ENV{GN_MMI_ORIGINAL_FM} && $ENV{GN_MMI_ORIGINAL_FM} ne "")
{
    print "gn.mmi.original.fm=$ENV{GN_MMI_ORIGINAL_FM}\n";
}
#GIONEE: lujian 2015-01-27 modify for "CR01435990"   end


#Gionee fengxb 2011-10-5 add for CR00366523 start
if(defined($ENV{GN_MATCH_NUMBERLENGTH})) {
	print "ro.gn.match.numberlength=$ENV{GN_MATCH_NUMBERLENGTH}\n";
}
#Gionee fengxb 2011-10-5 add for CR00366523 end


#Gionee zhangxiaowei 20141231 add for CR01431838 end

if(defined($ENV{GN_RO_GN_RESPIRATIONLAMP_SUPPORT})) {
	print "ro.gn.respirationlamp.support=$ENV{GN_RO_GN_RESPIRATIONLAMP_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_GNVERNUMBERREL})) {
	print "ro.gn.gnvernumberrel=$ENV{GN_RO_GN_GNVERNUMBERREL}\n";
}

if(defined($ENV{GN_RO_CONFIG_NOTIFICATION_SOUND})) {
	print "ro.config.notification_sound=$ENV{GN_RO_CONFIG_NOTIFICATION_SOUND}\n";
}

if(defined($ENV{GN_RO_CONFIG_ALARM_ALERT})) {
	print "ro.config.alarm_alert=$ENV{GN_RO_CONFIG_ALARM_ALERT}\n";
}

if(defined($ENV{GN_RO_CONFIG_RINGTONE})) {
	print "ro.config.ringtone=$ENV{GN_RO_CONFIG_RINGTONE}\n";
}

if(defined($ENV{GN_RO_GN_DEFAULT_RINGTONE2})) {
	print "ro.gn.default.ringtone2=$ENV{GN_RO_GN_DEFAULT_RINGTONE2}\n";
}

if(defined($ENV{GN_RO_GN_DEFAULT_MMSTONE})) {
	print "ro.gn.default.mmstone=$ENV{GN_RO_GN_DEFAULT_MMSTONE}\n";
}

if(defined($ENV{GN_RO_GN_DEFAULT_MMSTONE2})) {
	print "ro.gn.default.mmstone2=$ENV{GN_RO_GN_DEFAULT_MMSTONE2}\n";
}

if(defined($ENV{GN_RO_GN_DEFAULT_VIDEOCALL})) {
	print "ro.gn.default.videocall=$ENV{GN_RO_GN_DEFAULT_VIDEOCALL}\n";
}

if(defined($ENV{GN_RO_GN_OP_SPECIAL_VN})) {
	print "ro.gn.op_special_vn=$ENV{GN_RO_GN_OP_SPECIAL_VN}\n";
}

if(defined($ENV{GN_RO_GN_SDGCALL_ACCESS_SUPPORT})) {
	print "ro.gn.sdgcall.access.support=$ENV{GN_RO_GN_SDGCALL_ACCESS_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_HAND_ANSWER_SUPPORT})) {
	print "ro.gn.hand.answer.support=$ENV{GN_RO_GN_HAND_ANSWER_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_PHONE_CLOSEDUALMIC})) {
	print "ro.gn.phone.closedualmic=$ENV{GN_RO_GN_PHONE_CLOSEDUALMIC}\n";
}

if(defined($ENV{GN_RO_BUILD_DISPLAY_ID})) {
	print "ro.build.display.id=$ENV{GN_RO_BUILD_DISPLAY_ID}\n";
}

if(defined($ENV{GN_RO_PRODUCT_MODEL})) {
	print "ro.product.model=$ENV{GN_RO_PRODUCT_MODEL}\n";
}

if(defined($ENV{GN_RO_PRODUCT_BRAND})) {
	print "ro.product.brand=$ENV{GN_RO_PRODUCT_BRAND}\n";
}

if(defined($ENV{GN_RO_PRODUCT_NAME})) {
	print "ro.product.name=$ENV{GN_RO_PRODUCT_NAME}\n";
}

if(defined($ENV{GN_RO_PRODUCT_DEVICE})) {
	print "ro.product.device=$ENV{GN_RO_PRODUCT_DEVICE}\n";
}

if(defined($ENV{GN_RO_GN_GNROMVERNUMBER})) {
	print "ro.gn.gnromvernumber=$ENV{GN_RO_GN_GNROMVERNUMBER}\n";
}

if(defined($ENV{GN_RO_PRODUCT_MANUFACTURER})) {
	print "ro.product.manufacturer=$ENV{GN_RO_PRODUCT_MANUFACTURER}\n";
}

if(defined($ENV{GN_RO_GN_HAPTIC_VIBRATOR_PROP})) {
	print "ro.gn.haptic.vibrator.prop=$ENV{GN_RO_GN_HAPTIC_VIBRATOR_PROP}\n";
}

if(defined($ENV{GN_RO_GN_MUSIC_DTS})) {
	print "ro.gn.music.dts=$ENV{GN_RO_GN_MUSIC_DTS}\n";
}

if(defined($ENV{GN_RO_GN_MUSIC_SRS})) {
	print "ro.gn.music.srs=$ENV{GN_RO_GN_MUSIC_SRS}\n";
}

if(defined($ENV{GN_RO_GN_SDCARD_TYPE})) {
	print "ro.gn.sdcard.type=$ENV{GN_RO_GN_SDCARD_TYPE}\n";
}

if(defined($ENV{GN_RO_GN_AMOLED_LCMACL_SUPPORT})) {
	print "ro.gn.amoled.lcmacl.support=$ENV{GN_RO_GN_AMOLED_LCMACL_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_SINGLE_HAND_SUPPORT})) {
	print "ro.gn.single.hand.support=$ENV{GN_RO_GN_SINGLE_HAND_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_SOUNDCTRL_SUPPORT})) {
	print "ro.gn.soundctrl.support=$ENV{GN_RO_GN_SOUNDCTRL_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_MTK_POWEROFF_ALARM_PROP})) {
	print "ro.gn.mtk.poweroff.alarm.prop=$ENV{GN_RO_GN_MTK_POWEROFF_ALARM_PROP}\n";
}

#GIONEE:guoxt modify for CR01437293 2015-01-19 begin
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes")
{
    if(defined($ENV{GN_OVERSEA_EXTERNAL_VERNUMBER})) {
	print "ro.gn.extvernumber=$ENV{GN_OVERSEA_EXTERNAL_VERNUMBER}\n";
    }
    if(defined($ENV{GN_OVERSEA_HW_VERSION})) {
	print "ro.gn.extHWvernumber=$ENV{GN_OVERSEA_HW_VERSION}\n";
    }
}else{
    if(defined($ENV{GN_RO_GN_EXTVERNUMBER})) {
	print "ro.gn.extvernumber=$ENV{GN_RO_GN_EXTVERNUMBER}\n";
    }
}
#GIONEE:guoxt modify for CR01437293 2015-01-19 end

if(defined($ENV{GN_RO_GN_EXTMODEL})) {
	print "ro.gn.extmodel=$ENV{GN_RO_GN_EXTMODEL}\n";
}

if(defined($ENV{GN_RO_GN_NFC_SUPPORT})) {
	print "ro.gn.nfc.support=$ENV{GN_RO_GN_NFC_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_VOICEWAKE_SUPPORT})) {
	print "ro.gn.voicewake.support=$ENV{GN_RO_GN_VOICEWAKE_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_CLICKWAKE_SUPPORT})) {
	print "ro.gn.clickwake.support=$ENV{GN_RO_GN_CLICKWAKE_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_BUTTONLIGHT_SUPPORT})) {
	print "ro.gn.buttonlight.support=$ENV{GN_RO_GN_BUTTONLIGHT_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_QUICKOPERATE_SUPPORT})) {
	print "ro.gn.quickoperate.support=$ENV{GN_RO_GN_QUICKOPERATE_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_GLOVE_PATTERNS_SUPPORT})) {
	print "ro.gn.glove_patterns.support=$ENV{GN_RO_GN_GLOVE_PATTERNS_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_OPTRSPECSEGDEF})) {
	print "ro.gn.optrspecsegdef=$ENV{GN_RO_GN_OPTRSPECSEGDEF}\n";
}

if(defined($ENV{GN_RO_GN_PLATFORM_SUPPORT})) {
	print "ro.gn.platform.support=$ENV{GN_RO_GN_PLATFORM_SUPPORT}\n";
}

if(defined($ENV{GN_RW_GN_SENSOR_ADJUST_LCD_SUPPORT})) {
	print "gn.sensor.adjust.lcd.support=$ENV{GN_RW_GN_SENSOR_ADJUST_LCD_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_LCD_EFFECT_SUPPORT})) {
	print "ro.gn.lcd.effect.support=$ENV{GN_RO_GN_LCD_EFFECT_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_GN2SDCARDSWAP})) {
	print "ro.gn.gn2sdcardswap=$ENV{GN_RO_GN_GN2SDCARDSWAP}\n";
}

if(defined($ENV{GN_RO_PRODUCT_SCREENSIZE})) {
	print "ro.product.screensize=$ENV{GN_RO_PRODUCT_SCREENSIZE}\n";
}

if(defined($ENV{GN_RO_IREADER_CHANNEL})) {
	print "ro.ireader.channel=$ENV{GN_RO_IREADER_CHANNEL}\n";
}

if(defined($ENV{GN_RO_GN_CTASEC_SUPPORT})) {
	print "ro.gn.ctasec.support=$ENV{GN_RO_GN_CTASEC_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_TDSCDMA_SUPPORT})) {
	print "ro.gn.tdscdma.support=$ENV{GN_RO_GN_TDSCDMA_SUPPORT}\n";
}

if(defined($ENV{GN_RW_RUNTIME_CTS_TEST})) {
	print "runtime.cts.test=$ENV{GN_RW_RUNTIME_CTS_TEST}\n";
}

#GIONEE: luohui 2015-1-20 modify for Oversea start->
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes") {
    if(exists $ENV{GIONEEPROJECTID}) {
	    print "ro.gn.gnproductid=$ENV{GIONEEPROJECTID}\n";
    }

    if(exists $ENV{GIONEEPRODUCTID}) {
	    print "ro.gn.gnprojectid=$ENV{GIONEEPRODUCTID}\n";
    }
}else{
    if(defined($ENV{GN_RO_GN_GNPRODUCTID})) {
	    print "ro.gn.gnproductid=$ENV{GN_RO_GN_GNPRODUCTID}\n";
    }

    if(defined($ENV{GN_RO_GN_GNPROJECTID})) {
	    print "ro.gn.gnprojectid=$ENV{GN_RO_GN_GNPROJECTID}\n";
    }
}
#GIONEE: luohui 2015-1-20 modify for Oversea end<-

#GIONEE:guoxt modify for CR01442146 2015-01-26 begin
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes")
{
	if (exists $ENV{GN_THEME_DEFAULT_PACKAGENAME} && $ENV{GN_THEME_DEFAULT_PACKAGENAME} ne "")
	{
	    print "ro.gn.theme.default=$ENV{GN_THEME_DEFAULT_PACKAGENAME}\n";
	}
}
#GIONEE:guoxt modify for CR01442146 2015-01-26 end

#Gionee guoxt 20150120 modify for gionee brand OTA begin
if ($GN_GNOTAUPDATE_SUPPORT eq "yes") 
{ 
    print "#ro.gn.gnotaupdate.support=$ENV{GN_GNOTAUPDATE_SUPPORT}\n";
}else{
    print "#ro.gn.gnotaupdate.support=no\n";
}

if($ENV{GN_APK_AMIGO_SETTINGUPDATE_SUPPORT} eq "yes" && defined(GN_OVERSEA_OTA_SERVER)){
    print "ro.gn.oversea.ota.server=$ENV{GN_OVERSEA_OTA_SERVER}\n";
}
#Gionee guoxt 20150120 modify for gionee brand OTA end


if(defined($ENV{GN_RO_GN_GNZNVERNUMBER})) {
	print "ro.gn.gnznvernumber=$ENV{GN_RO_GN_GNZNVERNUMBER}\n";
}

#GIONEE:guoxt modify for CR01437293 2015-01-19 begin
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes") {
    if(defined($ENV{GN_INTERNAL_VER_NUMBER})) {
        print "ro.gn.gnvernumber=$ENV{GN_INTERNAL_VER_NUMBER}\n";
    }  
}else{
    if(defined($ENV{GN_RO_GN_GNVERNUMBER})) {
        print "ro.gn.gnvernumber=$ENV{GN_RO_GN_GNVERNUMBER}\n";
    }
}

if(defined($ENV{GN_RO_GN_OP_SPECIAL_VN})) {
	print "ro.gn.op_special_vn=$ENV{GN_RO_GN_OP_SPECIAL_VN}\n";
}

if(defined($ENV{GN_RO_GN_CUTEST})) {
	print "ro.gn.cutest=$ENV{GN_RO_GN_CUTEST}\n";
}

if(defined($ENV{GN_RO_GN_EFFECT_OPEN})) {
	print "ro.gn.effect.open=$ENV{GN_RO_GN_EFFECT_OPEN}\n";
}

if(defined($ENV{GN_RO_GN_GSD_PROP})) {
	print "ro.gn.gsd.prop=$ENV{GN_RO_GN_GSD_PROP}\n";
}

if(defined($ENV{GN_RO_GN_CAMERA_ERASER_SUPPORT})) {
	print "ro.gn.camera.eraser.support=$ENV{GN_RO_GN_CAMERA_ERASER_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_CAMERA_BESTFACE_SUPPORT})) {
	print "ro.gn.camera.bestface.support=$ENV{GN_RO_GN_CAMERA_BESTFACE_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_CAMERA_GNSPORTS_SUPPORT})) {
	print "ro.gn.camera.gnsports.support=$ENV{GN_RO_GN_CAMERA_GNSPORTS_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_CAMERA_PQ_SUPPOR})) {
	print "ro.gn.camera.pq.suppor=$ENV{GN_RO_GN_CAMERA_PQ_SUPPOR}\n";
}

if(defined($ENV{GN_RO_GN_CAMERA_FD_MENU_SUPPORT})) {
	print "ro.gn.camera.fd.menu.support=$ENV{GN_RO_GN_CAMERA_FD_MENU_SUPPORT}\n";
}

if(defined($ENV{GN_RO_GN_CAMERA_PREVIEW_STANDARD})) {
	print "ro.gn.camera.preview.standard=$ENV{GN_RO_GN_CAMERA_PREVIEW_STANDARD}\n";
}

#Gionee 20150109 chenrui add for oversea start
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes") {
    print "ro.gn.oversea.product=$ENV{GN_OVERSEA_PRODUCT}\n";
}

if (exists $ENV{GN_OVERSEA_CUSTOM} && $ENV{GN_OVERSEA_CUSTOM} ne "") {
    print "ro.gn.oversea.custom=$ENV{GN_OVERSEA_CUSTOM}\n";
}
#Gionee 20150109 chenrui add for oversea end
#gionee  zhangpj 20150109 add for CR01429061 begin
if (exists $ENV{GN_CAMERA_ZSL_DEFAULT_ON} && $ENV{GN_CAMERA_ZSL_DEFAULT_ON} eq "yes")
{
    print "ro.gn.camera.zsl.default.on=$ENV{GN_CAMERA_ZSL_DEFAULT_ON}\n";
} else {
    print "ro.gn.camera.zsl.default.on=no\n";
}

if (exists $ENV{GN_CAMERA_GOTOCHARM_SUPPORT} && $ENV{GN_CAMERA_GOTOCHARM_SUPPORT} eq "yes")
{
    print "ro.gn.camera.gotocharm.support=$ENV{GN_CAMERA_GOTOCHARM_SUPPORT}\n";
} else {
    print "ro.gn.camera.gotocharm.support=no\n";
}

if (exists $ENV{GN_CAMERA_PICBEST_SUPPORT} && $ENV{GN_CAMERA_PICBEST_SUPPORT} eq "yes")
{
    print "ro.gn.camera.picbest.support=$ENV{GN_CAMERA_PICBEST_SUPPORT}\n";
} else {
    print "ro.gn.camera.picbest.support=no\n";
}

if (exists $ENV{GN_CAMERA_PICLEAR_SUPPORT} && $ENV{GN_CAMERA_PICLEAR_SUPPORT} eq "yes")
{
    print "ro.gn.camera.piclear.support=$ENV{GN_CAMERA_PICLEAR_SUPPORT}\n";
} else {
    print "ro.gn.camera.piclear.support=no\n";
}

if (exists $ENV{GN_CAMERA_ACTIONPATH_SUPPORT} && $ENV{GN_CAMERA_ACTIONPATH_SUPPORT} eq "yes")
{
    print "ro.gn.camera.actionpath.support=$ENV{GN_CAMERA_ACTIONPATH_SUPPORT}\n";
} else {
    print "ro.gn.camera.actionpath.support=no\n";
}

if (exists $ENV{GN_CAMERA_FD_DEFAULT_ON} && $ENV{GN_CAMERA_FD_DEFAULT_ON} eq "yes")
{
    print "ro.gn.camera.fd.default.on=$ENV{GN_CAMERA_FD_DEFAULT_ON}\n";
} else {
    print "ro.gn.camera.fd.default.on=no\n";
}

if (exists $ENV{GN_CAMERA_FULLSCREEN_DEFAULT_ON} && $ENV{GN_CAMERA_FULLSCREEN_DEFAULT_ON} eq "yes")
{
    print "ro.gn.camera.default.fullscreen=$ENV{GN_CAMERA_FULLSCREEN_DEFAULT_ON}\n";
} else {
    print "ro.gn.camera.default.fullscreen=no\n";
}
#gionee  zhangpj 20150109 add for CR01429061 end
#Gionee baiyh 2015-02-26 add for CR01445228 begin
if (exists $ENV{GN_SUPPORT_PERMANENTMEMUKEY} && $ENV{GN_SUPPORT_PERMANENTMEMUKEY} eq "yes")
{
    print "ro.gn.permanentmenukey=$ENV{GN_SUPPORT_PERMANENTMEMUKEY}\n";
}
#Gionee baiyh 2015-02-26 add for CR01445228 end

#Gionee baiyh 2015-02-26 add for CR01445228 begin
if(defined($ENV{GN3RD_GMS_SUPPORT})) {
    print "ro.gn.gms.support=$ENV{GN3RD_GMS_SUPPORT}\n";
}else{
    print "ro.gn.gms.support=no\n";
}
#Gionee baiyh 2015-02-26 add for CR01445228 end

if(defined($ENV{GN_RO_GN_GEMINI_SUPPORT})) {
	print "ro.gn.gemini.support=$ENV{GN_RO_GN_GEMINI_SUPPORT}\n";
}
if(defined($ENV{GN_RO_GN_NAVI_KEYGUARD_APP_SUPPORT})) {
	print "ro.gn.navi.keyguard.app.support=$ENV{GN_RO_GN_NAVI_KEYGUARD_APP_SUPPORT}\n";
}

if(defined($ENV{GN_SMARTSTAY_SUPPORT})) {
	print "ro.gn.smartstay.support=$ENV{GN_SMARTSTAY_SUPPORT}\n";
}

if (exists $ENV{GN_DEFAULT_INPUT_METHOD})
{
    print "ro.gn.default.inputmethod=$ENV{GN_DEFAULT_INPUT_METHOD}\n";
}
#Gionee <sunll> <2014-12-25> add for gesture start
if (defined($ENV{GN_GESTURE_SUPPORT}) && $ENV{GN_GESTURE_SUPPORT} eq "yes")
{
    print "ro.gn.gesture.support=$ENV{GN_GESTURE_SUPPORT}\n";
}else{
    print "ro.gn.gesture.support=no\n";
}

if(defined($ENV{GN_BRUSH_SWITCH})) {
	print "persist.sys.brush.switch=$ENV{GN_BRUSH_SWITCH}\n";
}
#Gionee <sunll> <2014-12-25> add for gesture end

#Gionee <bug> <lichao> <2014-12-30> add for CR01432033 begin
#Gionee <bug> <lichao> <2015-02-13> modify
#Gionee xuwen 2013-07-09 add for CR00834689
if(defined($ENV{GN_AP_PW_DIALOG_SUPPORT})) {
	print "ro.gn.appwdialog.support=$ENV{GN_AP_PW_DIALOG_SUPPORT}\n";
}
if(defined($ENV{GN_NET_AUTOSWITCH_SUPPORT})) {
	print "ro.gn.net.autoswitch.support=$ENV{GN_NET_AUTOSWITCH_SUPPORT}\n";
}
#Gionee <bug> <lichao> <2014-12-30> add for CR01432033 end

#Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 begin
if (defined($ENV{GN_MTK_BSP_PS_CALIBRATE_INCALL}))
{
	print "ro.gn.pscali.incall.support=$ENV{GN_MTK_BSP_PS_CALIBRATE_INCALL}\n";
}
#Gionee BSP1 yaoyc 20140927 add for incall p-sensor calibrate CR01391528 end
#Gionee <Amigo_Skylight> wangym <20141208> add for  CR01422131 begin
if (defined($ENV{GN_APPSKYLIGHT_SUPPORT}))
{
	print "ro.gn.sky.light.support=$ENV{GN_APPSKYLIGHT_SUPPORT}\n";
}
#Gionee <Amigo_Skylight> wangym <20141208> add for  CR01422131 end

#Gionee <Amigo_Oom_Opt> liuran <20150128> add for  CR01443018 begin
if (defined($ENV{GN_OOM_OPT_SUPPORT}))
{
	print "ro.gn.oom.opt=$ENV{GN_OOM_OPT_SUPPORT}\n";
}
#Gionee <Amigo_Oom_Opt> liuran <20150128> add for  CR01443018 end

#GIONEE:guoxt modify for CR01477578 2015-05-11 begin
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes")
{
  print "persist.sys.permission.level=1\n";
}else{
  print "persist.sys.permission.level=-1\n";
}
#GIONEE:guoxt modify for CR01477578 2015-05-11 end
print "ro.gn.sv.version=SV1.0\n";

if (exists $ENV{GN_EMULATED_STORAGE} && $ENV{GN_EMULATED_STORAGE} eq "yes")
{
    print "ro.gn.emulated.storage=$ENV{GN_EMULATED_STORAGE}\n";
}
#Gionee <sunll> 20150119 end
#Gionee <sunll> 20150119 begin
if (defined($ENV{GN_SMALLSCREEN_SUPPORT}))
{
	print "ro.gn.smallscreen.support=$ENV{GN_SMALLSCREEN_SUPPORT}\n";
}
if (defined($ENV{GN_PERSIST_SYS_PERMISSION_LEVEL}))
{
	print "persist.sys.permission.level=$ENV{GN_PERSIST_SYS_PERMISSION_LEVEL}\n";
}
if (defined($ENV{GN_SV_VERSION}))
{
	print "ro.gn.sv.version=$ENV{GN_SV_VERSION}\n";
}
#Gionee <sunll> 20150119 end

#Gionee futao 2013-03-27 add for CR00790348 begin
if (exists $ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT} && $ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT} eq "yes")
{

    print "ro.gn.oversea.music.offline.ver=$ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT}\n";
}
#Gionee futao 2013-03-27 add for CR00790348 end
#Gionee 20150110 chenrui add for CR01432011 start
if (defined($ENV{GN_USB_UI_SUPPORT})) {
	print "ro.gn.usb.ui.support=$ENV{GN_USB_UI_SUPPORT}\n";
}
#Gionee 20150110 chenrui add for CR01432011 end

#Gionee:wangfei 2015-01-07 add for begin
if(defined($ENV{GN_ONLY_DISP_ALARM_GESTURE})) {
	print "ro.gn.only.disp.alarm.gesture=$ENV{GN_ONLY_DISP_ALARM_GESTURE}\n";
}else{
	print "ro.gn.only.disp.alarm.gesture=no\n";
}

if(defined($ENV{GN_HOTKNOT_SUPPORT})) {
	print "ro.gn.hotknot.support=$ENV{GN_HOTKNOT_SUPPORT}\n";
}else{
	print "ro.gn.hotknot.support=no\n";
}
#Gionee:wangfei 2015-01-07 add for end

#Gionee:guoxt 20150109 added for CR01435173 begin
if(exists $ENV{GN_RO_PRODUCT_NAME}){
    print "ro.product.board=$ENV{GN_RO_PRODUCT_NAME}\n";
}
#Gionee:guoxt 20150109 added for CR01435173 end

#Gionee guoxt 2015-01-12 add for CR01435173 begin
if (exists $ENV{GN_BUILD_PRODUCT} && $ENV{GN_BUILD_PRODUCT} ne "")
{
    print "ro.build.product=$ENV{GN_BUILD_PRODUCT}\n";
}

if (exists $ENV{GN_GUESTMODE_SUPPORT} && $ENV{GN_GUESTMODE_SUPPORT} ne "")
{
    print "ro.gn.guestmode.support=$ENV{GN_GUESTMODE_SUPPORT}\n";
}
#Gionee guoxt 2015-01-12 add for CR01435173 end
#Gionee yanxh 2015-1-14,add for CR01436504 begin.
if (exists $ENV{GN_VIDEO_REPEATPLAY_SUPPORT} && $ENV{GN_VIDEO_REPEATPLAY_SUPPORT} eq "yes")
{
    print "#ro.gn.video.repeatplay.support=$ENV{GN_VIDEO_REPEATPLAY_SUPPORT}\n";
} else {
    print "#ro.gn.video.repeatplay.support=no\n";
}
#Gionee yanxh 2015-1-14,add for CR01436504 end
#Gionee wenjun 2015-1-14 add for CR01437456 begin.
if(defined($ENV{GN_OVERSEA_IFONT_SUPPORT})) {
	print "ro.gn.oversea.ifont_support=$ENV{GN_OVERSEA_IFONT_SUPPORT}\n";
}
#Gionee wenjun 2015-1-14 add for CR01437456 begin.
#Gionee guoxt 20150120 modify for gionee brand OTA begin
if ($GN_GNOTAUPDATE_SUPPORT eq "yes") 
{ 
    print "ro.gn.gnotaupdate.support=$ENV{GN_GNOTAUPDATE_SUPPORT}\n";
}else{
    print "ro.gn.gnotaupdate.support=no\n";
}

if($ENV{GN_APK_AMIGO_SETTINGUPDATE_SUPPORT} eq "yes" && defined(GN_OVERSEA_OTA_SERVER)){
    print "ro.gn.oversea.ota.server=$ENV{GN_OVERSEA_OTA_SERVER}\n";
}
#Gionee guoxt 20150120 modify for gionee brand OTA end

#GIONEE:guoxt modify for CR01442146 2015-01-26 begin
if (exists $ENV{GN_OVERSEA_PRODUCT} && $ENV{GN_OVERSEA_PRODUCT} eq "yes")
{
	if (exists $ENV{GN_THEME_DEFAULT_PACKAGENAME} && $ENV{GN_THEME_DEFAULT_PACKAGENAME} ne "")
	{
	    print "ro.gn.theme.default=$ENV{GN_THEME_DEFAULT_PACKAGENAME}\n";
	}
}
#GIONEE:guoxt modify for CR01442146 2015-01-26 end

#GIONEE: wangfei 2015-01-27 add for CR01442372 begin
if(defined($ENV{GN_DISTANCEGESTURE_SUPPORT})) {
	print "ro.gn.distancegesture.support=$ENV{GN_DISTANCEGESTURE_SUPPORT}\n";
}

if(defined($ENV{GN_SMART_VIBRATE_ALERT})) {
	print "ro.gn.smart.vibrate.alert=$ENV{GN_SMART_VIBRATE_ALERT}\n";
}
#GIONEE: wangfei 2015-01-27 add for CR01442372 end

#GIONEE:guoxt modify for CR01443891 2015-01-30 begin
    if (exists $ENV{GN_AMIGO_SWITCH_FONTS_SUPPORT} && $ENV{GN_AMIGO_SWITCH_FONTS_SUPPORT} eq "yes")
    {
        print "ro.gn.switch.font=$ENV{GN_AMIGO_SWITCH_FONTS_SUPPORT}\n";
    }
#GIONEE:guoxt modify for CR01443891 2015-01-30 end

#Gionee <fingerprint> <qudw> <20150121> modify for CR01440177 begin
if(defined($ENV{GN_FINGERPRINT_SUPPORT})) {
    print "ro.gn.fingerprint.support=$ENV{GN_FINGERPRINT_SUPPORT}\n";
} else {
    print "ro.gn.fingerprint.support=no\n";
}

if (exists $ENV{GN_FP_TEE_SUPPORT} && $ENV{GN_FP_TEE_SUPPORT} eq "yes")
{
    print "ro.gn.fp.tee.support=$ENV{GN_FP_TEE_SUPPORT}\n";
} else {
    print "ro.gn.fp.tee.support=no\n";
}
#Gionee <fingerprint> <qudw> <20150121> modify for CR01440177 end

#Gionee <NFC> <qudw> <20150202> modify for nfc begin
if (exists $ENV{GN_NFC_NXP_SUPPORT} && $ENV{GN_NFC_NXP_SUPPORT} eq "yes")
{
    print "ro.gn.nfc.nxp.support=$ENV{GN_NFC_NXP_SUPPORT}\n";
} else {
    print "ro.gn.nfc.nxp.support=no\n";
}
#Gionee <NFC> <qudw> <20150202> modify for nfc end

#Gionee <Finger_Unlock> <longzp> <20150331> add for CR01457465 begin
if(defined($ENV{GN_FINGERPRINT_UNLOCK_SUPPORT})) {
	print "ro.gn.finger.unlock.support=$ENV{GN_FINGERPRINT_UNLOCK_SUPPORT}\n";
}
#Gionee <Finger_Unlock> <longzp> <20150331> add for CR01457465 end

if(defined($ENV{CONFIG_GN_BSP_MTK_AK4375_SUPPORT})) {
	print "ro.gn.bsp.mtk.ak4375.support=$ENV{CONFIG_GN_BSP_MTK_AK4375_SUPPORT}\n";
}

#Gionee <others> <wangmeng> <20150311> add for android navigationbar controller.begin
if (exists $ENV{GN_CONFIG_SHOW_NAVIGATIONBAR_SUPPORT}) 
{
	if ($ENV{GN_CONFIG_SHOW_NAVIGATIONBAR_SUPPORT} eq "no"){
        print "qemu.hw.mainkeys=1\n";
	}else{
        print "qemu.hw.mainkeys=0\n";
	}
}else{
    print "qemu.hw.mainkeys=1\n";
}
#Gionee <others> <wangmeng> <20150311> add for android navigationbar controller.end

#Gionee <bug> <lichao> <2015-03-13> add for CR01453981 begin
if(defined($ENV{GN_WLAN_WIFIDIRECT_SUPPORT})) {
	print "ro.gn.wlan.wifidirect.support=$ENV{GN_WLAN_WIFIDIRECT_SUPPORT}\n";
}
#Gionee <bug> <lichao> <2015-03-13> add for CR01453981 end

#Gionee rench 2015-04-30 add for CR01468748 begin
if (exists $ENV{GN_CAMERA_STORAGE_LOCATION_SUPPORT} && $ENV{GN_CAMERA_STORAGE_LOCATION_SUPPORT} eq "yes")
{
    print "ro.gn.camera.storagelocation=$ENV{GN_CAMERA_STORAGE_LOCATION_SUPPORT}\n";
}
#Gionee rench 2015-04-30 add for CR01468748 end

#Gionee hongsq 2015-05-06 add for CR01475347 begin
if (exists $ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT} && $ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT} eq "yes")
{
    print "ro.gn.oversea.music.offline.ver=$ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT}\n";
}
#Gionee hongsq 2015-05-06 add for CR01475347 end

if(defined($ENV{GN_NFC_WIFI_DIRECT_SUPPORT})) {
	print "ro.gn.nfc.wifi_direct.support=$ENV{GN_NFC_WIFI_DIRECT_SUPPORT}\n";
}

if(defined($ENV{GN_NEW_SCREEN_TIMEOUT})) {
	print "ro.gn.new.screen.timeout=$ENV{GN_NEW_SCREEN_TIMEOUT}\n";
}

#Gionee cuijiuyu 20121122 add for salesstatistic begin
print "ro.gn.salesstatistic.support=$ENV{GN_OVERSEA_SALES_STATISTIC_SUPPORT}\n";
if(exists $ENV{GN_OVERSEA_SALES_STATISTIC_URL} && $ENV{GN_OVERSEA_SALES_STATISTIC_URL} ne "")
{
    print "ro.gn.sales.salesstatistic.url=$ENV{GN_OVERSEA_SALES_STATISTIC_URL}\n";
}
#Gionee cuijiuyu 20121122 add for salesstatistic end

#Gionee guoxt 2013-12-09 add for CR00971946 start
#only for PowerSaver Control
if (exists $ENV{GN_OVERSEA_COMMON_MAINBOARD}){
    print "ro.gn.common.mainboard.prop=$ENV{GN_OVERSEA_COMMON_MAINBOARD}\n";
}
#Gionee guoxt 2013-12-09 add for CR00971946 end

#Gionee rench 2015-02-09 add for CR01447376 begin
if (defined($ENV{GN_OVERSEA_MWC_SUPPORT}) && $ENV{GN_OVERSEA_MWC_SUPPORT} eq "yes")
{
    print "ro.gn.oversea.mwc.support=$ENV{GN_OVERSEA_MWC_SUPPORT}\n";
}else{
    print "ro.gn.oversea.mwc.support=no\n";
}
#Gionee rench 2015-02-09 add for CR01447376 end

#Gionee liangjb 20150209 add for CR01447508 start
if (exists $ENV{GN_VOICEMAIL_SUPPORT} && $ENV{GN_VOICEMAIL_SUPPORT} eq "yes") 
{
    print "ro.gn.voicemail.support=$ENV{GN_VOICEMAIL_SUPPORT}\n";
}
#Gionee liangjb 20150209 add for CR01447508 end

#GIONEE: wangfei 2015-02-10 add for CR01447387 begin
if(defined($ENV{GN_WIFIDIRECT_MENU_SUPPORT})) {
	print "ro.gn.wifidirect.menu.support=$ENV{GN_WIFIDIRECT_MENU_SUPPORT}\n";
}
#GIONEE: wangfei 2015-02-10 add for CR01441873 end

#GIONEE: chenbo 2015-02-11 add for CR01447301 begin
if(defined($ENV{GN_APK_AMIGO_SYSTEMMANAGER_SUPER_SUPPORT})) {
	print "ro.gn.sysmgr.super.support=$ENV{GN_APK_AMIGO_SYSTEMMANAGER_SUPER_SUPPORT}\n";
}
#GIONEE: chenbo 2015-02-11 add for CR01447301 end

#GIONEE: yuchao 2015-02-15 add for CR01449041 begin
if (exists $ENV{GN_LONG_VOL_KEY_EXPAND} && $ENV{GN_LONG_VOL_KEY_EXPAND} eq "yes") 
{
    print "#ro.gn.long.vol_key_expand=$ENV{GN_LONG_VOL_KEY_EXPAND}\n";
}
#GIONEE: yuchao 2015-02-15 add for CR01449041 end

#gionee yewq 2012-12-17 modify for CR00747189 begin
if (exists $ENV{GN_EMAIL_GENERALSIGNATURE} && $ENV{GN_EMAIL_GENERALSIGNATURE} eq "yes") 
{
    print "#ro.gn.email.generalsignature=$ENV{GN_EMAIL_GENERALSIGNATURE}\n";
}
#gionee yewq 2012-12-17 modify for CR00747189 end

#gionee yangcuntao 2015-03-04 add for CR01449745 begin
if (exists $ENV{MOBILE_DATA_OFF_DEFAULT}) 
{
    print "ro.default_data_off=$ENV{MOBILE_DATA_OFF_DEFAULT}\n";
}
#gionee yangcuntao 2015-03-04 add for CR01449745 end

#Gionee yubo 2015-03-05 add for CR01450685 begin
if(defined($ENV{GN_SHUTDOWN_SUPPORT})) {
	print "ro.gn.shutdown.support=$ENV{GN_SHUTDOWN_SUPPORT}\n";
}
#Gionee yubo 2015-03-05 add for CR01450685 end

#Gionee rench 2015-04-23 add for CR01470087 begin
if (exists $ENV{GN_ALARM_INCOMING_CALL} && $ENV{GN_ALARM_INCOMING_CALL} eq "yes")
{
    print "ro.gn.alarm.incoming.call=$ENV{GN_ALARM_INCOMING_CALL}\n";
}
#Gionee rench 2015-04-23 add for CR01470087 end

#Gionee rench 2015-04-30 add for CR01468748 begin
if (exists $ENV{GN_CAMERA_STORAGE_LOCATION_SUPPORT} && $ENV{GN_CAMERA_STORAGE_LOCATION_SUPPORT} eq "yes")
{
    print "ro.gn.camera.storagelocation=$ENV{GN_CAMERA_STORAGE_LOCATION_SUPPORT}\n";
}
#Gionee rench 2015-04-30 add for CR01468748 end
#Gionee wangfei add for CR01454681 begin
if (exists $ENV{GN_SSGDIAL_SUPPORT})
{
    print "ro.gn.ssgdial.support=$ENV{GN_SSGDIAL_SUPPORT}\n";
}

if (exists $ENV{GN_SSGACCESSCALL_SUPPORT})
{
    print "ro.gn.ssgaccesscall.support=$ENV{GN_SSGACCESSCALL_SUPPORT}\n";
}

if (exists $ENV{GN_ONLY_FOUR_GESTURE} && $ENV{GN_ONLY_FOUR_GESTURE} eq "yes")
{
    print "ro.gn.only.four.gesture=$ENV{GN_ONLY_FOUR_GESTURE}\n";
}
#Gionee wangfei add for CR01454681 end
#Gionee hongsq 2015-05-13 add for CR01465162 begin
if (exists $ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT} && $ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT} eq "yes")
{
    print "ro.gn.oversea.music.offline.ver=$ENV{GN_OVERSEA_MUSIC_OFFLINE_SUPPORT}\n";
}
#Gionee hongsq 2015-05-13 add for CR01465162 end
#Gionee yangcuntao 2015-05-11 add for amigoframework locales setting begin
if (exists $ENV{GN_PRODUCT_LOCALES_EX1})
{
    print "ro.gn.product.locales.ex1=$ENV{GN_PRODUCT_LOCALES_EX1}\n";
}
if (exists $ENV{GN_PRODUCT_LOCALES_EX2})
{
    print "ro.gn.product.locales.ex2=$ENV{GN_PRODUCT_LOCALES_EX2}\n";
}
if (exists $ENV{GN_PRODUCT_LOCALES_EX3})
{
    print "ro.gn.product.locales.ex3=$ENV{GN_PRODUCT_LOCALES_EX3}\n";
}
if (exists $ENV{GN_PRODUCT_LOCALES_EX4})
{
    print "ro.gn.product.locales.ex4=$ENV{GN_PRODUCT_LOCALES_EX4}\n";
}
if (exists $ENV{GN_PRODUCT_LOCALES_EX5})
{
    print "ro.gn.product.locales.ex5=$ENV{GN_PRODUCT_LOCALES_EX5}\n";
}
#Gionee yangcuntao 2015-05-11 add for amigoframework locales setting end

print "dalvik.vm.checkjni=false\n";
#Gionee wangfei add for CR01484049 begin
if (exists $ENV{GN_SCREEN_PIN_SUPPORT})
{
    print "ro.gn.screen.pin.support=$ENV{GN_SCREEN_PIN_SUPPORT}\n";
}
#Gionee wangfei add for CR01484049 end

#Gionee wangfei 2015-05-19 add for CR01481055 begin
if (exists $ENV{GN_REMOVE_VIRTUAL_KEY} && $ENV{GN_REMOVE_VIRTUAL_KEY} eq "yes")
{
    print "ro.gn.remove.virtual.key=$ENV{GN_REMOVE_VIRTUAL_KEY}\n";
}
#Gionee wangfei 2015-05-19 add for CR01481055 end

#Gionee guoxt 20150520 modify for CR01484740 start
if (exists $ENV{GN_OVERSEA_ODM} && $ENV{GN_OVERSEA_ODM} eq "yes") 
{
    print "ro.gn.oversea.odm=$ENV{GN_OVERSEA_ODM}\n";
}
# Gionee guoxt 20150520 modify for CR01484740 end