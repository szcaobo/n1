#!/usr/bin/perl
@gn_platform_type = ("CB","GB","GF","GN","WB","WF","WQ","TS","U");

$pmPath = "gn_project";
chdir($pmPath);
@files = <*>;
$i = 0;
foreach $f (@files) {
	$result = 1;
	foreach $type (@gn_platform_type) {
		$result = $result && $f !~ /^$type.*\.mk$/;
	}
    next if ( $result );
    $f =~ /(.*)\.mk/;
    print $1;
    $i++;
    if ($i%3 == 0) {
        print "\n";
    } else {
        print " " x (26-length $f);
    }
}
if ($i%3 != 0) {
    print "\n";
}

exit 0;
