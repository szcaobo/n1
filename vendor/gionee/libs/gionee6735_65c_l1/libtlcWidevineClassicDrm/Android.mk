LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_MODULE := libtlcWidevineClassicDrm
LOCAL_SRC_FILES_64 := libtlcWidevineClassicDrm.so
LOCAL_SRC_FILES_32 := arm/libtlcWidevineClassicDrm.so
LOCAL_SHARED_LIBRARIES := libMcClient libstdc++
LOCAL_MULTILIB := both
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)
