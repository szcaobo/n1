# =============================================================================
#
# Makefile responsible for:
# - building two test binary - tlcmem_example dumpsecmemusage
# - building the TLC library - libsec_mem.so
#
# =============================================================================

# Do not remove this - Android build needs the definition
LOCAL_PATH	:= $(call my-dir)

ifeq ($(MTK_K64_SUPPORT), yes)
MOBICORE_LIB_PATH +=\
    $(LOCAL_PATH)/../mobicore/MobiCoreDriverLib/ClientLib/public \
    $(LOCAL_PATH)/../mobicore/common/MobiCore/inc
else
MOBICORE_LIB_PATH +=\
    $(LOCAL_PATH)/../mobicore/MobiCoreDriverLib/ClientLib/public \
    $(LOCAL_PATH)/../mobicore/common/MobiCore/inc
endif

# =============================================================================
# library tlcutilsLib
include $(CLEAR_VARS)

# Module name
LOCAL_MODULE	:= libsec_mem
LOCAL_MODULE_TAGS := debug eng optional

# Add your folders with header files here
LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/inc \
    $(LOCAL_PATH)/public \
    $(LOCAL_PATH)/../../trustlets/utils/Tlutils/public \
    $(LOCAL_PATH)/../../trustlets/utils/Drutils/public \
    $(MOBICORE_LIB_PATH)

# Add your source files here (relative paths)
LOCAL_SRC_FILES	+= tlcsecmem.cpp
# Need the MobiCore client library
LOCAL_SHARED_LIBRARIES := libMcClient
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_SHARED_LIBRARY)

# =============================================================================
# binary tlcutils
ifeq ($(strip $(TARGET_BUILD_VARIANT)), eng)
include $(CLEAR_VARS)

# Module name
LOCAL_MODULE	:= tlcmem_example
LOCAL_MODULE_TAGS := debug eng optional

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \
    $(LOCAL_PATH)/inc \
    $(LOCAL_PATH)/../../trustlets/utils/Tlutils/public \
    $(LOCAL_PATH)/../../trustlets/utils/Drutils/public \
    $(MOBICORE_LIB_PATH)

# Add your source files here
LOCAL_SRC_FILES	+= main.cpp

LOCAL_SHARED_LIBRARIES := libMcClient
LOCAL_SHARED_LIBRARIES += libsec_mem
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_EXECUTABLE)
endif

# =============================================================================
# binary tlcutils
ifeq ($(strip $(TARGET_BUILD_VARIANT)), eng)
include $(CLEAR_VARS)

# Module name
LOCAL_MODULE	:= dumpsecmemusage
LOCAL_MODULE_TAGS := debug eng optional

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/public \
    $(LOCAL_PATH)/inc \
    $(LOCAL_PATH)/../../trustlets/utils/Tlutils/public \
    $(LOCAL_PATH)/../../trustlets/utils/Drutils/public \
    $(MOBICORE_LIB_PATH)

# Add your source files here
LOCAL_SRC_FILES	+= dumpsecmemusage.cpp

LOCAL_SHARED_LIBRARIES := libMcClient
LOCAL_SHARED_LIBRARIES += libsec_mem
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_EXECUTABLE)
endif
