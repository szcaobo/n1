/*
 * Copyright (c) 2013 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */
 
#ifndef __TLMEMAPI_H__
#define __TLMEMAPI_H__

#if !defined(TRUSTEDAPP)
#include "TlApi/TlApiCommon.h"
#endif
#include "TlApi/TlApiError.h"

//#define SECMEM_DEBUG

/* Marshaled function parameters.
 * structs and union of marshaling parameters via TlApi.
 *
 * @note The structs can NEVER be packed !
 * @note The structs can NOT used via sizeof(..) !
 */

 /*
  * Function id definitions
  */
#define FID_DR_OPEN_SESSION     1
#define FID_DR_CLOSE_SESSION    2
#define FID_DR_INIT_DATA        3
#define FID_DR_EXECUTE          4
#define FID_DR_SYSTRACE         5

/* Marshaled function parameters.
 * structs and union of marshaling parameters via TlApi.
 *
 * @note The structs can NEVER be packed !
 * @note The structs can NOT used via sizeof(..) !
 */
#define QUERY_MAX_LEN       (8)
#define MAX_NAME_SZ         (32)

typedef struct {
    uint32_t    commandId;
    uint32_t    phy_addr;
    uint32_t    size;
    uint32_t    alignment;
    uint32_t    refcount;
    uint32_t    handle;
    uint32_t    id;
    uint8_t     owner[MAX_NAME_SZ];
} tlApimem_t, *tlApimem_ptr;

#endif // __TLAPIROT13_H__

