################################################################################
#
# <t-sdk Key Install Trustlet
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := TlKeyInstall


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------

TRUSTLET_UUID := 08110000000000000000000000000000
TRUSTLET_MEMTYPE := 2
TRUSTLET_NO_OF_THREADS := 1
TRUSTLET_SERVICE_TYPE := 3 # 2: service provider trustlet; 3: system trustlet
TRUSTLET_KEYFILE := pairVendorTltSig.pem            #only valid for service provider trustlets.
TRUSTLET_FLAGS := 0
TRUSTLET_INSTANCES := 10

#-------------------------------------------------------------------------------
# For 302A and later version
#-------------------------------------------------------------------------------
TBASE_API_LEVEL := 5
#4K
HEAP_SIZE_INIT := 4096
#10M
HEAP_SIZE_MAX := 1048576

#-------------------------------------------------------------------------------
# Files and include paths - Add your files here
#-------------------------------------------------------------------------------
KI_TRUSTZONE_PROTECT_BSP_PATH=../../../../../../../../../vendor/mediatek/proprietary/protect-bsp/platform/${ARCH_MTK_PLATFORM}/external/trustzone/trustlets
KI_TRUSTZONE_COMMON_SRC_PATH=../../../../../../../../../vendor/mediatek/proprietary/protect-bsp/trustzone/trustonic/trustlets

### Add include path here
INCLUDE_DIRS += \
    ../../../../../../../../../vendor/mediatek/proprietary/protect-bsp/trustzone/mtk/include \
    ../../../../../../../../../vendor/mediatek/proprietary/trustzone/mtk/include \
    ../../../../../../../../../vendor/trustonic/platform/${ARCH_MTK_PLATFORM}/trustzone/t-sdk/TlcSdk/Out/Public \
    ../../../../../../../../../vendor/mediatek/proprietary/external/include/key_install \
    ${KI_TRUSTZONE_PROTECT_BSP_PATH}/utils/Drutils/Out/Public \
    ${KI_TRUSTZONE_COMMON_SRC_PATH}/key_install/DrKeyInstall/Locals/Code/public \
    ${KI_TRUSTZONE_COMMON_SRC_PATH}/key_install/TlKeyInstall/Locals/Code/public

ifneq ("$(wildcard ../../../../../../../vendor/mediatek/proprietary/custom/${MTK_PROJECT}/drm/key.c)","")
SRC_C += ../../../../../../../vendor/mediatek/proprietary/custom/${MTK_PROJECT}/drm/key.c
else
ifneq ("$(wildcard ../../../../../../../vendor/mediatek/proprietary/custom/${TARGET_DEVICE}/drm/key.c)","")
SRC_C += ../../../../../../../vendor/mediatek/proprietary/custom/${TARGET_DEVICE}/drm/key.c
else
SRC_C += ../../../../../../../vendor/mediatek/proprietary/custom/common/drm/key.c
endif
endif

CUSTOMER_DRIVER_LIBS += ${TLKEYINSTALL_LIB_PATH}
CUSTOMER_DRIVER_LIBS += ${DRKEYINSTALL_LIB_PATH}
CUSTOMER_DRIVER_LIBS += $(DRUTILS_LIB_PATH)
#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)
include $(TLSDK_DIR)/trustlet.mk

