#!/bin/bash

t_sdk_root=${TBASE_BUILD_ROOT}

DRV_NAME=drWidevineModularDrm

if [ "$TEE_MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release"
  OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug"
  OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/Debug
  OPTIM=debug
fi

# For customer release - mode check
if [ "$TEE_MODE" == "Debug" ] && [ ! -f ${OUT_DIR}/${DRV_NAME}.axf ] ; then
    echo "${DRV_NAME}.axf is not existed in 'Debug' mode, changing to 'Release' mode"
    TEE_MODE=Release
    OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/release
    OPTIM=release
elif [ "$TEE_MODE" == "Release" ] && [ ! -f ${OUT_DIR}/${DRV_NAME}.axf ] ; then
    echo "${DRV_NAME}.axf is not existed in 'Release' mode, chaning to 'Debug' mode"
    TEE_MODE=Debug
    OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/${DRV_NAME}/debug
    OPTIM=debug
fi

# For customer release - file check
if [ ! -f ${OUT_DIR}/${DRV_NAME}.axf ] ; then
    echo "[ERROR] '${OUT_DIR}/${DRV_NAME}.axf' is still not existed! Please check it!"
    exit 0
fi

source ${t_sdk_root}/setup.sh

cd $(dirname $(readlink -f $0))

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export DRSDK_DIR_SRC=${COMP_PATH_DrSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
export DRSDK_DIR=${COMP_PATH_DrSdk}
echo "Running make..."	
make -f makefile.mk "$@"
