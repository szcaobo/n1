################################################################################
#
# <t-Base H264 Secure video decoder Trustlet
#
################################################################################


# output binary name without path or extension
OUTPUT_NAME := MtkH264SecVencTL


#-------------------------------------------------------------------------------
# MobiConvert parameters, see manual for details
#-------------------------------------------------------------------------------

TRUSTLET_UUID := 08090000000000000000000000000000
TRUSTLET_MEMTYPE := 2
TRUSTLET_NO_OF_THREADS := 1
TRUSTLET_SERVICE_TYPE := 3
TRUSTLET_KEYFILE := pairVendorTltSig.pem
TRUSTLET_FLAGS := 0
TRUSTLET_INSTANCES := 10 
TBASE_API_LEVEL := 3

#-------------------------------------------------------------------------------
# use generic make file
TRUSTLET_DIR ?= Locals/Code
TLSDK_DIR_SRC ?= $(TLSDK_DIR)

#-------------------------------------------------------------------------------
# standalone build
OUTPUT_ROOT := $(TEE_TRUSTLET_OUTPUT_PATH)/$(OUTPUT_NAME)
OUTPUT_PATH := $(OUTPUT_ROOT)/$(TEE_MODE)
TA_UUID ?= $(TRUSTLET_UUID)
TA_UUID := $(shell echo $(TA_UUID) | tr A-Z a-z)
TA_MEMTYPE ?= $(TRUSTLET_MEMTYPE)
TA_NO_OF_THREADS ?= $(TRUSTLET_NO_OF_THREADS)
TA_SERVICE_TYPE ?= $(TRUSTLET_SERVICE_TYPE)
TA_KEYFILE ?= $(TRUSTLET_KEYFILE)
TA_FLAGS ?= $(TRUSTLET_FLAGS)
TA_INSTANCES ?= $(TRUSTLET_INSTANCES)
TA_ADD_FLAGS ?= $(TRUSTLET_ADD_FLAGS)
TA_INTERFACE_VERSION ?= 0.0
TASDK_DIR_SRC ?= $(TLSDK_DIR_SRC)
GP_LEVEL :=
GP_UUIDKEYFILE :=
TA_BIN := $(OUTPUT_PATH)/$(TA_UUID).tlbin
TA_AXF := $(OUTPUT_PATH)/$(OUTPUT_NAME).axf
TA_LST2 := $(OUTPUT_PATH)/$(OUTPUT_NAME).lst2
CROSS=arm-none-eabi
READELF=$(CROSS_GCC_PATH_BIN)/$(CROSS)-readelf
READ_OPT=-a $(TA_AXF) > $(TA_LST2)
TA_PARAM := -servicetype $(TA_SERVICE_TYPE) \
            -numberofthreads $(TA_NO_OF_THREADS) \
            -numberofinstances $(TA_INSTANCES) \
            -memtype $(TA_MEMTYPE) \
            -flags $(TA_FLAGS) \
            -bin $(TA_AXF) \
            -output $(TA_BIN) \
            -keyfile $(TA_KEYFILE) \
            $(GP_LEVEL) \
            $(GP_UUIDKEYFILE) \
            -interfaceversion $(TA_INTERFACE_VERSION) \
            $(TA_ADD_FLAGS)

all : $(TA_BIN)

$(TA_BIN) : $(TA_AXF)
	$(info ******************************************)
	$(info ** READELF & MobiConvert *****************)
	$(info ******************************************)
	$(READELF) $(READ_OPT)
	$(JAVA_HOME)/bin/java -jar $(TASDK_DIR_SRC)/Bin/MobiConvert/MobiConvert.jar $(TA_PARAM) >$(TA_BIN).log
