#!/bin/bash

t_sdk_root=${TBASE_BUILD_ROOT}

export COMP_PATH_ROOT=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export DRUTILS_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/drutils
export DRUTILS_DIR=${COMP_PATH_ROOT}/../../../Drutils/Out
export DRSEC_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/drsec
export DRSEC_DIR=${COMP_PATH_ROOT}/../../../Drsec/Out
export SECVDECDR_OUT_DIR=${TEE_DRIVER_OUTPUT_PATH}/MtkH264SecVencDrv
export SECVDECDR_DIR=${COMP_PATH_ROOT}/../../../MtkH264SecVencDrv/Out

TL_NAME=MtkH264SecVencTL

if [ "$TEE_MODE" == "Release" ]; then
  echo -e "Mode\t\t: Release"
  OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Release
  OPTIM=release
else
  echo -e "Mode\t\t: Debug"
  OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/Debug
  OPTIM=debug
fi

# For customer release - mode check
if [ "$TEE_MODE" == "Debug" ] && [ ! -f ${OUT_DIR}/${TL_NAME}.axf ] ; then
    echo "${TL_NAME}.axf is not existed in 'Debug' mode, changing to 'Release' mode"
    TEE_MODE=Release
    OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/release
    OPTIM=release
elif [ "$TEE_MODE" == "Release" ] && [ ! -f ${OUT_DIR}/${TL_NAME}.axf ] ; then
    echo "${TL_NAME}.axf is not existed in 'Release' mode, chaning to 'Debug' mode"
    TEE_MODE=Debug
    OUT_DIR=${TEE_TRUSTLET_OUTPUT_PATH}/${TL_NAME}/debug
    OPTIM=debug
fi

# For customer release - file check
if [ ! -f ${OUT_DIR}/${TL_NAME}.axf ] ; then
    echo "[ERROR] '${OUT_DIR}/${TL_NAME}.axf' is still not existed! Please check it!"
    exit 0
fi

source ${t_sdk_root}/setup.sh

cd $(dirname $(readlink -f $0))

export TLSDK_DIR_SRC=${COMP_PATH_TlSdk}
export TLSDK_DIR=${COMP_PATH_TlSdk}
echo "Running make..."	
make -f makefile.mk "$@"
