#include "drStd.h"
#include "DrApi/DrApi.h"
#include "drError.h"
#include "mt_typedefs.h"
#include "dapc.h"
#define TAG "[dapc]"
#include "log.h"


unsigned char *devapc_ao_vaddr = 0;

static inline unsigned int readl(volatile unsigned int* addr)
{
    return *(volatile unsigned int*)addr;
}

static inline void writel(unsigned int val, volatile unsigned int* addr)
{
    *(volatile unsigned int*)addr = val;
}

int set_master_transaction(unsigned int master_index ,unsigned int transaction_type)
{
    volatile unsigned int* base = 0;
    unsigned int set_bit = 0;
 
    base = (unsigned int*) ((size_t)DEVAPC_MAS_SEC);

    if(master_index > 31)
        return -1;

    if (transaction_type == 0) {
        set_bit = ~(1 << master_index);
        writel(readl(base) & set_bit, base);
    } else if(transaction_type == 1) {
        set_bit = 1 << master_index;
        writel(readl(base) | set_bit, base);
    } else {
        return -2;
    }

    return 0;
}

void set_module_apc(unsigned int module, unsigned int domain_num , unsigned int permission_control)
{
    volatile unsigned int* base = 0;

    unsigned int clr_bit = 0x3 << ((module % MOD_NO_IN_1_DEVAPC) * 2);
    unsigned int set_bit = permission_control << ((module % MOD_NO_IN_1_DEVAPC) * 2);

    if(module >= DEVAPC_DEVICE_NUMBER) {
        drDbgPrintLnf("[DEVAPC] set_module_apc : device number %d exceeds the max number!\n", module);
        return;
    }

    if (domain_num == 0)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D0_APC_0 + (module/16) *4);
    }
    else if (domain_num == 1)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D1_APC_0 + (module/16) *4);
    }
    else if (domain_num == 2)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D2_APC_0 + (module/16) *4); 
    }
    else if (domain_num == 3)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D3_APC_0 + (module/16) *4);
    } 
    else if (domain_num == 4)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D4_APC_0 + (module/16) *4);
    }
    else if (domain_num == 5)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D5_APC_0 + (module/16) *4);
    }
    else if (domain_num == 6)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D6_APC_0 + (module/16) *4); 
    }
    else if (domain_num == 7)
    {
        base = (unsigned int*) ((unsigned int)DEVAPC_D7_APC_0 + (module/16) *4);
    }

     writel(readl(base) & ~clr_bit, base);
     writel(readl(base) | set_bit, base);

}

static int map_DAPC_Register()
{
    drApiResult_t ret;
    
    if((ret = drApiMapPhysicalBuffer((uint64_t)DEVAPC_AO_BASE, SIZE_4KB, MAP_HARDWARE, (void **)&DEVAPC_AO_VA_BUFFER)) != DRAPI_OK){
        drDbgPrintLnf("[DEVAPC] map DEVAPC_AO_BASE failed! ERROR: %d, phy addr : 0x%llx\n", ret, (uint64_t)DEVAPC_AO_BASE);
    }
      
    return 0;
}

int has_started=0;

int start_devapc()
{
    int module_index = 0;

    if (!has_started) {
        map_DAPC_Register();

        /*Enable Devapc*/

        writel(readl(DEVAPC_APC_CON) &  (0xFFFFFFFF ^ (1<<2)), DEVAPC_APC_CON);
        has_started = 1;
    }
}
