#ifndef __DISPLAY_TUI_H__
#define __DISPLAY_TUI_H__

#include "ddp_log.h"
#define DISP_TUI_OPTION_LCM_BUSY_POLLING
#define DISP_RDMA_INTERRUPT_ENABLE 0

int disp_tui_dump_backup_regs();
int disp_tui_init_backup_regs();
int disp_tui_reg_backup(void* addr);


static inline int disp_tui_poll(void* addr, unsigned int mask, unsigned int value, long timeout)
{
	volatile long i = 0;
	volatile unsigned int reg_val;
	int ret = 0;

	while(1) {
		reg_val = DISP_REG_GET(addr);

		//DDPMSG("poll%d 0x%p = 0x%x, msk=0x%x, val=0x%x\n", i, addr, reg_val, mask, value);
		
		if((reg_val & mask) == value) {
			ret = 0;
			break;
		}
		if(timeout != -1 && i > timeout) {
			ret = -1;
			break;
		}

		i++;
	}
	return ret;
}

static inline int disp_tui_poll_nz(void* addr, unsigned int mask, long timeout)
{
	volatile long i = 0;
	volatile unsigned int reg_val;
	int ret = 0;

	while(1) {
		reg_val = DISP_REG_GET(addr);

		//DDPMSG("poll%d 0x%p = 0x%x, msk=0x%x, val=0x%x\n", i, addr, reg_val, mask, value);
		
		if((reg_val & mask)) {
			ret = 0;
			break;
		}
		if(timeout != -1 && i > timeout) {
			ret = -1;
			break;
		}

		i++;
	}
	return ret;
}

int disp_tui_init();
int disp_tui_enter();
int disp_tui_leave();

int disp_tui_pan_display(unsigned long offset);
int disp_tui_draw_buffer(uint8_t *va, int w, int h, int pitch, int Bpp);

#endif
