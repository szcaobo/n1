/*
 * Copyright (c) 2013-2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef __DRDRMCOMMON_H__
#define __DRDRMCOMMON_H__


#include "DrApi/DrApi.h"
#include "TlApi/TlApiError.h"


/* Priority definitions */
#define EXCH_PRIORITY       MAX_PRIORITY
#define IPCH_PRIORITY       (MAX_PRIORITY-1)
#define DCIH_PRIORITY       (MAX_PRIORITY-1)


/* Kernel exceptions */
#define TRAP_UNKNOWN        ( 0)    /* unknown exception. */
#define TRAP_SYSCALL        ( 1)    /* invalid syscall number. */
#define TRAP_SEGMENTATION   ( 2)    /* illegal memory access. */
#define TRAP_ALIGNMENT      ( 3)    /* misaligned memory access. */
#define TRAP_UNDEF_INSTR    ( 4)    /* undefined instruction. */

/* System root and SP ids*/
#define ROOTID_SYSTEM       0xFFFFFFFE
#define SPID_SYSTEM         0xFFFFFFFE

/* Driver version (used by the IPC handler) */
#define DRIVER_VERSION       1

/* Thread numbers */
#define DRIVER_THREAD_NO_EXCH      1
#define DRIVER_THREAD_NO_IPCH      2
#define DRIVER_THREAD_NO_DCIH      3



#define THREADID_THREADNO_SHIFT     16
#define THREADID_TASKID_MASK        ((1 << THREADID_THREADNO_SHIFT) - 1)
#define THREADID_THREADNO_MASK      (~THREADID_TASKID_MASK)
#define GET_THREADNO(threadid)      (threadid >> THREADID_THREADNO_SHIFT)

/**
 * Maximum allowed length is 512KB. Some of the available pages
 * are used for mapping various physical memory locations, etc..
 */
#define DR_MAX_INPUT_LENGTH         0x80000

/* Physical Address */
typedef uint64_t phys_addr_t;

/* Macro and relevat definitions for stack */
#define STACK_DBG_INIT_VALUE        0xCC
#define fillStack(_name_,_val_) \
    memset(_STACK_ARRAY_VAR(_name_), _val_, getStackSize(_name_))
#define clearStack(_name_)      fillStack(_name_,STACK_DBG_INIT_VALUE)
#define TASKID_GET_THREADID(taskid,threadno) ((threadid_t)((taskid) | ((threadno) << THREADID_THREADNO_SHIFT)))
#define FUNC_PTR(func)              VAL2PTR( PTR2VAL( func ) )
#define getStackTop(_name_)         GET_STACK_TOP(_name_)

/* Driver virtual address definitions */
#define DR_ADDR_SEC_ADDR_BASE       (0x40000)  /* for secure mappings */
#define DR_ADDR(x)                  (DR_ADDR_SEC_ADDR_BASE + (x))
#define DR_VA_RESERVED              DR_ADDR(0x1000)

//----------------------------------------------------------------------------
#define SIZE_TO_MASK(_size_)      ( ~((_size_) - 1) )

#define PTR_OFFS_T(ptr,offset,addr_type,addr_int_type)      ( (addr_type)( (addr_int_type)(ptr) + (offset) ) )
#define PTR_AND_T(ptr,mask,addr_type,addr_int_type)         ( (addr_type)( (addr_int_type)(ptr) & (mask) ) )
#define PTR_MASK_T(p,m,addr_type,addr_int_type)             PTR_AND_T(p,m,addr_type,addr_int_type)

#define PTR_OFFS(ptr,offset)      PTR_OFFS_T(ptr,offset,addr_t,word_t)

#define SHIFT_1MB                 (20U) /**<  SIZE_1MB is 1 << SHIFT_1MB aka. 2^SHIFT_1MB. */
#define SIZE_1MB                  (1 << SHIFT_1MB) /**< Size of 1 KiB. */

//----------------------------------------------------------------------------
#define LOWU32(u64)     ((uint32_t)((u64) & 0x00000000FFFFFFFF))
#define HIGHU32(u64)    ((uint32_t)((u64) >> 32))

//------------------------------------------------------------------------------
// assume 4KB pages:   0->0, 1->0, 4095->0, 4096->4096
#define /*addr_type*/_alignDown(addr,/*word_t*/pageSize,addr_type,addr_int_type) \
    (PTR_MASK_T((addr),SIZE_TO_MASK(pageSize),addr_type,addr_int_type) )

//------------------------------------------------------------------------------
// for a given address, this function returns the start address of the page
// the address is located in in. Basically, this is the same as _alignDown(),
// we just add this function for completness as counterpart of _getPageEnd().
//
// assume 4KB pages:   0->0, 1->0, 4095->0, 4096->4096
#define /*phys_addr_t*/_getPageStart64(/*phys_addr_t*/addr,/*word_t*/pageSize) \
        (_alignDown(addr,pageSize,phys_addr_t,uint64_t))

//------------------------------------------------------------------------------
// for a given address, this function returns the offset into the page of a
// given size
// assume 4KB pages:   0->0, 1->1, 4095->4095, 4096->0, 4097->1
#define /*word_t*/ _getOffsetIntoPage64(/*phys_addr_t*/addr,/*word_t*/pageSize) \
        ((word_t)PTR_MASK_T( (addr),~SIZE_TO_MASK(pageSize),phys_addr_t,uint64_t) )

/* Thread specific definitions */
#define threadid_is_null(threadid)  (NILTHREAD == threadid)  /* returns true if thread id is NILTHREAD */


#endif // __DRDRMCOMMON_H__

