/*
 * Copyright (c) 2014 TRUSTONIC LIMITED
 * All rights reserved
 *
 * The present software is the confidential and proprietary information of
 * TRUSTONIC LIMITED. You shall not disclose the present software and shall
 * use it only in accordance with the terms of the license agreement you
 * entered into with TRUSTONIC LIMITED. This software may be subject to
 * export or import laws in certain countries.
 */

#ifndef TLC_FLOAT_H_
#define TLC_FLOAT_H_

#include "MobiCoreDriverApi.h"

#define LOG_TAG "tlcFloat"


typedef union 
{
    float  f;
    double d;
} tlcFloat_param_t;

mcResult_t tlcOpen(mcSpid_t spid, uint8_t* pTAData, uint32_t nTASize);
mcResult_t tlcFloatInit(tlcFloat_param_t params[4]);
mcResult_t tlcFloatAdd(tlcFloat_param_t params[4]);
mcResult_t tlcFloatMul(tlcFloat_param_t params[4]);
mcResult_t tlcFloatSqrt(tlcFloat_param_t params[4]);
void tlcClose(void);

#endif // TLC_FLOAT_H_
