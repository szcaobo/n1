LOCAL_PATH:=$(call my-dir)
include $(CLEAR_VARS)
#LOCAL_ARM_MODE:=arm

LOCAL_SHARED_LIBRARIES:= libc libcutils libcustom_nvram libnvram_platform

ifneq ($(strip $(MTK_BASIC_PACKAGE)),yes)
  LOCAL_SHARED_LIBRARIES+=libnvram_sec
endif
LOCAL_SRC_FILES:= \
	libnvram.c
#	libnvram.c
LOCAL_C_INCLUDES:= \
      $(MTK_PATH_SOURCE)/external/nvram/libnvram \
      $(MTK_PATH_SOURCE)/external/nvram/libfile_op \
      $(MTK_PATH_SOURCE)/external/seclib \


# pass the include path of system/core/include/private/android_filesystem_config.h
LOCAL_C_INCLUDES += system/core/include/private

LOCAL_MODULE:=libnvram
LOCAL_MODULE_TAGS := optional
LOCAL_PRELINK_MODULE:= false
include $(BUILD_SHARED_LIBRARY)


