#include "../cfgdefault/CFG_Custom1_Default.h"
#include "../cfgdefault/CFG_GPS_Default.h"
#include "../cfgdefault/CFG_PRODUCT_INFO_Default.h"
#include "../cfgdefault/CFG_WIFI_Default.h"
/* Gionee yang_yang 20150313 add for CR01454126 begin */
#if defined(CONFIG_GN_BSP_PS_STATIC_CALIBRATION)
#include "../cfgdefault/CFG_PS_CALI_Default.h"
#endif
/* Gionee yang_yang 20150313 add for CR01454126 end */
