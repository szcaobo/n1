/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary
**
** File:
**     EffectHandle.java
**
** Description:
**     Java EffectHandle class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion.android;

import com.immersion.Device;
import com.immersion.ImmVibeAPI;

/**
 * Encapsulates an effect handle representing a playing or paused effect.
 */
public class EffectHandle extends com.immersion.EffectHandle
{
    /**
     * Initializes this effect handle object.
     */
    protected EffectHandle(int deviceHandle, int effectHandle)
    {
        super(deviceHandle, effectHandle);
    }
    
    /**
     * Calls {@link #destroyStreamingEffect destroyStreamingEffect} upon object finalization.
     *
     * @throws  RuntimeException
     *                          The detail message of the exception may consist
     *                          of the following values:
     *                          <dl>
     *                              <dt>"VIBE_E_INVALID_ARGUMENT"</dt>
     *                              <dd>The device has been closed by a previous
     *                                  call to
     *                                 {@link Device#close Device.close}.</dd>
     *                              <dt>"VIBE_E_FAIL"</dt>
     *                              <dd>Error destroying the Streaming
     *                                  effect.</dd>
     *                              <dt>"VIBE_E_SERVICE_BUSY"</dt>
     *                              <dd>The Player Service is busy and could not
     *                                  complete the request.</dd>
     *                          </dl>
     */
    protected void finalize()
    {
        if ((ImmVibe.VIBE_INVALID_DEVICE_HANDLE_VALUE != deviceHandle) &&
            (ImmVibe.VIBE_INVALID_EFFECT_HANDLE_VALUE != effectHandle) &&
            ((effectHandle & (ImmVibeAPI.VIBE_EFFECT_TYPE_STREAMING << 28)) == (ImmVibeAPI.VIBE_EFFECT_TYPE_STREAMING << 28)))
        {
            ImmVibe.getInstance().destroyStreamingEffect(deviceHandle, effectHandle);
        }
    }
}
