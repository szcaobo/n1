#Gionee <Amigo_Vibrator> <fengxb> <2015-04-01> add for CR01459973 start
#    File Description: <Amigo_Vibrator> Support for VibratorService.
#
#    Author:            fengxb
#
#    Create Date:     2015/04/01
#
#    Change List:      CR01459973

#LOCAL_PATH := frameworks/base/core/java/com/immersion
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/defaultHapticTheme.ivt:system/media/defaultHapticTheme.ivt

#Gionee <Amigo_Vibrator> <fengxb> <2015-04-01> add for CR01459973 end
