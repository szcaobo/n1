/*
** =============================================================================
** Copyright (c) 2009-2013  Immersion Corporation. All rights reserved.
**                          Immersion Corporation Confidential and Proprietary.
**
** File:
**     IVTElement.java
**
** Description:
**     Java IVTElement class declaration.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Base class for elements of a Timeline effect in an IVT buffer.
 */
public class IVTElement
{
    private int     type;
    private int     time;

    /**
     * Initializes this IVT element object.
     *
     * @param   type            Type of this IVT element, one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_PERIODIC}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_MAGSWEEP}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_WAVEFORM}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_REPEAT}</li>
     *                          </ul>
     * @param   time            Time in milliseconds of this IVT element within
     *                          the Timeline effect.
     */
    IVTElement(int type, int time)
    {
        setType(type);
        setTime(time);
    }

    /**
     * Instantiates this IVT element according to the given parameters. This
     * method is not intended to be called by client applications.
     *
     * @param   element         Parameters defining this IVT element.
     * @return                  An instance of the appropriate subclass of
     *                          {@link IVTElement}.
     */
    static IVTElement newIVTElement(int[] element)
    {
        IVTElement retVal = null;

        if (element != null)
        {
            switch(element[0])
            {
                case ImmVibe.VIBE_ELEMTYPE_MAGSWEEP:
                    retVal = new IVTMagSweepElement(element[1], new MagSweepEffectDefinition(element[2], element[3], element[4], element[5], element[6], element[7], element[8], element[9]));
                break;

                case ImmVibe.VIBE_ELEMTYPE_PERIODIC:
                    retVal = new IVTPeriodicElement(element[1], new PeriodicEffectDefinition(element[2], element[3], element[4], element[5], element[6], element[7], element[8], element[9], element[10]));
                break;

                case ImmVibe.VIBE_ELEMTYPE_WAVEFORM:
                    retVal = new IVTWaveformElement(element[1], new WaveformEffectDefinition(null, element[3], element[4], element[5], element[6], element[7]));
                break;

                case ImmVibe.VIBE_ELEMTYPE_REPEAT:
                    retVal = new IVTRepeatElement(element[1], element[2], element[3]);
                break;
            }
        }
        return retVal;
    }

    /**
     * Sets the type of this IVT element.
     *
     * @param   type            Type of this IVT element, one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_PERIODIC}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_MAGSWEEP}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_WAVEFORM}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_REPEAT}</li>
     *                          </ul>
     */
    void setType(int type)
    {
        this.type = type;
    }

    /**
     * Gets the type of this IVT element.
     *
     * @return                  Type of this IVT element, one of
     *                          <ul>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_PERIODIC}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_MAGSWEEP}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_WAVEFORM}</li>
     *                              <li>{@link ImmVibe#VIBE_ELEMTYPE_REPEAT}</li>
     *                          </ul>
     */
    public int getType()
    {
        return type;
    }

    /**
     * Sets the time of this IVT element.
     *
     * @param   time            Time in milliseconds of this IVT element within
     *                          the Timeline effect.
     */
    public void setTime(int time)
    {
        this.time = time;
    }

    /**
     * Gets the time of this IVT element.
     *
     * @return                  Time in milliseconds of this IVT element within
     *                          the Timeline effect.
     */
    public int getTime()
    {
        return time;
    }

    /**
     * Gets a buffer containing the parameters of this IVT element.
     */
    public int[] getBuffer()
    {
        return null;
    }
}
