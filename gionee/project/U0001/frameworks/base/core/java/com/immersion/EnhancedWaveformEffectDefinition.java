/*
** =============================================================================
** Copyright (c) 2014  Immersion Corporation. All rights reserved.
**                     Immersion Corporation Confidential and Proprietary.
**
** File:
**     EnhancedWaveformEffectDefinition.java
**
** Description:
**     Java ImmVibe class declaration for Android.
**
** Merge:
**    Gionee BSP1 ningyd 20150303 modify for CR01452914 immersion vibrate
** =============================================================================
*/

package com.immersion;

/**
 * Encapsulates the parameters of a Enhanced Waveform effect.
 */
public class EnhancedWaveformEffectDefinition
{
    private byte[]    data;
    private int       sampleRate;
    private int       format;
    private int       magnitude;
    private int       actuatorIndex;
    private int       secureMode;

    /**
     * Initializes this Enhanced Waveform effect with the given parameters.
     *
     * @param   data            The data format can be one of the supported
     *                          formats depending on the secureMode parameter:
     *                          1) {@link ImmVibe#VIBE_SECURITY_MODE_NONE}
     *                          ------------------------
     *                          | PCM  data            |
     *                          ------------------------
     *                          2) {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}
     *                          ------------------------------------------
     *                          | offset | block | PCM data              |
     *                          ------------------------------------------.
     *                          PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     * @param   format          Waveform data format. See valid data format,
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8},
     *                          {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}
     *                          and {@link ImmVibeAPI#VIBE_WAVEFORMAT_PCM8}.
     * @param   magnitude       Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive. When set to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          the PCM data samples are not attenuated.
     *                          Magnitude values less than
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          serve to attenuate the PCM data samples.
     * @param   actuatorIndex   Index of the actuator on which to play the
     *                          effect. The actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     * @param   secureMode      The secure mode of the waveform effect data.
     *                          The secure mode can be one of supported modes
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_NONE} or
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}.
     */
    public EnhancedWaveformEffectDefinition(byte[] data,
                                    int sampleRate,
                                    int format,
                                    int magnitude,
                                    int actuatorIndex,
                                    int secureMode)
    {
        setData(data);
        setSampleRate(sampleRate);
        setFormat(format);
        setMagnitude(magnitude);
        setActuatorIndex(actuatorIndex);
        setSecureMode(secureMode);
    }

    /**
     * Sets the data of this Enhanced Waveform effect definition.
     *
     * @param   data            The data format can be one of the supported
     *                          formats depending on the secureMode parameter:
     *                          1) {@link ImmVibe#VIBE_SECURITY_MODE_NONE}
     *                          ------------------------
     *                          | PCM  data            |
     *                          ------------------------
     *                          2) {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}
     *                          ------------------------------------------
     *                          | offset | block | PCM data              |
     *                          ------------------------------------------.
     *                          The offset should be encoded in native-endian.
     *                          PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     */
    public void setData(byte[] data)
    {
        this.data = data;
    }

    /**
     * Gets the data of this Enhanced Waveform effect definition.
     *
     * @return  data            The data format can be one of the supported
     *                          formats depending on the secureMode parameter:
     *                          1).{@link ImmVibe#VIBE_SECURITY_MODE_NONE}
     *                          ------------------------
     *                          | PCM  data            |
     *                          ------------------------
     *                          2).{@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}
     *                          ------------------------------------------
     *                          | offset | block | PCM data              |
     *                          ------------------------------------------.
     *                          The offset should be encoded in native-endian.
     *                          PCM data defining the actuator drive signal for
     *                          the Waveform effect. The data is formatted in
     *                          the same way as the PCM data in a WAV file with
     *                          the exception that only 8-bit and 16-bit mono
     *                          (not stereo) samples are supported.
     */
    public byte[] getData()
    {
        return data;
    }

    /**
     * Gets the PCM data size of this Waveform effect definition.
     *
     * @return                  Size of the PCM data in bytes. The size will be
     *                          greater than zero. The maximum supported size is
     *                          16 megabytes, or less if limited by platform
     *                          constraints.
     */
    public int getDataSize()
    {
        return data.length;
    }

    /**
     * Sets the sampling rate of this Waveform effect definition.
     *
     * @param   sampleRate      Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     */
    public void setSampleRate(int sampleRate)
    {
        this.sampleRate = sampleRate;
    }

    /**
     * Gets the sampling rate of this Waveform effect definition.
     *
     * @return                  Sampling rate of PCM data in Hertz (number of
     *                          samples per second).
     */
    public int getSampleRate()
    {
        return sampleRate;
    }

    /**
     * Sets the data format of this enhanced Waveform effect definition.
     *
     * @param   format          waveform data format.
     */

    public void setFormat(int format)
    {
        this.format = format;
    }

    /**
     * Gets the bit depth of this Waveform effect definition.
     *
     * @return                  waveform data format.
     */
    public int getFormat()
    {
        return format;
    }

    /**
     * Sets the magnitude of this Waveform effect definition.
     *
     * @param   magnitude       Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive. When set to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          the PCM data samples are not attenuated.
     *                          Magnitude values less than
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          serve to attenuate the PCM data samples.
     */
    public void setMagnitude(int magnitude)
    {
        this.magnitude = magnitude;
    }

    /**
     * Gets the magnitude of this Waveform effect definition.
     *
     * @return                  Magnitude of the effect. The effect magnitude
     *                          goes from
     *                          {@link ImmVibe#VIBE_MIN_MAGNITUDE}
     *                          to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          inclusive. When set to
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          the PCM data samples are not attenuated.
     *                          Magnitude values less than
     *                          {@link ImmVibe#VIBE_MAX_MAGNITUDE},
     *                          serve to attenuate the PCM data samples.
     */
    public int getMagnitude()
    {
        return magnitude;
    }

    /**
     * Sets the actuator index of this Enhanced Waveform effect definition.
     *
     * @param   actuatorIndex   Index of the actuator on which to play the
     *                          effect. The actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public void setActuatorIndex(int actuatorIndex)
    {
        this.actuatorIndex = actuatorIndex;
    }

    /**
     * Gets the actuator index of this Enhanced Waveform effect definition.
     *
     * @return                  Index of the actuator on which to play the
     *                          effect. The actuator index goes from zero to
     *                          {@link ImmVibe#VIBE_MAX_LOGICAL_DEVICE_COUNT}.
     */
    public int getActuatorIndex()
    {
        return actuatorIndex;
    }

    /**
     * Sets the secure mode of the enhance waveform effect data.
     *
     * @param   secureMode      The secure mode of the waveform effect data.
     *                          This can be one of of the supported modes:
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_NONE} or
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}.
     */
    public void setSecureMode(int secureMode)
    {
        this.secureMode = secureMode;
    }

    /**
     * Sets the secure mode of the enhance waveform effect definition.
     *
     * @return   secureMode     The secure mode of the waveform effect data.
     *                          This can be one of the supported modes:
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_NONE} or
     *                          {@link ImmVibe#VIBE_SECURITY_MODE_AES128CTR}.
     */
    public int getSecureMode()
    {
        return secureMode;
    }

}
