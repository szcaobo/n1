/*************************************************************************************
 * 
 * Description:
 * 	Defines API to control cpu performance.
 *    The cpu control codes need be changed according to chipset.
 *    As the cpu control is different in different chipset. 
 *    At present we only is valid for MT6795.
 *
 * Note:
 *	If enable high performance function, will enable all cpu core with highest 
 *	cpu frequence. It will increase the oppotunity of thermal throttle. If you
 *	don't wana get high performance, please disable the function in time.
 *
 * <CR01492399>
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2015/5/29
 *
 *************************************************************************************/


#define LOG_TAG "MtkCam/GnPerfService"
#include <mtkcam/Log.h>

#include "inc/GnPerfService.h"

#define MY_LOGV(fmt, arg...)       CAM_LOGV("[%s] "fmt, __func__, ##arg)
#define MY_LOGD(fmt, arg...)       CAM_LOGD("[%s] "fmt, __func__, ##arg)
#define MY_LOGI(fmt, arg...)       CAM_LOGI("[%s] "fmt, __func__, ##arg)
#define MY_LOGW(fmt, arg...)       CAM_LOGW("[%s] "fmt, __func__, ##arg)
#define MY_LOGE(fmt, arg...)       CAM_LOGE("[%s] "fmt, __func__, ##arg)
#define MY_LOGA(fmt, arg...)       CAM_LOGA("[%s] "fmt, __func__, ##arg)
#define MY_LOGF(fmt, arg...)       CAM_LOGF("[%s] "fmt, __func__, ##arg)

#define PERF_LIB_FULLNAME        "/system/lib/libperfservicenative.so"
#define STR_FUNC_REG             "PerfServiceNative_userReg"
#define STR_FUNC_UNREG           "PerfServiceNative_userUnreg"
#define STR_FUNC_REG_SCN_CFG	 "PerfServiceNative_userRegScnConfig"
#define STR_FUNC_ENABLE  		 "PerfServiceNative_userEnable"
#define STR_FUNC_DISABLE         "PerfServiceNative_userDisable"

#define CPU_CORE_NUM	8
#define CPU_FREQ		1300000

namespace android {

/********************************************************************************
 *********************************************************************************/

GnPerfService::
GnPerfService()
	: mPerfLib(NULL)
	, mPerfEnable(NULL)
	, mPerfDisable(NULL)
	, mPerfRegScnConfig(NULL)
	, mPerfReg(NULL)
	, mPerfUnreg(NULL)
	, mScenHandle(-1)
	, mPerfServiceEnabled(false)
{
	initPerf();
}

GnPerfService::
~GnPerfService()
{
	uninitPerf();
}

bool
GnPerfService::
enableHighPerformance() 
{
	bool res = false;

	res = enablePerfService();
	if (!res) {
		MY_LOGE("Failed to enable hight performance.");
	}

	return res;
}

bool
GnPerfService::
disableHighPerformance() 
{
	bool res = false;

	res = disablePerfService();
	if (!res) {
		MY_LOGE("Failed to diable hight performance.");
	}

	return res;
}


bool
GnPerfService::
initPerf()
{
	bool res = false;

	Mutex::Autolock _l(mPerfLock);
	
    if(!mPerfLib) {
        mPerfLib = ::dlopen(PERF_LIB_FULLNAME, RTLD_NOW);
        if  (! mPerfLib) {
            char const *err_str = ::dlerror();
            MY_LOGE("dlopen: %s error=%s", PERF_LIB_FULLNAME, (err_str ? err_str : "unknown"));
			
			return res;
        }
    }

    mPerfEnable = reinterpret_cast<funcEnable>(dlsym(mPerfLib, STR_FUNC_ENABLE));
    if(mPerfEnable == NULL) {
        MY_LOGE("cannot get %s", STR_FUNC_ENABLE);
	}

    mPerfDisable = reinterpret_cast<funcDisable>(dlsym(mPerfLib, STR_FUNC_DISABLE));
    if(mPerfDisable == NULL) {
        MY_LOGE("cannot get %s", STR_FUNC_DISABLE);
	}

    mPerfReg = reinterpret_cast<funcReg>(dlsym(mPerfLib, STR_FUNC_REG));
    if(mPerfReg == NULL) {
        MY_LOGE("cannot get %s", STR_FUNC_REG);
		
		return res;
    }

	mPerfUnreg = reinterpret_cast<funcUnreg>(dlsym(mPerfLib, STR_FUNC_UNREG));
    if(mPerfUnreg == NULL) {
        MY_LOGE("cannot get %s", STR_FUNC_UNREG);
		
		return res;
    }

	mPerfRegScnConfig = reinterpret_cast<funcRegScnConfig>(dlsym(mPerfLib, STR_FUNC_REG_SCN_CFG));
    if(mPerfRegScnConfig == NULL) {
        MY_LOGE("cannot get %s", STR_FUNC_REG_SCN_CFG);
		
		return res;
    }

    if(mScenHandle == - 1) {
		//First param is cpu core number. 
		// Second param is cpu freq. if you don't care cpu freq, you can set 0. 
		// It reduces the opportunity of thermal throttle. 
        mScenHandle = mPerfReg(CPU_CORE_NUM, CPU_FREQ);
        if(mScenHandle == -1) {
            MY_LOGE("register scenario failed");
			return res;
        } 
    }

	res = true;

	return res;
}

bool
GnPerfService::
uninitPerf()
{
	bool res = true;

	Mutex::Autolock _l(mPerfLock);

    if( !mPerfLib ) {
        MY_LOGE("no lib");

		res = false;
        return res;
    }
    
    if(mScenHandle != -1) {
        if(mPerfDisable != NULL) {
            mPerfDisable(mScenHandle);
        } 

		if (mPerfUnreg != NULL) {
			mPerfUnreg(mScenHandle);
		}

		mScenHandle = -1;
    }

	::dlclose(mPerfLib);

	return res;
}

bool
GnPerfService::
enablePerfService()
{
	bool res = true;

	Mutex::Autolock _l(mPerfLock);

	if(!mPerfEnable) {
        MY_LOGE("no func");

		res = false;
        return res;
    }

	if (mPerfServiceEnabled) {
		MY_LOGD("Have already enable the PerfService");
        return res;
	}
  
    if(mScenHandle != -1) {
        MY_LOGD("enable PerfService, mScenHandle %d",  mScenHandle);
        mPerfEnable((MUINT32)mScenHandle);
		mPerfServiceEnabled = true;
    }

	return res;
}

bool
GnPerfService::
disablePerfService()
{
	bool res = true;

	Mutex::Autolock _l(mPerfLock);

	if(!mPerfDisable) {
        MY_LOGE("no func");

        return res;
    }
    
    if(mScenHandle != -1 && mPerfServiceEnabled) {
       	mPerfDisable(mScenHandle);
	   	mPerfServiceEnabled = false;
    }
	
	return res;
}

bool
GnPerfService::
enableForceToBigCore()
{
	bool res = false;
	int s = 0;
	int j = 0;
	int cpu_msk = 0xFF; //Force run on big-core
	cpu_set_t cpuset;
	cpu_set_t cpuold;
	cpu_set_t cpubackup;

    MY_LOGD("Flow CPUAffinity Set CPU Affinity Mask= %d\n", cpu_msk);
    
    CPU_ZERO(&cpuset);

    for(unsigned int Msk = 1, cpu_no = 0; Msk < 0xFF; Msk <<= 1, cpu_no++) {
        if(Msk&cpu_msk) {
            CPU_SET(cpu_no, &cpuset);
        }
    }

    s = mt_sched_getaffinity(gettid(), sizeof(cpu_set_t), &cpuold, &cpubackup);
    if (s <= 0) {
        MY_LOGD("Flow CPUAffinity Get fail");
        return res;
    } else {
        MY_LOGD("Flow CPUAffinity Thread %d Current CPU Affinity %08lx Backup Affinity %08lx", gettid(), cpuold.__bits[0], cpubackup.__bits[0]);
    }

    s = mt_sched_setaffinity(gettid(), sizeof(cpu_set_t), &cpuset);  
    if (s != 0) {
        MY_LOGD("Flow CPUAffinity Set fail");
        return res;
    } else {
        s = mt_sched_getaffinity(gettid(), sizeof(cpu_set_t), &cpuset, &cpubackup); 
        MY_LOGD("Flow CPUAffinity Thread %d New CPU Affinity %08lx Backup Affinity %08lx", gettid(), cpuset.__bits[0], cpubackup.__bits[0]);
    }

	res = true;
	return res;
}

bool
GnPerfService::
disableForceToBigCore()
{
	bool res = false;
	int status;

    pid_t tid = gettid();
	status = mt_sched_exitaffinity(gettid());
	if(!status) {
    	res = true;
		
		MY_LOGD("Flow CPUAffinity Exit affinity to CPU1 successfully, status=%d\n", status);
	} else {
		MY_LOGD("Flow CPUAffinity Exit affinity to CPU1 FAIL, status=%d, tid=%d\n", status, tid);
	} 

	return res;
}

};

