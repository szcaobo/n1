/* 
 * Description:
 *		Add for CR01429061,  refocus shot.
 * 
 * Author: zhangpj
 * Date	: 2015-01-18
 */
 

#define LOG_TAG "MtkCam/RefocusShot"
//
#include <mtkcam/Log.h>
#include <mtkcam/common.h>
//
#include <mtkcam/exif/IDbgInfoContainer.h>
//
#include <mtkcam/hwutils/CameraProfile.h>
using namespace CPTool;
//
#include <mtkcam/hal/IHalSensor.h>
//
#include <mtkcam/camshot/ICamShot.h>
#include <mtkcam/camshot/ISingleShot.h>
//
#include <mtkcam/exif/IBaseCamExif.h>
//
#include <Shot/IShot.h>
//
#include "ImpShot.h"
#include "RefocusShot.h"
//
using namespace android;
using namespace NSShot;

#define SHUTTER_TIMING (NSCamShot::ECamShot_NOTIFY_MSG_EOF)
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)(%s)[%s] "fmt, ::gettid(), getShotName(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#define FUNC_START  MY_LOGD("+")
#define FUNC_END    MY_LOGD("-")
#define FUNC_NAME   MY_LOGD("")


bool RefocusShot::IsFinalImage = false;

/******************************************************************************
 *
 ******************************************************************************/
extern "C"
sp<IShot>
createInstance_RefocusShot(
    char const*const    pszShotName, 
    uint32_t const      u4ShotMode, 
    int32_t const       i4OpenId
)
{
    sp<IShot>       pShot = NULL;
    sp<RefocusShot>     pImpShot = NULL;
    //
    //  (1.1) new Implementator.
    pImpShot = new RefocusShot(pszShotName, u4ShotMode, i4OpenId);
    if  ( pImpShot == 0 ) {
        CAM_LOGE("[%s] new RefocusShot", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (1.2) initialize Implementator if needed.
    if  ( ! pImpShot->onCreate() ) {
        CAM_LOGE("[%s] onCreate()", __FUNCTION__);
        goto lbExit;
    }
    //
    //  (2)   new Interface.
    pShot = new IShot(pImpShot);
    if  ( pShot == 0 ) {
        CAM_LOGE("[%s] new IShot", __FUNCTION__);
        goto lbExit;
    }
    //
lbExit:
    //
    //  Free all resources if this function fails.
    if  ( pShot == 0 && pImpShot != 0 ) {
        pImpShot->onDestroy();
        pImpShot = NULL;
    }
    //
    return  pShot;
}


/******************************************************************************
 *  This function is invoked when this object is firstly created.
 *  All resources can be allocated here.
 ******************************************************************************/
bool
RefocusShot::
onCreate()
{
#warning "[TODO] RefocusShot::onCreate()"
    bool ret = true;
    mpIMemDrv =  IMemDrv::createInstance();
    if (mpIMemDrv == NULL)
    {
        MY_LOGE("g_pIMemDrv is NULL \n");
        return 0;
    }

    mpIImageBufAllocator =  IImageBufferAllocator::getInstance();
    if (mpIImageBufAllocator == NULL)
    {
        MY_LOGE("mpIImageBufAllocator is NULL \n");
        return 0;
    }
	
    return ret;
}


/******************************************************************************
 *  This function is invoked when this object is ready to destryoed in the
 *  destructor. All resources must be released before this returns.
 ******************************************************************************/
void
RefocusShot::
onDestroy()
{
#warning "[TODO] RefocusShot::onDestroy()"
}


/******************************************************************************
 *
 ******************************************************************************/
RefocusShot::
RefocusShot(
    char const*const pszShotName, 
    uint32_t const u4ShotMode, 
    int32_t const i4OpenId
)
    : ImpShot(pszShotName, u4ShotMode, i4OpenId)
{
    FUNC_NAME;
}


/******************************************************************************
 *
 ******************************************************************************/
RefocusShot::
~RefocusShot()
{
    FUNC_NAME;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
RefocusShot::
sendCommand(
    uint32_t const  cmd, 
    MUINTPTR const  arg1, 
    uint32_t const  arg2,
    uint32_t const  arg3
)
{
    AutoCPTLog cptlog(Event_Shot_sendCmd, cmd, arg1);
    bool ret = true;
    //
    switch  (cmd)
    {
    //  This command is to reset this class. After captures and then reset, 
    //  performing a new capture should work well, no matter whether previous 
    //  captures failed or not.
    //
    //  Arguments:
    //          N/A
    case eCmd_reset:
        ret = onCmd_reset();
        break;

    //  This command is to perform capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_capture:
		ret=  onCmd_MultiCapture(); 
        break;

    //  This command is to perform cancel capture.
    //
    //  Arguments:
    //          N/A
    case eCmd_cancel:
        onCmd_cancel();
        break;
    //
    default:
        ret = ImpShot::sendCommand(cmd, arg1, arg2, arg3);
    }
    //
    return ret;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
RefocusShot::
onCmd_reset()
{
#warning "[TODO] RefocusShot::onCmd_reset()"
    bool ret = true;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
RefocusShot::
onCmd_MultiCapture()
{
	AutoCPTLog cptlog(Event_Shot_capture);
	MBOOL ret = MTRUE; 

    //deque all capture raw buffer
	IImageBuffer* pMultiImageBuffer[CAP_CNT];
	for(int32_t i=0;i<CAP_CNT;i++)
		mpCapBufMgr->dequeBuf(pMultiImageBuffer[i]);
	
	requestMultiBufs();

	for(int32_t i=0;i<CAP_CNT;i++)
	{
		 NSCamShot::ISingleShot *pSingleShot = 
	       NSCamShot::ISingleShot::createInstance(static_cast<EShotMode>(mu4ShotMode), "RefocusShot"); 

		 MUINT32 nrtype = queryCapNRType( getPreviewIso() );
		 

		 pSingleShot->init(); 

		 pSingleShot->enableNotifyMsg( SHUTTER_TIMING ); 

		 EImageFormat ePostViewFmt = 
		static_cast<EImageFormat>(mShotParam.miPostviewDisplayFormat);
		  
	    pSingleShot->enableDataMsg(
				NSCamShot::ECamShot_DATA_MSG_YUV
	  			); 

		// shot param 
	    NSCamShot::ShotParam rShotParam(
				eImgFmt_YUY2,					 //yuv format 
				mShotParam.mi4PictureWidth, 	 //picutre width 
				mShotParam.mi4PictureHeight,	 //picture height
				mShotParam.mu4Transform,		 //picture transform 
				ePostViewFmt,					 //postview format 
				mShotParam.mi4PostviewWidth,	 //postview width 
				mShotParam.mi4PostviewHeight,	 //postview height 
				0,								 //postview transform
				mShotParam.mu4ZoomRatio 		 //zoom   
				);									
	 
	// jpeg param 
		NSCamShot::JpegParam rJpegParam(
				NSCamShot::ThumbnailParam(
					mJpegParam.mi4JpegThumbWidth, 
					mJpegParam.mi4JpegThumbHeight, 
					mJpegParam.mu4JpegThumbQuality, 
					MTRUE),
				mJpegParam.mu4JpegQuality,		   //Quality 
				MFALSE							   //isSOI 
				);
	
	// sensor param 
		NSCamShot::SensorParam rSensorParam(
				getOpenId(),							 //sensor idx
				SENSOR_SCENARIO_ID_NORMAL_CAPTURE,		 //Scenaio 
				10, 									 //bit depth 
				MFALSE, 								 //bypass delay 
				MFALSE									 //bypass scenario 
				);	
	//
		pSingleShot->setCallbacks(fgCamShotNotifyCb, fgCamShotDataCb, this); 
	//	   
		ret = pSingleShot->setShotParam(rShotParam); 
	//
		ret = pSingleShot->setJpegParam(rJpegParam); 
	//
		if(mu4ShotMode == NSCam::eShotMode_RefocusShot)
		{
			CAM_LOGD("ZSD Shot");
			ret = pSingleShot->sendCommand(
									NSCamShot::ECamShot_CMD_SET_CAPTURE_STYLE,
									NSCamShot::ECamShot_CMD_STYLE_NORMAL,
									0,
									0);
		}
		else
		if(mu4ShotMode == NSCam::eShotMode_VideoSnapShot)
		{
			CAM_LOGD("VSS Shot");
			ret = pSingleShot->sendCommand(
									NSCamShot::ECamShot_CMD_SET_CAPTURE_STYLE,
									NSCamShot::ECamShot_CMD_STYLE_VSS,
									0,
									0);
		}
		else
		{
			CAM_LOGE("Un-supported Shot");
		}
		//
		ret = pSingleShot->sendCommand( NSCamShot::ECamShot_CMD_SET_NRTYPE, nrtype, 0, 0 );
		//register yuv buffer
		pSingleShot->registerImageBuffer(NSCamShot::ECamShot_BUF_TYPE_YUV, mpMultiYuvSource[i]);
		//run p2,raw to yuv
		ret=pSingleShot->startOne(rSensorParam,pMultiImageBuffer[i]);
		ret = pSingleShot->uninit(); 
		pSingleShot->destroyInstance(); 		
	}
	
		for(int32_t i=0;i<CAP_CNT;i++)
			mpCapBufMgr->enqueBuf(pMultiImageBuffer[i]);
		
		//handle yuv callback, do 3rd algo 
		handleMultiYuvDataCallback(mpMultiYuvSource,mpMultiYuvSource[0]->getBufSizeInBytes(0));
	
		//release buffers
		releaseMultiBufs();
	
		return ret;

}

/******************************************************************************
 *
 ******************************************************************************/
void
RefocusShot::
onCmd_cancel()
{
    AutoCPTLog cptlog(Event_Shot_cancel);
#warning "[TODO] RefocusShot::onCmd_cancel()"
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL 
RefocusShot::
fgCamShotNotifyCb(MVOID* user, NSCamShot::CamShotNotifyInfo const msg)
{
    AutoCPTLog cptlog(Event_Shot_handleNotifyCb);
    RefocusShot *pRefocusShot = reinterpret_cast <RefocusShot *>(user); 
    if (NULL != pRefocusShot) 
    {
        if ( SHUTTER_TIMING == msg.msgType) 
        {
            pRefocusShot->mpShotCallback->onCB_Shutter(true, 
                                                       0
                                                       ); 
        }
    }

    return MTRUE; 
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
RefocusShot::
fgCamShotDataCb(MVOID* user, NSCamShot::CamShotDataInfo const msg)
{
    RefocusShot *pRefocusShot = reinterpret_cast<RefocusShot *>(user); 
    if (NULL != pRefocusShot) 
    {
        if (NSCamShot::ECamShot_DATA_MSG_POSTVIEW == msg.msgType) 
        {
            //pRefocusShot->handlePostViewData( msg.puData, msg.u4Size);  
        }
        else if (NSCamShot::ECamShot_DATA_MSG_JPEG == msg.msgType)
        {
            pRefocusShot->handleJpegData(
                    (IImageBuffer*)msg.pBuffer,
                    (IImageBuffer*)msg.ext1,
                    (IDbgInfoContainer*)msg.ext2
                    );
        }
		//else if (NSCamShot::ECamShot_DATA_MSG_YUV == msg.msgType)
		//{
			//pRefocusShot->handleYuvDataCallback((IImageBuffer*)msg.pBuffer, msg.pBuffer->getBufSizeInBytes(0));  
		//}
    }

    return MTRUE; 
}


/******************************************************************************
*
*******************************************************************************/
MBOOL
RefocusShot::
handlePostViewData(MUINT8* const puBuf, MUINT32 const u4Size)
{
#if 0
    AutoCPTLog cptlog(Event_Shot_handlePVData);
    MY_LOGD("+ (puBuf, size) = (%p, %d)", puBuf, u4Size); 
    mpShotCallback->onCB_PostviewDisplay(0, 
                                         u4Size, 
                                         reinterpret_cast<uint8_t const*>(puBuf)
                                        ); 

    MY_LOGD("-"); 
#endif
    return  MTRUE;
}

/******************************************************************************
*
*******************************************************************************/
MBOOL
RefocusShot::
handleJpegData(IImageBuffer* pJpeg, IImageBuffer* pThumb, IDbgInfoContainer* pDbg)
{
    AutoCPTLog cptlog(Event_Shot_handleJpegData);
    MUINT8* puJpegBuf = (MUINT8*)pJpeg->getBufVA(0);
    //MUINT32 u4JpegSize = pJpeg->getBitstreamSize();    
	MUINT32 u4JpegSize = pJpeg->getBufSizeInBytes(0);	
    MUINT8* puThumbBuf = NULL;
    MUINT32 u4ThumbSize = 0;
    if( pThumb != NULL )
    {
        puThumbBuf = (MUINT8*)pThumb->getBufVA(0);
        u4ThumbSize = pThumb->getBitstreamSize();
    }

    MY_LOGD("+ (puJpgBuf, jpgSize, puThumbBuf, thumbSize, dbg) = (%p, %d, %p, %d, %p)",
            puJpegBuf, u4JpegSize, puThumbBuf, u4ThumbSize, pDbg); 

    MUINT8 *puExifHeaderBuf = new MUINT8[ DBG_EXIF_SIZE ]; 
    MUINT32 u4ExifHeaderSize = 0; 

    CPTLogStr(Event_Shot_handleJpegData, CPTFlagSeparator, "makeExifHeader");
   // makeExifHeader(eAppMode_PhotoMode, puThumbBuf, u4ThumbSize, puExifHeaderBuf, u4ExifHeaderSize, pDbg);
    MY_LOGD("(thumbbuf, size, exifHeaderBuf, size) = (%p, %d, %p, %d)", 
                      puThumbBuf, u4ThumbSize, puExifHeaderBuf, u4ExifHeaderSize); 

    // dummy raw callback 
    mpShotCallback->onCB_RawImage(0, 0, NULL);   

    // Jpeg callback 
    CPTLogStr(Event_Shot_handleJpegData, CPTFlagSeparator, "onCB_CompressedImage");
	if(IsFinalImage == true)
	{
	    mpShotCallback->onCB_CompressedImage(0,
	                                         u4JpegSize, 
	                                         reinterpret_cast<uint8_t const*>(puJpegBuf),
	                                         u4ExifHeaderSize,                       //header size 
	                                         puExifHeaderBuf,                    //header buf
	                                         0,                       //callback index 
	                                         true                     //final image 
	                                         ); 
	} 
	else
	{
		mpShotCallback->onCB_CompressedImage(0,
											 u4JpegSize, 
											 reinterpret_cast<uint8_t const*>(puJpegBuf),
											 u4ExifHeaderSize,						 //header size 
											 puExifHeaderBuf,					 //header buf
											 0, 					  //callback index 
											 false					  //final image 
											 ); 
	}
    MY_LOGD("-"); 

    delete [] puExifHeaderBuf; 

    return MTRUE; 

}

MBOOL
RefocusShot::
releaseMultiBufs()
{
    
	MY_LOGD("[releaseMultiBufs] - E.");
	deallocMem(mJpegBuf);
	deallocMem(mThumbBuf);
	
	for(MUINT32 i=0; i<CAP_CNT; i++) 
	{
		deallocMem(mpMultiYuvSource[i]);
	}
	MY_LOGD("[releaseMultiBufs] - X.");
	return	MTRUE;
}

void
RefocusShot::
deallocMem(IImageBuffer *pBuf)
{
	pBuf->unlockBuf(LOG_TAG);
	if (pBuf->getImgFormat() == eImgFmt_JPEG)
	{
		mpIImageBufAllocator->free(pBuf);
	}
	else
	{
		pBuf->decStrong(pBuf);
		for (Vector<ImageBufferMap>::iterator it = mvImgBufMap.begin();
				it != mvImgBufMap.end();
				it++)
		{
			if (it->pImgBuf == pBuf)
			{
				mpIMemDrv->unmapPhyAddr(&it->memBuf);
				if (mpIMemDrv->freeVirtBuf(&it->memBuf))
				{
					MY_LOGE("m_pIMemDrv->freeVirtBuf() error");
				}
				else
				{
					mvImgBufMap.erase(it);
				}
				break;
			}
		}
	}
}

MBOOL
RefocusShot::
requestMultiBufs()
{
	MY_LOGD("[requestMultiBufs] - E.");
	MBOOL	ret = MTRUE;
	MBOOL bVertical = ( mShotParam.mu4Transform == NSCam::eTransform_ROT_90 || 
	    mShotParam.mu4Transform == NSCam::eTransform_ROT_270 );

	mJpgWidth = bVertical? mShotParam.mi4PictureHeight: mShotParam.mi4PictureWidth;
	mJpgHeight = bVertical? mShotParam.mi4PictureWidth: mShotParam.mi4PictureHeight;

	mThumbWidth = bVertical? mJpegParam.mi4JpegThumbHeight: mJpegParam.mi4JpegThumbWidth;
	mThumbHeight = bVertical? mJpegParam.mi4JpegThumbWidth: mJpegParam.mi4JpegThumbHeight;	

	mYuvWidth = mShotParam.mi4PictureWidth;
	mYuvHeight = mShotParam.mi4PictureHeight ;

	MY_LOGD("[requestBufs] mJpgWidth %d mJpgHeight %d",mJpgWidth,mJpgHeight);

	mJpegBuf = allocMem(eImgFmt_JPEG, 
	                             mJpgWidth, 
	                             mJpgHeight);
	mThumbBuf = allocMem(eImgFmt_JPEG, 
	                             mThumbWidth, 
	                             mThumbHeight); 

	for(MUINT32 i=0; i<CAP_CNT; i++) 
	{
		 MY_LOGD("reuqest buffer %d",i);			 
		 mpMultiYuvSource[i]=allocMem(eImgFmt_YUY2, mJpgWidth, mJpgHeight); //format base on your 3rd algorithm
	}
	//mpYuvSource = allocMem(eImgFmt_YUY2, mJpgWidth, mJpgHeight); //format base on your 3rd algorithm

	MY_LOGD("[requestMultiBufs] JpegBuf(0x%x),ThumbnailBuf(0x%x)", mJpegBuf,mThumbBuf);
	ret = MTRUE;
	MY_LOGD("[requestMultiBufs] - X.");
	return	ret;
}

IImageBuffer*
RefocusShot::
allocMem(MUINT32 fmt, MUINT32 w, MUINT32 h)
{
    IImageBuffer* pBuf;

    if( fmt != eImgFmt_JPEG )
    {
        /* To avoid non-continuous multi-plane memory, allocate ION memory and map it to ImageBuffer */
        MUINT32 plane = NSCam::Utils::Format::queryPlaneCount(fmt);
        ImageBufferMap bufMap;

        bufMap.memBuf.size = 0;
        for (int i = 0; i < plane; i++)
        {
            bufMap.memBuf.size += (NSCam::Utils::Format::queryPlaneWidthInPixels(fmt,i, w) * NSCam::Utils::Format::queryPlaneBitsPerPixel(fmt,i) / 8) * NSCam::Utils::Format::queryPlaneHeightInPixels(fmt, i, h);
        }

        if (mpIMemDrv->allocVirtBuf(&bufMap.memBuf)) {
            MY_LOGE("g_pIMemDrv->allocVirtBuf() error \n");
            return NULL;
        }
        //memset((void*)bufMap.memBuf.virtAddr, 0 , bufMap.memBuf.size);
        if (mpIMemDrv->mapPhyAddr(&bufMap.memBuf)) {
            MY_LOGE("mpIMemDrv->mapPhyAddr() error \n");
            return NULL;
        }

        MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
        MUINT32 bufStridesInBytes[3] = {0};

        for (MUINT32 i = 0; i < plane; i++)
        { 
            bufStridesInBytes[i] = NSCam::Utils::Format::queryPlaneWidthInPixels(fmt,i, w) * NSCam::Utils::Format::queryPlaneBitsPerPixel(fmt,i) / 8;
        }
        IImageBufferAllocator::ImgParam imgParam(
                fmt, 
                MSize(w,h), 
                bufStridesInBytes, 
                bufBoundaryInBytes, 
                plane
                );

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1(
                                        bufMap.memBuf.memID,
                                        bufMap.memBuf.virtAddr,
                                        bufMap.memBuf.useNoncache, 
                                        bufMap.memBuf.bufSecu, 
                                        bufMap.memBuf.bufCohe);

        sp<ImageBufferHeap> pHeap = ImageBufferHeap::create(
                                                        LOG_TAG,
                                                        imgParam,
                                                        portBufInfo);
        if(pHeap == 0)
        {
            MY_LOGE("pHeap is NULL");
            return NULL;
        }
        //
        pBuf = pHeap->createImageBuffer();
        pBuf->incStrong(pBuf);

        bufMap.pImgBuf = pBuf;
        mvImgBufMap.push_back(bufMap);
    }
    else
    {
        MINT32 bufBoundaryInBytes = 0;
        IImageBufferAllocator::ImgParam imgParam(
                MSize(w,h), 
                w * h * 6 / 5,  //FIXME
                bufBoundaryInBytes
                );

        pBuf = mpIImageBufAllocator->alloc_ion(LOG_TAG, imgParam);
    }
    if (!pBuf || !pBuf->lockBuf( LOG_TAG, eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN ) )
    {
        MY_LOGE("Null allocated or lock Buffer failed\n");
        return  NULL;
    }

    pBuf->syncCache(eCACHECTRL_INVALID);

    return pBuf;
}

MBOOL
RefocusShot::
handleMultiYuvDataCallback(IImageBuffer* puBuf[CAP_CNT],MUINT32 const u4Size)
{

	MY_LOGD("[handleMultiYuvDataCallback] + (puBuf, size) = (%p, %d)", puBuf, u4Size);
	
    #if 0//def Debug_Mode
		char svFileName[100];
      		for(MUINT32 i=0 ;i<CAP_CNT;i++)
         { 
         	 //::sprintf(svFileName, "/sdcard/YUV_img%d_%d_%d.bin", i,mJpgWidth, mJpgHeight);         	 
         	 ::sprintf(svFileName, "/sdcard/YUV_img%d_%d_%d.yuv", i,mJpgWidth, mJpgHeight);
         	 	puBuf[i]->saveToFile(svFileName);
				
			MY_LOGD("dump-----------------yuv buffer");
      		// saveBufToFile(svFileName, (uint8_t*)mpSourceImgBuf[i]->getBufVA(0),mpSourceImgBuf[i]->getBufSizeInBytes(0)); 
      	}
    #endif
		//TBD
	
		//do 3rd process here
		//
	   //after 3rd process, do jpeg and thumbnail.....
	   //handleProcessedYuvData(puBuf[2]);	   
		for(MUINT32 i=0 ;i<CAP_CNT;i++)
		{
			if(i == CAP_CNT-1) 
			{
				IsFinalImage = true; 
			}
			else
			{
				IsFinalImage = false;
			}
	    	handleProcessedYuvData(puBuf[i]);			
		}
	    MY_LOGD("[handleMultiYuvDataCallback] - X.");
		return 0;

}

MBOOL
RefocusShot::
handleProcessedYuvData(IImageBuffer* puBuf)
{
    MY_LOGD("[handleProcessedYuvData] - E.");
    MY_LOGD("[handleProcessedYuvData] - getBufSizeInBytes :%d.",puBuf->getBufSizeInBytes(0));

	//
    MUINT32 u4JpegSize = 0;
    MUINT32 u4ThumbSize = 0; 	
    //1. create jpeg
    // jpeg param 
	/*

    NSCamShot::JpegParam rJpegParam(
            mJpegParam.mu4JpegQuality,         //Quality 
            MFALSE                             //isSOI 
            );
	//main yuv has rotate in pass2
    createJpegImg(puBuf,rJpegParam,0,mJpegBuf,u4JpegSize);
	//2.create thumbnail
    if (0 != mJpegParam.mi4JpegThumbWidth && 0 != mJpegParam.mi4JpegThumbHeight)
    {
        NSCamShot::JpegParam rParam(mJpegParam.mu4JpegThumbQuality, MTRUE);
        createJpegImg(puBuf, rParam, 0, mThumbBuf, u4ThumbSize);
    }

	 mJpegBuf->syncCache(eCACHECTRL_INVALID);
     mThumbBuf->syncCache(eCACHECTRL_INVALID);
   
	//3.make exif and jpeg callback
    handleJpegData(
                    mJpegBuf,
                    mThumbBuf,
                    NULL
                    );	
	*/


	handleJpegData(
					puBuf,
					NULL,
					NULL
				  );	
				  
	MY_LOGD("[handleProcessedYuvData] - X.");
    return MTRUE; 
}

//
MBOOL
RefocusShot::
createJpegImg(IImageBuffer const * rSrcImgBufInfo
		  , NSCamShot::JpegParam const & rJpgParm
		  , MUINT32 const u4Transform
		  , IImageBuffer const * rJpgImgBufInfo
		  , MUINT32 & u4JpegSize)
{
	MBOOL ret = MTRUE;
	// (0). debug
	MY_LOGD("[createJpegImg] - E.");
	MY_LOGD("[createJpegImg] - rSrcImgBufInfo.eImgFmt=%d", rSrcImgBufInfo->getImgFormat());
	MY_LOGD("[createJpegImg] - u4Transform=%d", u4Transform);
	MY_LOGD("[createJpegImg]  dst imgbuf(0x%x)", rJpgImgBufInfo);

	CPTLog(Event_FBShot_JpegEncodeImg, CPTFlagStart);
	//
	// (1). Create Instance
	NSCam::NSIoPipe::NSSImager::ISImager *pISImager = NSCam::NSIoPipe::NSSImager::ISImager::createInstance(rSrcImgBufInfo);
	if(!pISImager) {
	MY_LOGE("createJpegImg can't get ISImager instance.");
	return MFALSE;
	}

	// init setting
	pISImager->setTargetImgBuffer(rJpgImgBufInfo);
	//
	pISImager->setTransform(u4Transform);
	//
	//pISImager->setFlip(u4Flip);
	//
	//pISImager->setResize(rJpgImgBufInfo.u4ImgWidth, rJpgImgBufInfo.u4ImgHeight);
	//
	pISImager->setEncodeParam(rJpgParm.fgIsSOI, rJpgParm.u4Quality);
	//
	//pISImager->setROI(Rect(0, 0, rSrcImgBufInfo->getImgSize().w, rSrcImgBufInfo->getImgSize().h));
	//
	pISImager->execute();
	//
	u4JpegSize = rJpgImgBufInfo->getBitstreamSize();

	pISImager->destroyInstance();
	CPTLog(Event_FBShot_JpegEncodeImg, CPTFlagEnd);

	MY_LOGD("[init] - X. ret: %d.", ret);
	return ret;
}
