LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS:= optional

LOCAL_SRC_FILES:= ArcSoftCameraFeature.cpp \
			ArcSoftFaceBeauty.cpp \
			ArcSoftLiveEffect.cpp \
			ArcSoftFaceProcess.cpp \
			ArcSoftAgeGenderDetection.cpp \
			ArcSoftNightShot.cpp \
			ArcSoftNightVideo.cpp \
			ArcSoftSceneDetection.cpp \
			ArcSoftPicZoom.cpp

LOCAL_LDLIBS += -L$(SYSROOT)/usr/lib -llog

LOCAL_MODULE:= libarcsoft

LOCAL_C_INCLUDES:= \
	$(LOCAL_PATH)/include \
	$(TOP)/external/libgn_camera_feature/include 

PRODUCT_COPY_FILES += \
	$(LOCAL_PATH)/bin/libarcsoft_beautyshot.so:system/lib/libarcsoft_beautyshot.so \
	$(LOCAL_PATH)/bin/libarcsoft_nighthawk.so:system/lib/libarcsoft_nighthawk.so \
	$(LOCAL_PATH)/bin/libarcsoft_night_shot.so:system/lib/libarcsoft_night_shot.so \
	$(LOCAL_PATH)/bin/libarcsoft_picauto.so:system/lib/libarcsoft_picauto.so \
	$(LOCAL_PATH)/bin/libarcsoft_piczoom.so:system/lib/libarcsoft_piczoom.so \
	$(LOCAL_PATH)/bin/libamipengine.so:system/lib/libamipengine.so \
	$(LOCAL_PATH)/bin/libOpenCL.so:system/lib/libOpenCL.so \

ifeq "$(strip $(GN_ARCSOFT_FEATURE_SUPPORT))" "yes"
include $(BUILD_STATIC_LIBRARY)
endif

include $(CLEAR_VARS)
arcsoft_maskfiles := $(LOCAL_PATH)/maskfile
DIRS := $(addprefix $(TARGET_OUT)/, etc/gn_camera_feature/arcsoft/maskfile)

$(DIRS):
	@echo Directory : $@
	@mkdir -p $@
	@cp -rf $(arcsoft_maskfiles)/* $@

beautyshot_style := $(LOCAL_PATH)/style
STYLE_DIRS := $(addprefix $(TARGET_OUT)/, etc/gn_camera_feature/arcsoft/style)

$(STYLE_DIRS):
	@echo Directory : $@
	@mkdir -p $@
	@cp -rf $(beautyshot_style)/* $@

ifeq "$(strip $(GN_ARCSOFT_FEATURE_SUPPORT))" "yes"
ALL_PREBUILT += $(DIRS) \
				$(STYLE_DIRS)
endif
