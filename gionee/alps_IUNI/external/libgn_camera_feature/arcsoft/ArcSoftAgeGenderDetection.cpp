/*************************************************************************************
 * 
 * Description:
 * 	Defines ArcSoft APIs for age gender detection.
 *
 * Author : zhuangxiaojian
 * Email  : zhuangxj@gionee.com
 * Date   : 2014-10-28
 *
 *************************************************************************************/

#define LOG_TAG "ArcSoftAgeGenderDetection"
#include <android/log.h>
#include <string.h>

#include "ArcSoftAgeGenderDetection.h"

namespace android {
ArcSoftAgeGenderDetection::ArcSoftAgeGenderDetection()
	: mInitialized(false)
	, mListener(NULL)
	, mArcSoftFaceProcess(NULL)
{
	mArcSoftFaceProcess = ArcSoftFaceProcess::getInstance();
}

ArcSoftAgeGenderDetection::~ArcSoftAgeGenderDetection()
{

}

int32
ArcSoftAgeGenderDetection::init()
{
	MRESULT res = 0;

	if (mArcSoftFaceProcess != NULL) {
		mArcSoftFaceProcess->init();
	}
	
	return res;
}

void
ArcSoftAgeGenderDetection::deinit()
{
	if (mArcSoftFaceProcess != NULL) {
		mArcSoftFaceProcess->deinit();
	}
}

int32
ArcSoftAgeGenderDetection::setCameraListener(GNCameraFeatureListener* listener)
{
	MRESULT res = 0;

	mListener = listener;

	return res;
}

int32
ArcSoftAgeGenderDetection::enableAgeGenderDetection()
{
	MRESULT res = 0;
	
	if (mArcSoftFaceProcess != NULL) {
		mArcSoftFaceProcess->enableFaceProcess(GN_CAMERA_FACE_FEATURE_AGEGENDER_DETECTION);
	}
	
	return res;
}

int32
ArcSoftAgeGenderDetection::disableAgeGenderDetection()
{
	MRESULT res = 0;
	
	if (mArcSoftFaceProcess != NULL) {
		mArcSoftFaceProcess->disableFaceProcess(GN_CAMERA_FACE_FEATURE_AGEGENDER_DETECTION);
	}
	
	return res;
}

int32
ArcSoftAgeGenderDetection::processAgeGenderDetection(LPASVLOFFSCREEN srcImg, GNDataType dataType)
{
	MRESULT res = 0;
	MLong nFaceNum = 0;
	
	AGE_GENDER_INFO ageGenderArray[MAX_FACE_FOR_AGE_GENDER] = {0};

	PAGE_GENDER_INFO pAgeGenderAarry = (PAGE_GENDER_INFO)ageGenderArray;
	PAGE_GENDER_INFO* pAgeGenderInfo = &pAgeGenderAarry;

	if (mArcSoftFaceProcess == NULL) {
		PRINTE("%s mArcSoftFaceProcess is NULL", __func__);
		return res;
	}
	
	if (!mArcSoftFaceProcess->isFaceProcessEnable()) {
		PRINTE("Haven't initialized the facebeauty enginer.");
		return res;
	}

	res = mArcSoftFaceProcess->faceDetection(srcImg, &pAgeGenderInfo, &nFaceNum, dataType);
	if (res != 0) {
		PRINTD("%s Failed to detect face [#%ld]", __func__, res);
		return res;
	}

	if (nFaceNum > 0) {
		AgeGenderResult_t ageGenderResult = {0};
		memset(&ageGenderResult, 0, sizeof(AgeGenderResult_t));
		ageGenderResult.faceNumber = nFaceNum;
		
		for (int i = 0; i < nFaceNum; i ++) {
			PAGE_GENDER_INFO ageGenderInfo = *pAgeGenderInfo;
			ageGenderResult.ageResult[i]	= ageGenderInfo->age;
			ageGenderResult.genderResult[i] = ageGenderInfo->gender;
			PRINTE("%s age %d gender %d", __func__, ageGenderResult.ageResult[i], ageGenderResult.genderResult[i]);
		}

		if (mListener != NULL) {
			mListener->notify(GN_CAMERA_MSG_TYPE_AGEGENDER_DETECTION, 0, 0, 0, (void*)&ageGenderResult);
		}
	}

	return res;
}


};
