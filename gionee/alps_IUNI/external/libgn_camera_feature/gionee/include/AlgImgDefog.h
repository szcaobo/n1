
#ifndef __ALGIMGDEFOG_INCLUDE__
#define __ALGIMGDEFOG_INCLUDE__

#include <jni.h>
//#include <string>
//#include <vector>
#include <math.h>
#include "AlgCommon.h"
#include "ImgFormate.h"

#define   INITIALIZATION_COMPLETED     0
#define   INITIALIZATION_NOT_START     1
#define   INITIALIZATIION_RUN     2

#define DEFOG_MEM_TEST 0
#define DEFOG_MEM_TEST_CODE 3

// 场景检测类型
#define SCENE_NORMAL  0 //常规场景
#define SCENE_FOG_OR_FLARE 1//雾或眩光场景
#define SCENE_HDR 2//高动态(HDR)场景
#define SCENE_NIGNT 3//夜景场景
#define SCENE_MOTION 99//仅供内部使用

typedef struct tagAlgScnDetectionParm
{
	uint32_t counterStable;
	float32_t lastMeanLum;
	float32_t MeanLum;
	uint8_t lastTypeScene;
}AlgScnDetectionParm;

typedef enum tagDEFOGMODE
{
	MODE_AUTO,//功能全开，算法自动选择处理方式

	/*尚未实现，预留接口
	MODE_DAY_DEFOG_AND_DEFLARE,//只开白天去雾和炫光
	MODE_NIGHT_DEFLARE,//只开晚间去雾和炫光
	MODE_lOCAL_TONE_MAP//只开lOCAL TONE MAP
	*/
}DEFOGMODE;


typedef struct tagAlgDefogRunTimeParm
{
	uint32_t width;
	uint32_t stride;
	uint32_t height;
	uint32_t framesize;
	float32_t 	omiga;//去雾百分比， 一般取80
	float32_t 	constrain_ratio_atmosphere;//大气光比例约束：最大雾厚与大气光的比值，最大雾亮度总是小于大气光亮度
	uint16_t 	constrain_offset_atmosphere;//大气偏移约束： 最大雾厚与天空亮度的差，最大雾亮度总是小于大气光亮度
	uint16_t 	constrain_offset_degree;//全局雾浓度约束，最大雾浓度与全局雾浓度的偏差， 最大雾浓度总是大于全局雾浓度
	uint16_t 	constrain_abs;//全局雾浓度约束，最大雾浓度与全局雾浓度的偏差， 最大雾浓度总是大于全局雾浓度
	uint8_t IsLowLightHDRScene ;//算法内部自动维护，不需要APP设置
	uint8_t IsNormalLightHDRScene ;//算法内部自动维护，不需要APP设置
	uint8_t is_osd_turn_on ;//调试接口，发布时设置为FALSE
	uint16_t  num_threads ; //APP开发者建议算法库使用的线程数
	/*threshold_entropy 信息熵阈值默认为6.8， 场景越简单，信息熵entropy越小,
	 *  本算法处理entropy值较低(< threshold_entropy)的场景， 效果比较差，因此不处理。
	 * 	面巾纸，地板，干净的桌面， 白墙，通常在6以下，
	 * 	风景通常在0.7以上， 背光，有灯光场景通常在7.5以上
	 */
	uint16_t  DPC_degree_threhold ;//暗通道雾感阈值，当暗通道雾感大于该阈值时，放弃去雾处理， 暗通道雾感比第一次雾感判定需要更多计算量，因此进行了第2次分析
	float32_t  threshold_entropy ;

	float32_t 	threholdFlareLightNight ;
	DEFOGMODE 	mode;

	//algorithm context
	uint8_t degree;
	AlgScnDetectionParm parmScn;
}AlgDefogRunTimeParm;





typedef struct tagAlgDefogInitParm
{
	int32_t width;//初始化后，需要处理的最大图像 宽度width，
	int32_t height;//初始化后，需要处理的最大图像 高度height
	int32_t framesize;
	uint32_t	pixelArrayFormat ;

	//uint8_t* min_rgb_img;
	uint8_t* dark_channl_img;
	//uint8_t* transmission_map;
	float32_t* 	histogram ;
	uint32_t 	histogrambin;
	uint8_t* ShareMem;
	uint8_t* bufNV21;
	int32_t  strideNV21;


	uint8_t atmosph_rgb[3];
	uint8_t atmosphYCrCb[3];
    //bool NR_enable;
}AlgDefogInitParm;



typedef struct _tag_FogFlareDegree{
	/*简单结果:
	 * 需要处理1， 不需要处理0

	/*场景类型列表
	 *SCENE_NORMAL   //常规场景
	 *SCENE_FOG_OR_FLARE //雾或眩光场景
	 *SCENE_HDR //高动态(HDR)场景
	 *SCENE_NIGNT //夜景场景
	 */
	uint8_t result;//场景类型


//******场景检测参数，只供算法内部读写******start****/

	/* 	result = (levelFog > runtimeparm->DPC_degree_threhold)
	||(	levelFlareLightNight > threholdFlareLightNight)
	||(levelSceneEntropy > runtimeparm->threshold_entropy)
	*/
	uint8_t isDefault;//是否是默认值， isDefault = true 时， 表示没有记录，下面的数据都没有意义
	uint8_t levelFog;//白天雾或炫光， 不去分雾和炫光
	uint8_t IsLowLightHDRScene;//夜晚灯光场景的炫光，
	uint8_t IsNormalLightHDRScene;//夜晚灯光场景的炫光，
	float levelSceneEntropy;//信息熵， 值小于6.8时， 一般是细节少的场景，不需要去雾，白墙，桌面，白质， 纯色布料等
	//预留接口
	uint32_t levelContrast;//对比度， 高对比度时采用Local tone mapping 压缩对比度，对比度低时，进行对比度增强
//******场景检测参数，只供算法内部读写*******end***/
} FogFlareDegree_t;

typedef struct tagAlgDefogParm
{
	//uint8_t* 	imagedata;//源图像数据指针
	uint8_t*srcY;//源图像数据指针
	uint8_t* srcCrCb;//源图像数据指针

	AlgDefogInitParm  initparm;
	AlgDefogRunTimeParm  runtimeparm;
	/*
	 *	INITIALIZATION_COMPLETED
	 *	INITIALIZATION_NOT_START
	 *	INITIALIZATIION_RUN
	 */
	uint8_t init_flag;
	//bool isinited;
}AlgDefogParm;




class DefogProcessor
{
private:
	AlgDefogParm parm;
	int32_t RunNV21( uint8_t* __restrict srcY, uint8_t* __restrict srcCrCb, uint32_t srcStride);
	int32_t RunYUY2( uint8_t* __restrict srcImg, uint32_t srcStride);
	int32_t AccurateDetectionNV21(	FogFlareDegree_t* result
									,	const uint8_t* __restrict srcY
									,	const uint8_t* __restrict srcCrCb
									, 	int32_t width
									,	int32_t height
									,   uint32_t srcStride
								);

	int32_t AccurateDetectionYV12(	FogFlareDegree_t* result
			,const uint8_t* __restrict srcY
			,const uint8_t* __restrict srcCr
			,const uint8_t* __restrict srcCb
			,int32_t width
			,int32_t height
			,uint32_t srcYStride
			,uint32_t srcCrStride
			,uint32_t srcCbStride
		);

	int32_t FastDetectionY( FogFlareDegree_t* result
							,   const uint8_t* __restrict srcY
							, 	int32_t width
							,	int32_t height
							,   uint32_t srcStride
							);
public:
	DefogProcessor();
	 ~DefogProcessor();

	//to verify the version
	void GetVersion (char *version, unsigned int versionLength);
	int32_t  Init(const AlgDefogInitParm*  initparm);
	int32_t  Ctrl( const  AlgDefogRunTimeParm* parm = NULL);

	/*srcY：连续存储的图像Y数据，可以使NV21的Y， 也可以使YV12的Y，
	     快速模式：mode = 1，　准确模式：mode = 0，
	 * result 检测结果
	 * uint32_t srcStride, 一行图像占据的连续存储的内存空间的字节数
	 */
	int32_t Detection(PIMGOFFSCREEN srcImg, FogFlareDegree_t* result, uint32_t mode);

	//pixelArrayFormat: 支持NV21 YUY2
	int32_t Run( PIMGOFFSCREEN srcImg);
	AlgDefogRunTimeParm* GetDefaltRunTimeparm(int mode);

#if  DEFOG_MEM_TEST
	int8_t  memtest1();
#endif
	void Destroy();
};




#endif
