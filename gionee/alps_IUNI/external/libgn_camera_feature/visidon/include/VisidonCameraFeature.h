/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/
 
#ifndef ANDROID_VISIDON_CAMERA_FEATURE_H
#define ANDROID_VISIDON_CAMERA_FEATURE_H

#include <pthread.h>
#include <GNCameraFeatureBase.h>
#include <stdio.h>

extern "C"
{
#include "VisidonSuperPhoto.h"
#include "VisidonRTSuperPhoto.h"
#include "VisidonSingleHdr.h"
}

namespace android { 

class VisidonCameraFeature : public GNCameraFeatureBase {
public:
    VisidonCameraFeature();
	~VisidonCameraFeature();
	
    static VisidonCameraFeature* createInstance();
    virtual void destroyInstance();
	
	virtual int32 init();
    virtual void  deinit();
    virtual int32 initPreviewSize(int width, int height, GNImgFormat format);
	virtual int32 setSuperPhoto(GNSuperPhotoParam_t superPhotoMode);
	virtual int32 setPicZoom(PicZoomParam const picZoomParam);
	virtual int32 setSingleHdr(GNHdr_t hdrMode);
	virtual int32 setExParameters(int32 type, void* param);
    virtual int32 processPreview(void* inputBuffer, int size, int mask);
	virtual int32 processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask);
	virtual int32 getBurstCnt(GNCameraFeature_t cameraFeature);
	
private:
	virtual int32 processSuperPhoto(ImageParamInfo* imageData);
	virtual int32 processRTSuperPhoto(ImageParamInfo* imageData);
	virtual int32 processSingleHDR(ImageParamInfo* imageData, bool isPreview);
	int32 getImgPixelFormat(GNImgFormat format);
	bool dumpFrame(ImageParamInfo* imageData, int dump_type);

private:
	VisidonSuperPhoto* mpVisidonSuperPhoto;
	VisidonRTSuperPhoto* mpVisidonRTSuperPhoto;
	VisidonSingleHdr* mpVisidonSingleHdr;
	ImageParamInfo mPreviewData;
	int mGNCameraFeature;
	int mDumpCnt;
	
	pthread_mutex_t mMutex;

	int mPreWidth;
	int mPreHeight;
	GNImgFormat mPreFormat;
	int mIsoValue;//debug
};
};
#endif /* ANDROID_TEST_CAMERA_FEATURE_H */
