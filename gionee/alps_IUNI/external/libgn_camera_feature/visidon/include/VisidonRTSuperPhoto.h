/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/
 
#ifndef ANDROID_VISIDON_RT_SUPER_PHOTO_CAMERA_FEATURE_H
#define ANDROID_VISIDON_RT_SUPER_PHOTO_CAMERA_FEATURE_H

#include <GNCameraFeatureDefs.h>
#include <stdio.h>
#include <pthread.h>

#include "GNCameraCmdThread.h"

extern "C"
{
#include "VDRTSuperPhotoAPIExtension.h"
#include "VDRTSuperPhotoAPI.h"
}

namespace android { 

typedef enum 
{
	GN_RTSUPERPHOTO_STATE_UNINIT	= 0,
	GN_RTSUPERPHOTO_STATE_INITING	= 1,
	GN_RTSUPERPHOTO_STATE_INITED	= 2,
} GNRTSuperPhotoState_t;

class VisidonRTSuperPhoto {
public:
    VisidonRTSuperPhoto();
	~VisidonRTSuperPhoto();
	
	int32 init();
    void  deinit();
    int32 enableRTSuperPhoto(int width, int height, GNImgFormat format, float zoom, float totalZoom);
	void disableRTSuperPhoto();
	int32 processRTSuperPhoto(ImageParamInfo* imageData);

private:
	int32 initEngine(int width, int height, GNImgFormat format);
	void deinitEngine();
	void updateRTSuperPhotoState(GNRTSuperPhotoState_t state) {mRTSuperPhotoState = state;}
	static void* defferedWorkRoutine(void* data);
	
private:
	//realtime superphoto params
	int mDenoiseStrength;
	int mSharpening;
	int mContrast;
	int mEnableVideoStab;
	int mDeghosting;
	int mLightMode;
	float mZoomRatio;
	float mTotalZoomRatio;
	void* mRTSuperPhotoEngine;
	int mPreWidth;
	int mPreHeight;
	GNImgFormat mFormat;
	int mColorFilterStrength;
	bool mInitialized;

	GNCameraCmdThread mCmdThread;
	GNRTSuperPhotoState_t mRTSuperPhotoState;

};
};
#endif /* ANDROID_VISIDON_RT_SUPER_PHOTO_CAMERA_FEATURE_H */
