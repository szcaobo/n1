/*************************************************************************************
 * 
 * Description:
 * 	Defines Crunchfish APIs for camera HAL.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2013-05-08
 *
 *************************************************************************************/

#define LOG_TAG "VisidonCameraFeature"

#include "VisidonCameraFeature.h"

#include <cutils/properties.h>
#include <stdlib.h>
#include <cutils/log.h>
#include <time.h>

#include "GNCameraFeatureDbgTime.h"

    
namespace android {

static int count_dump = 0;

VisidonCameraFeature::VisidonCameraFeature() 
	: mPreWidth(0)
	, mPreHeight(0)
	, mDumpCnt(0)
	, mGNCameraFeature(GN_CAMERA_FEATURE_NONE)
	, mPreFormat(GN_IMG_FORMAT_NONE)
{
	mpVisidonSuperPhoto = new VisidonSuperPhoto();
	if (mpVisidonSuperPhoto == NULL) {
		PRINTD("Failed to new VisidonSuperPhoto");
	}

	mpVisidonRTSuperPhoto = new VisidonRTSuperPhoto();
	if (mpVisidonRTSuperPhoto == NULL) {
		PRINTD("Failed to new VisidonRTSuperPhoto");
	}

	mpVisidonSingleHdr = new VisidonSingleHdr();
	if (mpVisidonSingleHdr == NULL) {
		PRINTD("Failed to new VisidonSingleHdr");
	}
	
	pthread_mutex_init(&mMutex, NULL);
}

VisidonCameraFeature::
~VisidonCameraFeature() 
{
	if (mpVisidonSuperPhoto != NULL) {
		delete mpVisidonSuperPhoto;
		mpVisidonSuperPhoto = NULL;
	}	

	if (mpVisidonRTSuperPhoto != NULL) {
		delete mpVisidonRTSuperPhoto;
		mpVisidonRTSuperPhoto = NULL;
	}	

	if (mpVisidonSingleHdr != NULL) {
		delete mpVisidonSingleHdr;
		mpVisidonSingleHdr = NULL;
	}
	
	pthread_mutex_destroy(&mMutex);  
}

VisidonCameraFeature*
VisidonCameraFeature::
createInstance() 
{
	return new VisidonCameraFeature();
}

void
VisidonCameraFeature::
destroyInstance() 
{
	delete this;
}

int32
VisidonCameraFeature::
init()
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);
	
	if (mpVisidonSuperPhoto != NULL) {
		mpVisidonSuperPhoto->init();
	}

	if (mpVisidonRTSuperPhoto != NULL) {
		mpVisidonRTSuperPhoto->init();
	}

	if (mpVisidonSingleHdr != NULL) {
		mpVisidonSingleHdr->init();
	}

	pthread_mutex_unlock(&mMutex);

	return res;
}

void 
VisidonCameraFeature::
deinit()
{
	pthread_mutex_lock(&mMutex);
	
	if (mpVisidonSuperPhoto != NULL) {
		mpVisidonSuperPhoto->deinit();
	}
	
	if (mpVisidonRTSuperPhoto != NULL) {
		mpVisidonRTSuperPhoto->deinit();
	}
	
	if (mpVisidonSingleHdr != NULL) {
		mpVisidonSingleHdr->deinit();
	}

	pthread_mutex_unlock(&mMutex);
}

int32 
VisidonCameraFeature::
initPreviewSize(int width, int height, GNImgFormat format)
{
	int32 res = 0;
	
	pthread_mutex_lock(&mMutex);

	if (width != mPreWidth || height != mPreHeight) {
		if (mpVisidonRTSuperPhoto != NULL && (mGNCameraFeature & GN_CAMERA_FEATURE_PIC_ZOOM)) {
			mpVisidonRTSuperPhoto->enableRTSuperPhoto(width, height, format, 1.0, 1.0);
		}
		
		if (mpVisidonSingleHdr != NULL && (mGNCameraFeature & GN_CAMERA_FEATURE_SINGLE_HDR)) {
			mpVisidonSingleHdr->enableSingleHDR(width, height, format);
		}

		mPreWidth = width;
		mPreHeight = height;

		mPreviewData.width = width;
		mPreviewData.height = height;
	}

	mPreFormat = format;
	
	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32
VisidonCameraFeature::
setSuperPhoto(GNSuperPhotoParam_t param)
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);

	if (param.mode == GN_SUPER_PHOTO_ON) {
		mGNCameraFeature |= GN_CAMERA_FEATURE_SUPER_PHOTO;

		if (mpVisidonSuperPhoto != NULL) {
			mpVisidonSuperPhoto->enableSuperPhoto(param.scale, param.type);
		}
	} else {
		mGNCameraFeature &= ~GN_CAMERA_FEATURE_SUPER_PHOTO;
		
		if (mpVisidonRTSuperPhoto != NULL) {
			mpVisidonSuperPhoto->disableSuperPhoto();
		}
	}

	pthread_mutex_unlock(&mMutex);

	return 0;

}

int32
VisidonCameraFeature::
setPicZoom(PicZoomParam const picZoomParam)
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);
	
	if (picZoomParam.picZoomMode == GN_PIC_ZOOM_ON) {
		mGNCameraFeature |= GN_CAMERA_FEATURE_PIC_ZOOM;
		
		if (mpVisidonRTSuperPhoto != NULL) {
			mpVisidonRTSuperPhoto->enableRTSuperPhoto(mPreWidth, mPreHeight, mPreFormat, picZoomParam.zoom, picZoomParam.totalZoom);
		}
	} else {
		mGNCameraFeature &= ~GN_CAMERA_FEATURE_PIC_ZOOM;
		if (mpVisidonRTSuperPhoto != NULL) {
			mpVisidonRTSuperPhoto->disableRTSuperPhoto();
		}
	}

	pthread_mutex_unlock(&mMutex);

	return 0;
}

int32
VisidonCameraFeature::
setSingleHdr(GNHdr_t hdrMode)
{
	int32 res = 0;

	pthread_mutex_lock(&mMutex);

	if ((hdrMode == GN_HDR_ON) && (!(mGNCameraFeature & GN_CAMERA_FEATURE_SINGLE_HDR))) {
		mGNCameraFeature |= GN_CAMERA_FEATURE_SINGLE_HDR;

		if (mpVisidonSingleHdr != NULL) {
			mpVisidonSingleHdr->enableSingleHDR(mPreWidth, mPreHeight, mPreFormat);
		}
	} else if ((hdrMode == GN_HDR_OFF) && (mGNCameraFeature & GN_CAMERA_FEATURE_SINGLE_HDR)){
		mGNCameraFeature &= ~GN_CAMERA_FEATURE_SINGLE_HDR;

		if (mpVisidonSingleHdr != NULL) {
			mpVisidonSingleHdr->disableSingleHDR();
		}
	}

	pthread_mutex_unlock(&mMutex);

	return res;
}

int32
VisidonCameraFeature::
setExParameters(int32 type, void* param)
{
	int32 res = 0;
	
	if (mpVisidonSuperPhoto != NULL) {
		mpVisidonSuperPhoto->setExParameters(type, param);
	}
	
	return res;
}


int32
VisidonCameraFeature::
processRTSuperPhoto(ImageParamInfo* imageData)
{
	int32 res = 0;

	if (mpVisidonRTSuperPhoto != NULL) {
		res = mpVisidonRTSuperPhoto->processRTSuperPhoto(imageData);
		if (res != 0) {
			PRINTD("%s Faile to process super photo", __func__);
		}
	}
	
	return res;
}

int32
VisidonCameraFeature::
processSuperPhoto(ImageParamInfo* imageData)
{
	int32 res = 0;

	if (mpVisidonSuperPhoto != NULL) {
		res = mpVisidonSuperPhoto->processSuperPhoto(imageData);
		if (res != 0) {
			PRINTD("%s Faile to process super photo", __func__);
		}
	}
	
	return res;
}


int32
VisidonCameraFeature::
processSingleHDR(ImageParamInfo* imageData, bool isPreview)
{
	int32 res = 0;

	if (mpVisidonSingleHdr != NULL) {
		res = mpVisidonSingleHdr->processSingleHDR(imageData, isPreview);
		if (res != 0) {
			PRINTD("%s Failed to process HDR", __func__);
		}
	}
	
	return res;
}

int32 
VisidonCameraFeature::
processPreview(void* inputBuffer, int size, int mask)
{
	int32 res = 0;
	int dumpFrameRef = 0;
#ifdef DEBUG_TIME
	GNCameraFeatureDbgTime dbgTime;
#endif
	
	if (inputBuffer == NULL || size == 0) {
		PRINTE("the inputBuffer is null.");
		return -1;
	}

	pthread_mutex_trylock(&mMutex);

	mPreviewData.buffer = inputBuffer;
	mPreviewData.bufferSize = size;
	
	if (dumpFrame(&mPreviewData, GN_CAMERA_DUMP_FRM_PREVIEW_INPUT)) {
		dumpFrameRef ++;
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_PIC_ZOOM) & mask) {
		res = processRTSuperPhoto(&mPreviewData);
		if (res != 0) {
			PRINTD("%s process super photo failed", __func__);
		}
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_SINGLE_HDR) & mask) {
		res = processSingleHDR(&mPreviewData, true);
		if (res != 0) {
			PRINTD("%s process HDR failed", __func__);
		}
	}

	if (dumpFrame(&mPreviewData, GN_CAMERA_DUMP_FRM_PREVIEW_OUTPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}

	pthread_mutex_unlock(&mMutex); 

#ifdef DEBUG_TIME
	dbgTime.print(__func__);
#endif

	return res;
}

int32
VisidonCameraFeature::
processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask)
{
	int32 res = 0;
	int32 stride = 0;
	int32 scaneline = 0;
	int32 dumpFrameRef = 0;
	ImageParamInfo imageData;

	if (inputBuffer == NULL || size == 0) {
		PRINTE("%s Invalid input params", __func__);
		return -1;
	}
	
	pthread_mutex_lock(&mMutex);
	
	if (ALIGN_FORMAT != ALIGN_TO_32) {
		stride = ALIGN_TO_SIZE(width, ALIGN_FORMAT);
		scaneline = ALIGN_TO_SIZE(height, ALIGN_FORMAT);
	} else {
		stride = width;
		scaneline = height;
	}

	imageData.width = stride;
	imageData.height = scaneline;
	imageData.buffer = inputBuffer;
	imageData.bufferSize = size;
	imageData.pixelArrayFormat = getImgPixelFormat(format);

	if (dumpFrame(&imageData, GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT)) {
		dumpFrameRef ++;
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_SUPER_PHOTO) & mask) {
		res = processSuperPhoto(&imageData);
		if (res != 0) {
			PRINTD("%s processSuperPhoto failed", __func__);
		}
	}

	if ((mGNCameraFeature & GN_CAMERA_FEATURE_SINGLE_HDR) & mask) {
		res = processSingleHDR(&imageData, false);
		if (res != 0) {
			PRINTD("%s processSingleHDR failed", __func__);
		}
	}

	if (dumpFrame(&imageData, GN_CAMERA_DUMP_FRM_SNAPSHOT_OUTPUT)) {
		dumpFrameRef ++;
	}

	if (dumpFrameRef) {
		mDumpCnt ++;
	}

	pthread_mutex_unlock(&mMutex); 

	return res;
}

int32
VisidonCameraFeature::
getBurstCnt(GNCameraFeature_t cameraFeature)
{
	int32 res = 0;

	if (mpVisidonSuperPhoto != NULL && (cameraFeature & GN_CAMERA_FEATURE_SUPER_PHOTO)) {
		res = mpVisidonSuperPhoto->getBurstCnt();
	}

	return res;
}

int32
VisidonCameraFeature::
getImgPixelFormat(GNImgFormat format)
{
	int32 pixelArrayFormat = 0;
	bool isSuperPhotoEnable = 0;
	
	isSuperPhotoEnable = mGNCameraFeature & GN_CAMERA_FEATURE_SUPER_PHOTO;
	
	switch (format) {
		case GN_IMG_FORMAT_YV12:
			pixelArrayFormat = isSuperPhotoEnable ? VD_YUV_420P : IMAGE_FORMAT_YV12;
			break;
		case GN_IMG_FORMAT_YUYV:
			pixelArrayFormat = isSuperPhotoEnable ? VD_YUV_YUYV : IMAGE_FORMAT_YUYV;
			break;
		case GN_IMG_FORMAT_NV21:
			pixelArrayFormat = isSuperPhotoEnable ? VD_YUV_NV21 : IMAGE_FORMAT_NV21;
			break;
		default:
			pixelArrayFormat = isSuperPhotoEnable ? VD_YUV_YUYV : IMAGE_FORMAT_YUYV;
			break;
	}

	return pixelArrayFormat;
}

bool 
VisidonCameraFeature::
dumpFrame(ImageParamInfo* imageData, int dump_type)
{
    bool ret = false;

    char value[PROPERTY_VALUE_MAX];
	property_get("persist.visidon.dumpbuffer", value, "0");
	int32_t enabled = atoi(value);

	if (enabled & GN_CAMERA_DUMP_FRM_MASK_ALL) {		
		if ((enabled & dump_type) && (imageData != NULL) 
			&& (imageData->buffer != NULL)) {
			int width = 0;
			int height = 0;
			char fileStr[64] = {0};
			char timeStr[128] = {0};
			time_t timeCurr;
			struct tm *timeInfo = NULL;;
			
			time(&timeCurr);
			timeInfo = localtime(&timeCurr);
			strftime(timeStr, sizeof(timeStr),"/data/%Y%m%d%H%M%S", timeInfo);

			String8 filePath(timeStr);

			width = imageData->width;
			height = imageData->height;

			switch (dump_type) {
				case GN_CAMERA_DUMP_FRM_PREVIEW_INPUT:
					sprintf(fileStr, "p_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_PREVIEW_OUTPUT:
					sprintf(fileStr, "p_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_SNAPSHOT_INPUT:
					sprintf(fileStr, "s_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_SNAPSHOT_OUTPUT:
					sprintf(fileStr, "s_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_VIDEO_INPUT:
					sprintf(fileStr, "v_in_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				case GN_CAMERA_DUMP_FRM_VIDEO_OUTPUT:
					sprintf(fileStr, "v_out_%dx%d_%d.yuv", width, height, mDumpCnt);
					break;
				default:
					break;
			}

			filePath.append(fileStr);

			FILE* fp = fopen(filePath.string(), "w");
			if (fp == NULL) {
				PRINTE("fail to open file to save img: %s", filePath.string());
				return false;
			}

			fwrite(imageData->buffer, 1, imageData->bufferSize, fp);
			
			fclose(fp);

            ret = true;
		}
	}
	
    return ret;
}


};
