/*******************************************************
 *
 * Description: Define API for process thread.
 *
 * Author:	zhuangxiaojian
 * E-mail:	zhuangxj@gionee.com
 * Date:	2015-06-30
 *
 ******************************************************/

#ifndef GN_CAMERA_FEATURE_THREAD_H
#define GN_CAMERA_FEATURE_THREAD_H

#include <pthread.h>
#include "GNSemaphore.h"
#include "GNCameraQueue.h"

namespace android {

typedef enum
{
    GN_CAMERA_CMD_TYPE_NONE,
	GN_CAMERA_CMD_TYPE_INIT,
	GN_CAMERA_CMD_TYPE_CONFIG,
	GN_CAMERA_CMD_TYPE_UNINIT,
    GN_CAMERA_CMD_TYPE_EXIT,
    GN_CAMERA_CMD_TYPE_MAX
} camera_cmd_type_t;

typedef struct {
    camera_cmd_type_t cmd;
} camera_cmd_t;

class GNCameraCmdThread {

public:
    GNCameraCmdThread();
    ~GNCameraCmdThread();

	/****************************************************************************
	 * FUNCTION   : launch
	 *
	 * DESCRIPTION: launch Cmd Thread
	 *
	 * PARAMETERS :
	 *   @start_routine : thread routine function ptr
	 *   @user_data     : user data ptr
	 *
	 * RETURN     : int32_t type of status
	 *              NO_ERROR  -- success
	 *              none-zero failure code
	 ***************************************************************************/
    int32_t launch(void *(*start_routine)(void *), void* user_data);

	/****************************************************************************
	 * FUNCTION   : exit
	 *
	 * DESCRIPTION: exit the CMD thread
	 *
	 * PARAMETERS : None
	 *
	 * RETURN     : int32_t type of status
	 *              NO_ERROR  -- success
	 *              none-zero failure code
	 ***************************************************************************/
    int32_t exit();

	/****************************************************************************
	 * FUNCTION   : sendCmd
	 *
	 * DESCRIPTION: send a command to the Cmd Thread
	 *
	 * PARAMETERS :
	 *   @cmd     : command to be executed.
	 *   @sync_cmd: flag to indicate if this is a synchorinzed cmd. If true, this call
	 *              will wait until signal is set after the command is completed.
	 *   @priority: flag to indicate if this is a cmd with priority. If true, the cmd
	 *              will be enqueued to the head with priority.
	 *
	 * RETURN     : int32_t type of status
	 *              NO_ERROR  -- success
	 *              none-zero failure code
	 ***************************************************************************/
    int32_t sendCmd(camera_cmd_type_t cmd, uint8_t sync_cmd, uint8_t priority);

	/****************************************************************************
	 * FUNCTION   : getCmd
	 *
	 * DESCRIPTION: dequeue a cmommand from cmd queue
	 *
	 * PARAMETERS : None
	 *
	 * RETURN     : cmd dequeued
	 ***************************************************************************/
    camera_cmd_type_t getCmd();

public:
    GNCameraQueue cmd_queue;      /* cmd queue */
    pthread_t cmd_pid;           /* cmd thread ID */
    cam_semaphore_t cmd_sem;               /* semaphore for cmd thread */
    cam_semaphore_t sync_sem;              /* semaphore for synchronized call signal */
};

}; // namespace android

#endif /* GN_CAMERA_FEATURE_THREAD_H */
