/*************************************************************************************
 * 
 * Description:
 * 	Defines gionee feature APIs for camera hal.
 *
 * Author : wutangzhi
 * Email  : wutz@gionee.com
 * Date   : 2012-06-11
 *
 *************************************************************************************/
#include "GNCameraFeature.h"


namespace android {

GNCameraFeature::
GNCameraFeature()
	: mScaladoCameraFeature(NULL)
	, mArcsoftCameraFeature(NULL)
	, mCrunchfishCameraFeature(NULL)
	, mGNCameraProfile(0)
	, mGNCameraFeatureMask(0)
{	

}

GNCameraFeature*
GNCameraFeature::
createInstance()
{
	return new GNCameraFeature();
}

void
GNCameraFeature::
destroyInstance()
{
	delete this;
}


int32
GNCameraFeature::
init()
{
	int32 res = 0;

	return res;
}

void 
GNCameraFeature::
deinit()
{

}

void 
GNCameraFeature::
setCameraType(GNCameraType_t cameraType)
{

}

int32 
GNCameraFeature::
setCameraListener(GNCameraFeatureListener* listener) 
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
setCapturePath(const char* filePath)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
setEffect(GNLiveEffect_t effect)
{
	int32 res = 0;

	return res;
}

int32
GNCameraFeature::
setSingleHdr(GNHdr_t hdrMode)
{
	int32 res = 0;
	
	return res;
}

int32
GNCameraFeature::
setSuperPhoto(GNSuperPhotoParam_t superPhotoParam)
{
	int32 res = -1;

	return res;
}


int32 
GNCameraFeature::
setGestureShot(GNGestureShot_t gestureShotMode)
{
	int32 res = 0;
	return res;;
}

int32 
GNCameraFeature::
setGestureDetection(GNGestureDetection_t gestureDetectionMode)
{
	int32 res = 0;
	return res;;
}


int32 
GNCameraFeature::
setFaceBeauty(FaceBeautyParam const faceBeautyParam)
{
	int32 res = 0;

	return res;;
}

int32 
GNCameraFeature::
setAgeGenderDetection(GNAgeGenderDetection_t const ageGenderDetection)
{
	int32 res = 0;
	
	return res;
}

int32 
GNCameraFeature::
setMirror(GNMirror_t mirrorMode)
{
	int32 res = 0;
	
	return res;
}

int32 
GNCameraFeature::
setSceneDetection(GNSceneDetectionParam param)
{
	int32 res = 0;
	
	return res;
}

int32 
GNCameraFeature::
setNightShot(GNNightShot_t nightShotMode)
{
	int32 res = 0;
	
	return res;
}

int32 
GNCameraFeature::
setNightVideo(GNNightVideo_t nightVideoMode)
{
	int32 res = 0;
	
	return res;
}

int32 
GNCameraFeature::
setDefogShot(GNDefogShot_t defogShotMode, GNImgFormat format)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
setPicZoom(PicZoomParam const picZoomParam)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
initPreviewSize(int width, int height, GNImgFormat format)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
initVideoSize(int width, int height, GNImgFormat format)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
processPreview(void* inputBuffer, int size, int mask)
{
	int32 res = 0;
	
    return res;
}

int32 
GNCameraFeature::
processVideo(void* inputBuffer, int size, int mask)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
processPicture(void* inputBuffer, int* size, int mask)
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
processRaw(void* inputBuffer, int size, int width, int height, GNImgFormat format, int mask) 
{
	int32 res = 0;

	return res;
}

int32 
GNCameraFeature::
setBurstCnt(GNCameraFeature_t cameraFeature, int count)
{
	int32 res = -1;

	return res;;
}

int32 
GNCameraFeature::
getBurstCnt(GNCameraFeature_t cameraFeature)
{
	int32 res = 0;
	
	return res;;
}

int32 
GNCameraFeature::
setOrientation(int orientation)
{
	int32 res = 0;

	
    return res;;
}

int32 
GNCameraFeature::
setExParameters(int32 type, void* param)
{
	int32 res = 0;

    return res;;
}

void
GNCameraFeature::
updateFeatureMask(int32 flag, int32 mask)
{
	if (flag != 0) {
		mGNCameraFeatureMask |= mask;
	} else {
		mGNCameraFeatureMask &= ~mask;
	}
}

int32
GNCameraFeature::
getProfileMask(int32 featureMask)
{
	int32 profileMask = 0;

	featureMask &= mGNCameraFeatureMask;

	if (featureMask & GN_CAMERA_FEATURE_HDR) {
		profileMask = (GN_CAMERA_PROFILE_SCALADO_HDR & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_LOWLIGHT) {
		profileMask |= (GN_CAMERA_PROFILE_SCALADO_LOWLIGHT & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_EFFECT) {
		profileMask |= ((GN_CAMERA_PROFILE_SCALADO_EFFECT | GN_CAMERA_PROFILE_ARCSOFT_EFFECT) & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_PICTURE_FREAME) {
		profileMask |= (GN_CAMERA_PROFILE_SCALADO_PICTURE_FRAME & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_PANORAMA) {
		profileMask |= (GN_CAMERA_PROFILE_SCALADO_PANORAMA & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_FACE_BEAUTY) {
		profileMask |= (GN_CAMERA_PROFILE_ARCSOFT_FACE_BEAUTY & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_GESTURE_SHOT) {
		profileMask |= (GN_CAMERA_PROFILE_ARCSOFT_GESTURE_SHOT & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_GESTURE_DETECTION) {
		profileMask |= (GN_CAMERA_PROFILE_CRUNCHFISH_GESTURE_DETECTION & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATUER_MIRROR) {
		profileMask |= (GN_CAMERA_PROFILE_SCALADO_MIRROR & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATUER_SCENE_DETECTION) {
		profileMask |= ((GN_CAMERA_PROFILE_ARCSOFT_SCENE_DETECTION | GN_CAMERA_PROFILE_GIONEE_SCENE_DETECTION) & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATUER_NIGHT_SHOT) {
		profileMask |= (GN_CAMERA_PROFILE_ARCSOFT_NIGHT_SHOT & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATUER_NIGHT_VIDEO) {
		profileMask |= (GN_CAMERA_PROFILE_ARCSOFT_NIGHT_VIDEO & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATUER_AGEGENDER_DETECTION) {
		profileMask |= (GN_CAMERA_PROFILE_AGEGENDER_DETECTION & mGNCameraProfile);
	}
	
	if (featureMask & GN_CAMERA_FEATURE_PIC_ZOOM) {
		profileMask |= ((GN_CAMERA_PROFILE_ARCSOFT_PIC_ZOOM | GN_CAMERA_PROFILE_VISIDON_RT_SUPER_PHOTO) & mGNCameraProfile);
	}
	
	if (featureMask & GN_CAMERA_FEATURE_DEFOG_SHOT) {
		profileMask |= (GN_CAMERA_PROFILE_GIONEE_DEFOG_SHOT & mGNCameraProfile);
	}

	if (featureMask & GN_CAMERA_FEATURE_SINGLE_HDR) {
		profileMask |= (GN_CAMERA_PROFILE_VISIDON_HDR & mGNCameraProfile);
	}
	
	if (featureMask & GN_CAMERA_FEATURE_SUPER_PHOTO) {
		profileMask |= (GN_CAMERA_PROFILE_VISIDON_SUPER_PHOTO & mGNCameraProfile);
	}	

	return profileMask;
}

}; // namespace android
