package android.content.res;


import android.content.res.AuroraThemeZip.ThemeFileInfo;
import android.util.Log;

/**
 * aurora资源aurora-framework-res.apk
 * @author alexluo
 *
 */
public class AuroraFrameworkTheme extends AuroraApplicationTheme {

	private static AuroraFrameworkTheme mInstance;
	
	  // Locker
    private static final Object sLock = new Object();
	
	private AuroraFrameworkTheme(Resources res, String packageName) {
		super(res, packageName);
		// TODO Auto-generated constructor stub
	}
	
	public static AuroraFrameworkTheme getInstance(Resources res, String packageName){
		synchronized (sLock) {
			if(mInstance == null){
				mInstance = new AuroraFrameworkTheme(res, packageName);
				
			}
			mInstance.update();
			return mInstance;
		}
	}
	
	@Override
	public Integer getThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeInteger(id);
	}
	@Override
	public Float getFloatThemeValue(int id) {
		// TODO Auto-generated method stub
		return mThemeZipFile.getThemeFloat(id);
	}
	
	
	
	@Override
	public ThemeFileInfo getThemeFileInfo(String themePah,int id) {
		Log.d("auroraF", "invoke aurora framework");
		return mThemeZipFile.getThemeFileInfo(AuroraApplicationTheme.SYSTEM_RES_IUNI, themePah);
	}
	
	

}
