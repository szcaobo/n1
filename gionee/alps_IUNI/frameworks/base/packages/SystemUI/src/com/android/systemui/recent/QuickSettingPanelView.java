/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.recent;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.LayoutTransition;
import android.animation.TimeInterpolator;
import android.app.ActivityManager;
import android.app.ActivityManagerNative;
import android.app.ActivityOptions;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.systemui.R;
import com.android.systemui.qs.QSPanel;
import com.android.systemui.qs.QSPanel.QuickSettingCallback;
import com.android.systemui.recent.HandlerBar.HandlerBarCallback;
import com.android.systemui.statusbar.BaseStatusBar;
import com.android.systemui.statusbar.phone.PhoneStatusBar;
import com.android.systemui.statusbar.phone.PhoneStatusBarPolicy;
import com.android.systemui.statusbar.phone.BarTransitions;

import android.view.KeyEvent;
import android.content.res.Configuration;

import java.util.ArrayList;
import java.util.HashMap;

// Aurora <zhanggp> <2013-10-17> added for systemui begin
import android.widget.Button;
// Aurora <zhanggp> <2013-10-17> added for systemui end
//Aurora <tongyh> <2013-11-04> add animation to QuickSetting's and RecentsNoApps's and RecentsContainer's view begin
import android.view.animation.Animation;
import android.os.Handler;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.view.View.OnKeyListener;
import android.widget.RelativeLayout;
import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.view.Gravity;

//Aurora <tongyh> <2013-11-04> add animation to QuickSetting's and RecentsNoApps's and RecentsContainer's view end
public class QuickSettingPanelView extends FrameLayout implements Animator.AnimatorListener{
    static final String TAG = "RecentsPanelView";
    static final boolean DEBUG = PhoneStatusBar.DEBUG || false;
	public static int phoneHeight;
	public static int phoneWidth;
	private View mQuickSetting;

    private static final int QUIT_ANIMATION_DURATION = 300;

    private static final int MASK_VIEW_FULL_ALPHA = 210;

    private boolean mShowing;
    private boolean mWaitingToShow;
    
    private boolean mWaitingForWindowAnimation;

    private int mThumbnailWidth;
    private boolean mFitThumbnailToXY;
    private boolean mHighEndGfx;
    private HandlerBarCallback mHandlerBarCallback;
    private IntentFilter mIntentFilter;
    public static final String CLOSE_RECENTSPAELVIEW = "com.android.systemui.recent.close_recentspanelview";
    public static final String OPEN_RECENTSPAELVIEW = "com.android.systemui.recent.open_recentspanelview";
    
    //iht 2015-03-23 更新图标广播
    public static final String NOTIFY_APPS_TO_UPDATEIICON = "com.aurora.action.pulbicres.update";
    
    public static boolean isResponseHomeKey = false;
    public boolean isScreenOff = false;
    private int density;
    boolean isrung = true;

    public boolean mAnimating = false;
    
    private static final String RECENTS_PANEL_HIDDEN = "com.android.systemui.recent.aurora.RECENTS_PANEL_HIDDEN";

    private ObjectAnimator animQuickSettingTranslation, animQuickSettingAlpha, bgAlpha;

    private static final String VOICEWAKEUP_UNLOCK = "com.qualcomm.listen.voicewakeup.unlock";

    private View mRecentsMaskView;
    private static float quickSettingAlpha = 0;

    private AnimatorSet mEnterAnimSet = new AnimatorSet();
    
    private QSPanel mQuickSettingPanel;
    private boolean mListening;

    private boolean isShowInputMethod = false;
    public void init(HandlerBarCallback hbc, int mPhoneWidth, int mPhoenHeight){
    	mHandlerBarCallback = hbc;
    	phoneWidth = mPhoneWidth;
    	phoneHeight = mPhoenHeight;

    	mIntentFilter = new IntentFilter();
    	mIntentFilter.addAction(Intent.ACTION_SCREEN_OFF);

    	mIntentFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
    	mIntentFilter.addAction(Intent.ACTION_CONFIGURATION_CHANGED);

    	mIntentFilter.addAction("com.aurora.deskclock.startalarm");
    	mIntentFilter.addAction("com.aurora.deskclock.stopalarm");

        mIntentFilter.addAction(VOICEWAKEUP_UNLOCK);

        mIntentFilter.addAction("hhh.hhh.hhh.hhh");
        
        //iht 2015-03-23 更新图标广播
        mIntentFilter.addAction(NOTIFY_APPS_TO_UPDATEIICON);

    	mContext.registerReceiver(mIntentReceiver, mIntentFilter);

    	density = (int)(mContext.getResources().getDisplayMetrics().density);
    }

    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {

        phoneWidth = w;
        phoneHeight = h;
    }

    public QuickSettingPanelView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public QuickSettingPanelView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        updateValuesFromResources();

		initQuickSettingsSize();
    }

	private View quickSettingsView = null;
	private View recentPanelView = null;
	private View rootView = null;

	int last_orientation = getScreenState();
	
	private int getScreenState(){
		return getResources().getConfiguration().orientation;
	}

	@Override
	protected void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == last_orientation) return;
		last_orientation = newConfig.orientation;
		resetRecentsPanelViewOffset(false);
		initView();
		Log.e("222222", "---QuickSettingPanelView onConfigurationChanged getScreenState() = ----------" + getScreenState());
		if(getScreenState() == Configuration.ORIENTATION_PORTRAIT){
			orientationPortraitLayout();
		} else {
			orientationLandLayout();
		}
		rotationChangeAnimation();
	}
	// this method needs modify
	private void rotationChangeAnimation(){
		ObjectAnimator animReset01 = null;
		ObjectAnimator animReset02 = null;

		if(isOrientationPortrait()){
			((RecentsLayout)mRecentsMaskView).startScroll(0, 0, 0, HandlerBar.mRecentsScrimHeight, HandlerBar.AUTO_ENTER_ANIMATION_DURATION);

		} else {
			((RecentsLayout)mRecentsMaskView).startScroll(0, 0, HandlerBar.mRecentsScrimHeight, 0, HandlerBar.AUTO_ENTER_ANIMATION_DURATION);
		}
		autoQuickSettingEnterAnimation();
	}

	private void resetRecentsPanelViewOffset(boolean removePanel){
		if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){
            animQuickSettingTranslation.cancel();
        }
        if (mEnterAnimSet != null && mEnterAnimSet.isRunning())
            mEnterAnimSet.cancel();		
		mQuickSetting.setTranslationY(0);		
		mQuickSetting.setTranslationX(0);

		mRecentsMaskView.scrollBy(-mRecentsMaskView.getScrollX(), -mRecentsMaskView.getScrollY());
	}

	private boolean isOrientationPortrait(){
		int orientation = getScreenState();
		if (orientation == Configuration.ORIENTATION_PORTRAIT) return true;
		return false;
	}


	private int quickSettingWidth;
	private int quickSettingHeight;
	private int quickSettingWidthLand;
	private int quickSettingHeightLand;
	private int quickSettingMarginTop;
	private int quickSettingMarginLeft;
	private int quickSettingMarginTopLand;
	private int quickSettingMarginLeftLand;

	private int recentHeight;
	private int recentWidthLand;


	private void initQuickSettingsSize(){
		quickSettingWidth = getResources().getDimensionPixelSize(R.dimen.quick_settings_width);
		quickSettingHeight = getResources().getDimensionPixelSize(R.dimen.quick_settings_height);
		quickSettingWidthLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_width_land);
		quickSettingHeightLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_height_land);
		quickSettingMarginTop = getResources().getDimensionPixelSize(R.dimen.quick_settings_margintop);
		quickSettingMarginLeft = getResources().getDimensionPixelSize(R.dimen.quick_settings_marginleft);
		quickSettingMarginTopLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_margintop_land);
		quickSettingMarginLeftLand = getResources().getDimensionPixelSize(R.dimen.quick_settings_marginleft_land);
	}

	private void initView(){
		if(quickSettingsView == null || recentPanelView == null){
			quickSettingsView = findViewById(R.id.quick_setting);
			recentPanelView = findViewById(R.id.upbgview);
		}
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
	}

	private void orientationPortraitLayout(){

		/*
		FrameLayout.LayoutParams quickParam = (FrameLayout.LayoutParams)quickSettingsView.getLayoutParams();
		quickParam.width = quickSettingWidth;
		quickParam.height = quickSettingHeight;
		quickParam.setMargins(quickSettingMarginLeft, quickSettingMarginTop, 0, 0);
		quickParam.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

		FrameLayout.LayoutParams recentParam = (FrameLayout.LayoutParams)recentPanelView.getLayoutParams();
		recentParam.width = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.height = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;

		quickSettingsView.setLayoutParams(quickParam);
		recentPanelView.setLayoutParams(recentParam);
		*/
		quickSettingAnimStart = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_start);
		quickSettingAnimEnd = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_end); 
	}

	private void orientationLandLayout(){
		/*
		FrameLayout.LayoutParams quickParam = (FrameLayout.LayoutParams)quickSettingsView.getLayoutParams();
		quickParam.width = quickSettingWidthLand;
		quickParam.height = quickSettingHeightLand;
		quickParam.setMargins(quickSettingMarginLeftLand, quickSettingMarginTopLand, 0, 0);
		quickParam.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;

		FrameLayout.LayoutParams recentParam = (FrameLayout.LayoutParams)recentPanelView.getLayoutParams();
		recentParam.width = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.height = FrameLayout.LayoutParams.MATCH_PARENT;
		recentParam.gravity = Gravity.TOP | Gravity.RIGHT;

		quickSettingsView.setLayoutParams(quickParam);
		recentPanelView.setLayoutParams(recentParam);
		*/
		quickSettingAnimStart = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_start_land);
		quickSettingAnimEnd = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_end_land); 
	}

    private boolean pointInside(int x, int y, View v) {
        final int l = v.getLeft();
        final int r = v.getRight();
        final int t = v.getTop();
        final int b = v.getBottom();
        return x >= l && x < r && y >= t && y < b;
    }

    public void onUiHidden() {
        Intent intent= new Intent(HandlerBar.ENABLE_HANDLER);
        mContext.sendBroadcastAsUser(intent, new UserHandle(UserHandle.USER_CURRENT));

        mContext.sendBroadcastAsUser(new Intent(RECENTS_PANEL_HIDDEN),
            new UserHandle(UserHandle.USER_CURRENT));
    }

    public void dismiss() {
    	
    	removeRecentsPanelView();
    }

    public void dismissAndGoBack() {
    	
    	removeRecentsPanelView();
    }

    public void onAnimationCancel(Animator animation) {
        Log.d("felix","RecentsPanelView.DEBUG onAnimationCancel()");
    }

    public void onAnimationEnd(Animator animation) {
        Log.d("felix","RecentsPanelView.DEBUG onAnimationEnd()");
    }

    public void onAnimationRepeat(Animator animation) {
        Log.d("felix","RecentsPanelView.DEBUG onAnimationRepeat()");
    }

    public void onAnimationStart(Animator animation) {
        Log.d("felix","RecentsPanelView.DEBUG onAnimationStart()");
    }

    @Override
    public boolean dispatchHoverEvent(MotionEvent event) {
        final int x = (int) event.getX();
        final int y = (int) event.getY();
        if (x >= 0 && x < getWidth() && y >= 0 && y < getHeight()) {
            return super.dispatchHoverEvent(event);
        }
        return true;
    }

    public void updateValuesFromResources() {
        final Resources res = mContext.getResources();
        mThumbnailWidth = Math.round(res.getDimension(R.dimen.status_bar_recents_thumbnail_width));
        mFitThumbnailToXY = res.getBoolean(R.bool.config_recents_thumbnail_image_fits_to_xy);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        
        mRecentsMaskView = findViewById(R.id.upbgview);
		
		mQuickSetting = findViewById(R.id.quick_setting);
		
		mQuickSettingPanel = (QSPanel)findViewById(R.id.quick_settings_panel);
		mQuickSettingPanel.setOnQuickSettingCallback(new QuickSettingCallback() {
			
			@Override
			public void finishQuickSetting() {
				// TODO Auto-generated method stub
				removeRecentsPanelView();
			}
		});
    }

    private Handler handler = new Handler();
    
    
    @Override
	public boolean onTouchEvent(MotionEvent event) {
    	if(inRangeOfView(mQuickSetting,event)){
    		return true;
    	}
    	
    	if (event.getAction() ==  MotionEvent.ACTION_UP){
    		
    	   removeRecentsPanelView();
    	}
		return true;
	}
    
    

    private boolean inRangeOfView(View view, MotionEvent ev){
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int x = location[0];
        int y = location[1];
        if(ev.getX() < x || ev.getX() > (x + view.getWidth()) || ev.getY() < y || ev.getY() > (y + view.getHeight())){
            return false;
        }
        return true;
    }
    
       @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.KEYCODE_BACK:
        	    if (event.getAction() == KeyEvent.ACTION_UP) {
                    // Cause pull up from BACK key fail
                    if (!event.isCanceled()) {
                        removeRecentsPanelView();
                    }
        	    }
        	    break;
            
        }
        return super.dispatchKeyEvent(event);
    }

    public void removeRecentsPanelView(){
        if (mEnterAnimSet.isRunning())
            mEnterAnimSet.cancel();

        if (mAnimating) return;

    	 if(isScreenOff){
 			scrollToOriginalLocationNoAnimation();
 			setRecentsMaskViewAlpha();
			mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
			mHandlerBarCallback.removeRecentPanelView();
			isScreenOff = false;
    	 }else{
    		 scrollToOriginalLocation();
        	 autoQuickSettingQuitAnimation();
    	 }

         onUiHidden();
     }
     
    public void removeRecentsPanelView(boolean isShowHandlerBar){
        if (mEnterAnimSet.isRunning())
            mEnterAnimSet.cancel();

         if (mAnimating) return;

    	 if(isScreenOff){
 			scrollToOriginalLocationNoAnimation();
 			setRecentsMaskViewAlpha();
			mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
			mHandlerBarCallback.removeRecentPanelView(false);
			isScreenOff = false;
    	 }else{
    		 scrollToOriginalLocation();
        	 autoQuickSettingQuitAnimation(isShowHandlerBar);
    	 }
         onUiHidden();
     }
    
 	public void quickRecentPanelZMove(int scrollZ) {

 		mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(blurredBackgroundAlphaChanged(Math.min(scrollZ, 255)));

 		setMaskAlpha(backgroundAlphaChanged(scrollZ));

        if (isOrientationPortrait()) 		
            mQuickSetting.setTranslationY(-Math.max(-scrollZ, -(quickSettingAnimEnd)));
        else
            mQuickSetting.setTranslationX(-Math.max(-scrollZ, -(quickSettingAnimEnd)));
	    mQuickSetting.setAlpha(distanChangeToAlpahZeroToOne(scrollZ));
 	}
 	
 	private BroadcastReceiver mIntentReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equals(NOTIFY_APPS_TO_UPDATEIICON)){
            	mHandlerBarCallback.refreshViewIcons();
            }
            
        	if (Intent.ACTION_SCREEN_OFF.equals(action)) {
        		if(isShown()){
        			isScreenOff = true;
        			removeRecentsPanelView();
        		}
            }

        	else if(Intent.ACTION_CLOSE_SYSTEM_DIALOGS.equals(action)){

                boolean shown = isShown();
                if(shown && !mAnimating) {
            		removeRecentsPanelView();
            	}
            	isResponseHomeKey = false;
            }else if(Intent.ACTION_CONFIGURATION_CHANGED.equals(action)){
            	int orientation = getScreenState();
				if(last_orientation != orientation){
					dispatchConfigurationChanged(mContext.getResources().getConfiguration());
				}
                mHandlerBarCallback.updateOrientation(orientation);
                if (InvokerService.getInstance() != null)
                    InvokerService.getInstance().mLandscape = (orientation == Configuration.ORIENTATION_LANDSCAPE);
            }

            else if("com.aurora.deskclock.startalarm".equals(action)){

            	if(isShown()){
            		removeRecentsPanelView(false);
            	}else{
            		mHandlerBarCallback.showHandlerBarView(false);
            	}
            }else if("com.aurora.deskclock.stopalarm".equals(action)){
            	mHandlerBarCallback.showHandlerBarView(true);

            } else if (VOICEWAKEUP_UNLOCK.equals(action)) {
                removeRecentsPanelView();
            } else if ("hhh.hhh.hhh.hhh".equals(action)){
				mQuickSetting.setTranslationY(0);		
				mQuickSetting.setTranslationX(0);
			}
        }
 	};

    public void setRecentsMaskViewAlpha(){
	    setMaskAlpha(0);
    }
    public void setRecentsMaskViewAlphaFull(){
    	setMaskAlpha(MASK_VIEW_FULL_ALPHA);
    }
    public void setRecentsQuickSettingViewAlpha(){
    	mQuickSetting.setAlpha(0);
    }
    
    public float distanChangeToAlpahZeroToOne(float frac){
        final int fullAlpha = 1;
        final int H = HandlerBar.mRecentsScrimHeight;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = fullAlpha * frac / H;
        if(alpha < 0){
        	alpha = 0;
        }
        return alpha;
    }
    
    public float distanChangeToAlpahOneToZero(float frac) {
        final int fullAlpha = 1;
        final int H = HandlerBar.mRecentsScrimHeight;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = fullAlpha - (fullAlpha * frac / H);
        if(alpha < 0){
        	alpha = 0;
        }
        return alpha;
    }
    
    public int backgroundAlphaChanged(float frac) {
        final int H = HandlerBar.mRecentsScrimHeight-60;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = MASK_VIEW_FULL_ALPHA * frac / H;
        if(alpha < 0){
        	alpha = 0;
        }
        return (int)alpha;
    }
    
    public int blurredBackgroundAlphaChanged(float frac) {
        final int fullAlpha = 255;
        final int H = HandlerBar.mRecentsScrimHeight-60;
        frac = Math.min(frac, H);
        float alpha = 1f;
        alpha = fullAlpha * frac / H;
        if(alpha < 0){
        	alpha = 0;
        }
        return (int)alpha;
    }

	private int quickSettingAnimStart = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_start);
	private int quickSettingAnimEnd = getResources().getDimensionPixelSize(R.dimen.quick_settings_scroll_end); 

    public void autoQuickSettingEnterAnimation(){
    	
    	Log.e("222222", "-----autoQuickSettingEnterAnimation-------");

    	setMaskAlpha(MASK_VIEW_FULL_ALPHA);

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingTranslation.cancel()");
            animQuickSettingTranslation.cancel();
        }

		if(isOrientationPortrait()){
			animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY", quickSettingAnimStart,quickSettingAnimEnd);
		} else {
	    	animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX", quickSettingAnimStart,quickSettingAnimEnd);
		}

    	animQuickSettingTranslation.setDuration(HandlerBar.AUTO_ENTER_ANIMATION_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingAlpha.cancel()");
            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", 0f,1f);
    	animQuickSettingAlpha.setDuration(HandlerBar.AUTO_ENTER_ANIMATION_DURATION);

    	mEnterAnimSet = new AnimatorSet();

        mEnterAnimSet.addListener(new AnimatorListenerAdapter() {
            boolean canceled;

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
                canceled = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                canceled = true;
                mAnimating = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                if (canceled) return;

                handler.postDelayed(mDisableHandlerRunnable, 0);

                mAnimating = false;
            }
        });

    	mEnterAnimSet.play(animQuickSettingTranslation);
    	mEnterAnimSet.play(animQuickSettingAlpha);
    	mEnterAnimSet.start();
    }
    
    
    public void continueAutoQuickSettingEnterAnimation(){

    	Log.e("222222", "-----continueAutoQuickSettingEnterAnimation mListening-------" + mListening);
    	if (!mListening) {
    		mListening = true;
    		mQuickSettingPanel.setListening(mListening);
    	}
    	setMaskAlpha(MASK_VIEW_FULL_ALPHA);
    	
    	int translateZ = (int) ((isOrientationPortrait())
            ? mQuickSetting.getTranslationY()
            : mQuickSetting.getTranslationX());
    	if(translateZ <= quickSettingAnimStart){
    		translateZ = quickSettingAnimStart;
    	}else if(translateZ >= quickSettingAnimEnd){
    		translateZ = quickSettingAnimEnd;
    	}

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingTranslation.cancel()");
            animQuickSettingTranslation.cancel();
        }

		String orientation = "translationY";
		if(!isOrientationPortrait()){
			orientation = "translationX";
		}

    	animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, orientation, translateZ,quickSettingAnimEnd);

    	animQuickSettingTranslation.setDuration(HandlerBar.ANIMATION_CONTINUE_ENTER_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingAlpha.cancel()");
            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),1f);

    	animQuickSettingAlpha.setDuration(HandlerBar.ANIMATION_CONTINUE_ENTER_DURATION);

    	mEnterAnimSet = new AnimatorSet();
    	mEnterAnimSet.play(animQuickSettingTranslation);
    	mEnterAnimSet.play(animQuickSettingAlpha);
        mEnterAnimSet.addListener(new AnimatorListenerAdapter() {
            boolean canceled;
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
                canceled = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                super.onAnimationCancel(animation);
                canceled = true;
                mAnimating = false;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                if (canceled) return;

                handler.postDelayed(mDisableHandlerRunnable, 0);

                mAnimating = false;
            }
        });
    	mEnterAnimSet.start();
    }

    public void autoQuickSettingQuitAnimation(){

    	Log.e("222222", "-----autoQuickSettingQuitAnimation mListening-------" + mListening);
    	if (mListening) {
    		mListening = false;
    		mQuickSettingPanel.setListening(mListening);
    	}
        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingTranslation.cancel()");
            animQuickSettingTranslation.cancel();
        }

		if(isOrientationPortrait()){
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY", quickSettingAnimEnd,quickSettingAnimStart);
		} else {
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX", quickSettingAnimEnd,quickSettingAnimStart);
		}

    	animQuickSettingTranslation.setDuration(QUIT_ANIMATION_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingAlpha.cancel()");
            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),0f);

    	animQuickSettingAlpha.setDuration(QUIT_ANIMATION_DURATION);

        mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);

        if (bgAlpha != null && bgAlpha.isRunning()) {

            bgAlpha.cancel();
        }
    	bgAlpha = ObjectAnimator.ofInt(mRecentsMaskView.getBackground(), "alpha", sMaskAlpha,0);

    	bgAlpha.setDuration(QUIT_ANIMATION_DURATION);

    	animQuickSettingTranslation.addListener(new AnimatorListenerAdapter(){

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
            }

			@Override
			public void onAnimationCancel(Animator arg0) {
                mAnimating = false;
				setRecentsMaskViewAlpha();
				mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
				
			}

			@Override
			public void onAnimationEnd(Animator arg0) {

                mAnimating = false;

				setRecentsMaskViewAlpha();

				mHandlerBarCallback.removeRecentPanelView();
			}
			
		}
    	);
    	AnimatorSet animSet = new AnimatorSet();
    	animSet.play(animQuickSettingAlpha);
    	animSet.play(animQuickSettingTranslation);
    	animSet.play(bgAlpha);
    	animSet.start();
    }
    
    public void autoQuickSettingQuitAnimation(final boolean isShowHandlerBar){
    	Log.e("222222", "-----autoQuickSettingQuitAnimation  isShowHandlerBar-------");
        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){

            animQuickSettingTranslation.cancel();
        }

		if(isOrientationPortrait()){
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY", quickSettingAnimEnd,quickSettingAnimStart);
		} else {
				animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX", quickSettingAnimEnd,quickSettingAnimStart);
		}

    	animQuickSettingTranslation.setDuration(QUIT_ANIMATION_DURATION);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingAlpha.cancel()");
            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),0f);

    	animQuickSettingAlpha.setDuration(QUIT_ANIMATION_DURATION);


        mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);

        if (bgAlpha != null && bgAlpha.isRunning()) {
            bgAlpha.cancel();
        }
        bgAlpha = ObjectAnimator.ofInt(mRecentsMaskView.getBackground(), "alpha",sMaskAlpha,0);
        bgAlpha.setDuration(QUIT_ANIMATION_DURATION);

    	animQuickSettingTranslation.addListener(new AnimatorListenerAdapter(){
			@Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                mAnimating = true;
            }

            @Override
			public void onAnimationCancel(Animator arg0) {

                mAnimating = false;
				setRecentsMaskViewAlpha();
				mHandlerBarCallback.setRecentsPanelViewBackgroundAlpha(0);
				
			}

			@Override
			public void onAnimationEnd(Animator arg0) {
                mAnimating = false;
				setRecentsMaskViewAlpha();

				mHandlerBarCallback.removeRecentPanelView(isShowHandlerBar);
			}
			
		}
    	);
    	AnimatorSet animSet = new AnimatorSet();
    	animSet.play(animQuickSettingAlpha);
    	animSet.play(animQuickSettingTranslation);
        animSet.play(bgAlpha);
    	animSet.start();
    }
    
 	public void scrollToOriginalLocation(){

 		((RecentsLayout)mRecentsMaskView).startScroll(0, mRecentsMaskView.getScrollY(), 0,
            -mRecentsMaskView.getScrollY(), QUIT_ANIMATION_DURATION * 2);
 	}
 	
 	public View getMoveView(){
 		return mRecentsMaskView;
 	}
 	
 	public View getQuickSettingView(){

 		return mQuickSetting;
 	}
 	
 	public void autoQuickSettingEnterAnimationForZ(int scrollZ){
 		Log.e("222222", "-----autoQuickSettingEnterAnimationForZ-------");
 		if(scrollZ>HandlerBar.mRecentsScrimHeight){
 			return;
 		}

        if (animQuickSettingTranslation != null && animQuickSettingTranslation.isRunning()){

            animQuickSettingTranslation.cancel();
        }
        if (isOrientationPortrait()) {
            animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationY",
                    mQuickSetting.getTranslationY() < quickSettingAnimStart ? quickSettingAnimStart : mQuickSetting.getTranslationY(),
                    quickSettingAnimEnd);
        } else {
            animQuickSettingTranslation = ObjectAnimator.ofFloat(mQuickSetting, "translationX",
                    mQuickSetting.getTranslationX() < quickSettingAnimStart ? quickSettingAnimStart : mQuickSetting.getTranslationX(),
                    quickSettingAnimEnd);
        }

    	animQuickSettingTranslation.setDuration(500);

        if (animQuickSettingAlpha != null && animQuickSettingAlpha.isRunning()){
            Log.d("felix","RecentsPanelView.DEBUG animQuickSettingAlpha.cancel()");
            animQuickSettingAlpha.cancel();
        }
    	animQuickSettingAlpha = ObjectAnimator.ofFloat(mQuickSetting, "alpha", mQuickSetting.getAlpha(),1f);

    	animQuickSettingAlpha.setDuration(500);
    	AnimatorSet animSet = new AnimatorSet();

        animSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
            }
        });
    	animSet.play(animQuickSettingTranslation);
    	animSet.play(animQuickSettingAlpha);
    	animSet.start();
    }
 	
 	public void closeHardAccelerate(){
 		mHandlerBarCallback.getRecentsPanelView().setLayerType(View.LAYER_TYPE_NONE,null);
    	mRecentsMaskView.setLayerType(View.LAYER_TYPE_NONE,null);
    	mQuickSetting.setLayerType(View.LAYER_TYPE_NONE,null);
    }
 	
    public void openHardAccelerate(){
    	mHandlerBarCallback.getRecentsPanelView().setLayerType(View.LAYER_TYPE_HARDWARE,null);
    	mRecentsMaskView.setLayerType(View.LAYER_TYPE_HARDWARE,null);
    	mQuickSetting.setLayerType(View.LAYER_TYPE_HARDWARE,null);
    }
    
    public void scrollToOriginalLocationNoAnimation(){
    	mRecentsMaskView.scrollTo(0, 0);
    }

    private static volatile int sMaskAlpha = 0; // Hold current alpha of mRecentsMaskView
    /**
     * Set RecentsMaskView`s background alpha and remember it.
     * Because Android doesn`t support Drawable.getAlpha() untill api-level 19
     * @param alpha to set
     * 
     * @author Felix.Duan
     * @date 2014-5-8
     */
    private void setMaskAlpha(int alpha) {
        mRecentsMaskView.getBackground().setAlpha(alpha);
        sMaskAlpha = alpha;
    }

    private Runnable mDisableHandlerRunnable = new Runnable() {
        @Override
        public void run() {
            if (isShown()) {
                Intent intent = new Intent(HandlerBar.DISABLE_HANDLER);
                mContext.sendBroadcastAsUser(intent, new UserHandle(UserHandle.USER_CURRENT));
            }
        }
    };

    private Runnable mUpdateImeStateRunnable = new Runnable() {
        @Override
        public void run() {
            if (isShowInputMethod) {
            	if(isShown()){
            		removeRecentsPanelView(false);
            	}else{
            		mHandlerBarCallback.showHandlerBarView(false);
            	}
                if (InvokerService.getInstance() != null) // Avoid NullPointer Exception when not using InvokerService
                    InvokerService.getInstance().updateInputMethodStatus(isShowInputMethod);
            } else {
                mHandlerBarCallback.showHandlerBarView(true);
                isShowInputMethod = false;
                if (InvokerService.getInstance() != null) // Avoid NullPointer Exception when not using InvokerService
                    InvokerService.getInstance().updateInputMethodStatus(isShowInputMethod);
            }
        }
    };

    public void updateImeState(boolean showing) {
        isShowInputMethod = showing;
        handler.removeCallbacks(mUpdateImeStateRunnable);
        handler.postDelayed(mUpdateImeStateRunnable, 100);
    }
}
