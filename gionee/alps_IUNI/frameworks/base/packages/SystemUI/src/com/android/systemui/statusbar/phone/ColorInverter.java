package com.android.systemui.statusbar.phone;

import java.util.HashSet;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//M;tymy batteryPercentage_color begin
import com.android.systemui.R;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import com.android.systemui.statusbar.policy.AuroraBatteryController;
//M;tymy batteryPercentage_color begin

/**
 * 
 * @author xiejun
 *this is for immersion type
 *
 */

public class ColorInverter {
	private static final String TAG = ColorInverter.class.getSimpleName();
	private boolean DEBUG = false;
	private HashSet<View> mViewSet = new HashSet<View>();
	private ViewGroup mSourceView;
	private int mColor;
	private int mStatusbarColor;
	public static final int MODE_OPAQUE = 0;
    public static final int MODE_SEMI_TRANSPARENT = 1;
    public static final int MODE_TRANSLUCENT = 2;
    public static final int MODE_LIGHTS_OUT = 3;
    public static final int MODE_TRANSPARENT = 4;
    public static final int MODE_WARNING = 5;
    public static final int MODE_LIGHTS_OUT_TRANSPARENT = 6;
    private int mDefaultColor = 0xffffffff;
	private PorterDuffColorFilter mColorFilter = new PorterDuffColorFilter(mDefaultColor, PorterDuff.Mode.MULTIPLY);;
	public static final int INVERT_TYPE_DEFAULT =0;
	public static final int INVERT_TYPE_BLACK = INVERT_TYPE_DEFAULT;
	public static final int INVERT_TYPE_WHITE = INVERT_TYPE_BLACK+1;
	private static int WHITE_RANGE=60;
	private static int ALPHA_RANGE=80;
	private int mLastInvertColor = 0;
	private int mOpaque;
    private int mSemiTransparent;
    private int mTransparent;
    private int mWarning;
    private Context mContext;
    
  //M;tymy batteryPercentage_color begin
    private boolean mIsCharge;
  //M;tymy batteryPercentage_color end
    private HashSet<ViewGroup> mSourceViewSet = new HashSet<ViewGroup>();
    
    public ColorInverter(Context context){
    	mContext = context;
    	context.registerReceiver(mbatteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }
    
	public ColorInverter(Context context,ViewGroup viewGroup){
		Log.i(TAG,"mSourceView = "+mSourceView);
		mSourceView =viewGroup;
		//M;tymy batteryPercentage_color begin
		mContext = context;
		context.registerReceiver(mbatteryReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		//M;tymy batteryPercentage_color end
	}
	
	public void setmSourceView(ViewGroup sourceView) {
		mSourceView = sourceView;
	}
	
	public void addSourceView(ViewGroup sourceView){
		mSourceViewSet.add(sourceView);
	}
	public /*synchronized*/ void invert(int imerseStatusbarViewColor){
		if(DEBUG){
			Log.i(TAG,"begin invert status bar views mViewSet.count = "+mViewSet.size());
		}
		clearAllViewSet();
		mColor = getInvertColor(imerseStatusbarViewColor,mStatusbarColor);
		for(ViewGroup vg:mSourceViewSet){
			collectViews(vg);
		}
		
		if(DEBUG){
			Log.i(TAG,"inverting status bar views mViewSet.count = "+mViewSet.size());
		}
		invertAll();
		if(DEBUG){
			Log.i(TAG,"end invert status bar views mViewSet.count = "+mViewSet.size());
		}
	}

	private void invertAll() {
		for(View v : mViewSet){
			if(v instanceof TextView ){
				invert((TextView)v);
			}else if(v instanceof ImageView){
				invert((ImageView)v);
			}else{
				//Log.i(TAG,"View is not TextView and ImageView,we don't invert it's color. this view is "+v);
			}
		}
	}

	private void collectViews(ViewGroup source) {
		if(source ==null||source!=null&&source.getChildCount()<=0){
			if(DEBUG){
				Log.i(TAG," source View group is null or it's child count<0");
			}
			return;
		}
		int count = source.getChildCount();
		for(int i=0;i<count;i++){
			View child = source.getChildAt(i);
			if(child instanceof ImageView || child instanceof TextView){
				mViewSet.add(child);
			}else if(child instanceof ViewGroup){
				collectViews((ViewGroup)child);
			}
			
		}
	}

	private void clearAllViewSet() {
		mViewSet.clear();
	}
	
	private void invert(TextView tv){
		//M;pgm batteryPercentage_color begin
	//M;tymy batteryPercentage_color begin
		//if((tv.getId() == R.id.percentageTv) && mIsCharge){
			//don't invert the percentageTv
			  tv.setTextColor(mColor);
	//	}else{
    //M;tymy batteryPercentage_color end
		//  tv.setTextColor(mColor);
		//}
		//M;pgm batteryPercentage_color end	  
	}
	
	private void invert(ImageView iv){
		mColorFilter.setColor(mColor);
		if(iv.getId()==R.id.batteryIv){
			if(AuroraBatteryController.isLowBatteryAndNotCharging){
				if(mColor == 0xffffffff){
					iv.setImageResource(R.drawable.aurora_stat_sys_battery_15);
				}else{
					iv.setImageResource(R.drawable.aurora_stat_sys_battery_black_15);
				}
				return;
			}
		}
		Drawable drawable = iv.getBackground();
		if(drawable == null){
			drawable = iv.getDrawable();
		}
		if(drawable!=null){
			if(drawable instanceof AnimationDrawable){
				int size=((AnimationDrawable)drawable).getNumberOfFrames();
				drawable.setColorFilter(mColorFilter);
	        	for(int i=0;i<size;i++){
	        		((AnimationDrawable)drawable).getFrame(i).setColorFilter(mColorFilter);
				} 
			}else{
				if(DEBUG){
					Log.i(TAG,"invert ImagerView iv = "+iv + "  drawable  instance of VectorDrawable :  "+(drawable  instanceof VectorDrawable));
				}
				if(canTicktInvert(iv)){
					iv.setColorFilter(mColorFilter);
					drawable.setColorFilter(mColorFilter);
				}
			}      	
		}
	}
	
	public int getInvertColor(int  imerseStatusbarViewColor, int statusbarColor){
		int immerseColor = imerseStatusbarViewColor;
		return immerseColor;
	}
	
	//M;tymy batteryPercentage_color begin
	private BroadcastReceiver mbatteryReceiver = new BroadcastReceiver(){
        public void onReceive(Context context, Intent intent){
        	String action =intent.getAction();
        	if(Intent.ACTION_BATTERY_CHANGED.equals(action)){
        		int status=intent.getIntExtra("status",BatteryManager.BATTERY_STATUS_UNKNOWN);
        		if(status==BatteryManager.BATTERY_STATUS_CHARGING||status == BatteryManager.BATTERY_STATUS_FULL){
        			mIsCharge = true;
        		}else{
        			mIsCharge = false;
        		}
        	}
        }
    };
    
    public void clear(){
    	mSourceViewSet.clear();
    	mContext.unregisterReceiver(mbatteryReceiver);
    }
  //M;tymy batteryPercentage_color end
    
    public boolean canTicktInvert(ImageView v){
		if (v != null && v.getParent() instanceof ImageSwitcher) {
			if (v.getTag() != null
					&& !isInvertApp(v.getTag().toString())) {
				return false;
			}
		}
    	return true;	
    }

	private boolean isInvertApp(String packageName) {
		if(packageName != null && ("android".equals(packageName) 
        		|| "com.android.providers.downloads".equals(packageName) 
        		|| "com.android.systemui".equals(packageName) 
        		|| "com.android.mms".equals(packageName)
        		||"com.mediatek.mtklogger".equals(packageName))){
			return true;
		}
		return false;
	}
}
