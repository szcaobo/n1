/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.systemui.qs.tiles;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.provider.Settings;
import android.provider.Settings.Global;
import android.util.Log;

import com.android.systemui.R;
import com.android.systemui.qs.AuroraMuteAndVibrateLinkage;
import com.android.systemui.qs.GlobalSetting;
import com.android.systemui.qs.QSTile;
import com.android.systemui.qs.QSTile.ResourceIcon;

import com.mediatek.systemui.ext.BehaviorSet;
import com.mediatek.systemui.ext.PluginFactory;
import com.mediatek.xlog.Xlog;

import com.mediatek.audioprofile.AudioProfileManager;
import android.media.AudioManager;
import android.util.Log;

//M; tymy QuickSettings   new a class:MuteTile

/** Quick settings tile: Mute mode **/
public class MuteTile extends QSTile<QSTile.BooleanState> {

    private static final String TAG = "MuteTile";
    private static final boolean DEBUG = true; 
    private final GlobalSetting mSetting;
    private boolean mListening;
    private AudioManager mAudioManager = null;
    private AuroraMuteAndVibrateLinkage mAuroraMuteAndVibrateLinkage;
    
    public MuteTile(Host host) {
        super(host);
        mSetting = new GlobalSetting(mContext, mHandler, Global.MODE_RINGER) {
            @Override
            protected void handleValueChanged(int value) {
                refreshState();            
            }
        };
        mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
        mAuroraMuteAndVibrateLinkage=new AuroraMuteAndVibrateLinkage(mContext,mAudioManager);
        
    }

    @Override
    protected BooleanState newTileState() {
        return new BooleanState();       
    }

    @Override
    public void handleClick() {
    	Log.v(TAG,"MuteTile-------mState.value= "+mState.value);
        mAuroraMuteAndVibrateLinkage.silentChecked(!mState.value);
    }

    @Override
    public void handleLongClick() {
    	 Intent intent =  new Intent("gn.com.android.audioprofile.action.AUDIO");
    	 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
    	 mHost.startSettingsActivity(intent);
    }

    @Override
    protected void handleUpdateState(BooleanState state, Object arg) {    
    	boolean muteMode=mAuroraMuteAndVibrateLinkage.isSilent();
    	
        state.value = muteMode;
        state.visible = true;
        state.label = mContext.getString(R.string.status_bar_settings_mute_label);
        if (muteMode) {
        	//state.color=mContext.getResources().getColor(R.color.qs_title_color_select);
        	state.color=mContext.getResources().getColor(R.color.aurora_toolbar_color_enable);
            state.icon = ResourceIcon.get(R.drawable.aurora_toolbar_silent_enable_svg);
            state.contentDescription =  mContext.getString(
                    R.string.accessibility_quick_settings_mute_on);
        } else {
        	//state.color=mContext.getResources().getColor(R.color.qs_title_color_normal);
        	state.color=mContext.getResources().getColor(R.color.aurora_toolbar_color_disable);
            state.icon = ResourceIcon.get(R.drawable.aurora_toolbar_silent_off_svg);
            state.contentDescription =  mContext.getString(
                    R.string.accessibility_quick_settings_mute_off);
        }
    }

    @Override
    protected String composeChangeAnnouncement() {
        if (mState.value) {
            return mContext.getString(R.string.accessibility_quick_settings_mute_changed_on);
        } else {
            return mContext.getString(R.string.accessibility_quick_settings_mute_changed_off);
        }
    }

    public void setListening(boolean listening) {
        if (mListening == listening) return;
        mListening = listening;
        if (listening) {
            final IntentFilter filter = new IntentFilter();
            filter.addAction(AudioManager.RINGER_MODE_CHANGED_ACTION);
            mContext.registerReceiver(mReceiver, filter);
        } else {
            mContext.unregisterReceiver(mReceiver);
        }
        mSetting.setListening(listening);        
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ((AudioManager.RINGER_MODE_CHANGED_ACTION).equals(intent.getAction())) {          	
                refreshState();
            }
        }
    };
}
