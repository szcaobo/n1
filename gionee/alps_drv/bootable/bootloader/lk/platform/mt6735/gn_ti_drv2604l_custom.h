#ifndef GN_TI_DRV2604L_CUSTOM_H
#define GN_TI_DRV2604L_CUSTOM_H
#include <platform/mt_gpio.h>


#define DRV2604L_I2C_BUS_ID 2
#define DRV2604L_I2C_ADDR			0x5A
#define DRV2604L_GPIO_ENABLE_PIN GPIO86|0x80000000


#define MODE_REG            0x01
#define MODE_STANDBY_MASK           0x40
#define MODE_STANDBY        0x40
#define MODE_RESET                  0x80
#define DRV2604_MODE_MASK           0x07
#define MODE_INTERNAL_TRIGGER       0
#define MODE_EXTERNAL_TRIGGER_EDGE  1
#define MODE_EXTERNAL_TRIGGER_LEVEL 2
#define MODE_PWM_OR_ANALOG_INPUT    3
#define MODE_AUDIOHAPTIC            4
#define MODE_REAL_TIME_PLAYBACK     5
#define MODE_DIAGNOSTICS            6
#define AUTO_CALIBRATION            7

/*
** Real Time Playback
*/
#define REAL_TIME_PLAYBACK_REG		0x02

/*
** Library Selection
*/
#define LIBRARY_SELECTION_REG       0x03
#define LIBRARY_SELECTION_DEFAULT   0x00
#define LIBRARY_SELECTION_HIZ_MASK          0x10
#define LIBRARY_SELECTION_HIZ_EN            1
#define LIBRARY_SELECTION_HIZ_DIS           0

/*
** Waveform Sequencer
*/
#define WAVEFORM_SEQUENCER_REG      0x04
#define WAVEFORM_SEQUENCER_REG2     0x05
#define WAVEFORM_SEQUENCER_REG3     0x06
#define WAVEFORM_SEQUENCER_REG4     0x07
#define WAVEFORM_SEQUENCER_REG5     0x08
#define WAVEFORM_SEQUENCER_REG6     0x09
#define WAVEFORM_SEQUENCER_REG7     0x0A
#define WAVEFORM_SEQUENCER_REG8     0x0B
#define WAVEFORM_SEQUENCER_MAX      8
#define WAVEFORM_SEQUENCER_DEFAULT  0x00

/*
** OverDrive Time Offset
*/
#define OVERDRIVE_TIME_OFFSET_REG  0x0D

/*
** Sustain Time Offset, postive
*/
#define SUSTAIN_TIME_OFFSET_POS_REG 0x0E

/*
** Sustain Time Offset, negative
*/
#define SUSTAIN_TIME_OFFSET_NEG_REG 0x0F

/*
** Brake Time Offset
*/
#define BRAKE_TIME_OFFSET_REG       0x10

/*
** Rated Voltage
*/
#define RATED_VOLTAGE_REG           0x16

/*
** Overdrive Clamp Voltage
*/
#define OVERDRIVE_CLAMP_VOLTAGE_REG 0x17

/*
** Auto Calibrationi Compensation Result
*/
#define AUTO_CALI_RESULT_REG        0x18

/*
** Auto Calibration Back-EMF Result
*/
#define AUTO_CALI_BACK_EMF_RESULT_REG 0x19

/*
** Feedback Control
*/
#define FEEDBACK_CONTROL_REG        0x1A
#define FEEDBACK_CONTROL_DEVICE_TYPE_MASK        0x80
#define FEEDBACK_CONTROL_BEMF_ERM_GAIN0 0 // 0.33x
#define FEEDBACK_CONTROL_BEMF_ERM_GAIN1 1 // 1.0x
#define FEEDBACK_CONTROL_BEMF_ERM_GAIN2 2 // 1.8x
#define FEEDBACK_CONTROL_BEMF_ERM_GAIN3 3 // 4.0x

#define FEEDBACK_CONTROL_BEMF_LRA_GAIN0 0 // 5x
#define FEEDBACK_CONTROL_BEMF_LRA_GAIN1 1 // 10x
#define FEEDBACK_CONTROL_BEMF_LRA_GAIN2 2 // 20x
#define FEEDBACK_CONTROL_BEMF_LRA_GAIN3 3 // 30x

#define FEEDBACK_CONTROL_LOOP_GAIN_MASK        0x0C
#define LOOP_RESPONSE_SLOW      (0 << 2)
#define LOOP_RESPONSE_MEDIUM    (1 << 2) // default
#define LOOP_RESPONSE_FAST      (2 << 2)
#define LOOP_RESPONSE_VERY_FAST (3 << 2)

#define FEEDBACK_CONTROL_FB_BRAKE_MASK        0x70
#define FB_BRAKE_FACTOR_1X   (0 << 4) // 1x
#define FB_BRAKE_FACTOR_2X   (1 << 4) // 2x
#define FB_BRAKE_FACTOR_3X   (2 << 4) // 3x (default)
#define FB_BRAKE_FACTOR_4X   (3 << 4) // 4x
#define FB_BRAKE_FACTOR_6X   (4 << 4) // 6x
#define FB_BRAKE_FACTOR_8X   (5 << 4) // 8x
#define FB_BRAKE_FACTOR_16X  (6 << 4) // 16x
#define FB_BRAKE_DISABLED    (7 << 4)

#define FEEDBACK_CONTROL_MODE_ERM 0 // default
#define FEEDBACK_CONTROL_MODE_LRA (1 << 7)

#define Control1_REG            	0x1B
#define Control1_REG_AC_COUPLE_MASK	0x20
#define Control1_REG_DRIVE_TIME_MASK	0x1f


#define Control2_REG            		0x1C
#define Control2_REG_BIDIR_INPUT_MASK   0x80

#define	BIDIR_INPUT_UNIDIRECTIONAL	(0<<7)
#define	BIDIR_INPUT_BIDIRECTIONAL	(1<<7)
#define IDISS_TIME_MASK         0x03
#define IDISS_TIME_VERY_SHORT   0
#define IDISS_TIME_SHORT        1
#define IDISS_TIME_MEDIUM       2 // default
#define IDISS_TIME_LONG         3

#define BLANKING_TIME_MASK          0x0C
#define BLANKING_TIME_VERY_SHORT    (0 << 2)
#define BLANKING_TIME_SHORT         (1 << 2)
#define BLANKING_TIME_MEDIUM        (2 << 2) // default
#define BLANKING_TIME_VERY_LONG     (3 << 2)

#define AUTO_RES_SAMPLE_TIME_MASK	0x30
#define AUTO_RES_SAMPLE_TIME_150us	(0 << 4)
#define AUTO_RES_SAMPLE_TIME_200us  (1 << 4)
#define AUTO_RES_SAMPLE_TIME_250us  (2 << 4) // default
#define AUTO_RES_SAMPLE_TIME_300us  (3 << 4)



#define BIDIR_INPUT_MASK           0x80
#define UNIDIRECT_INPUT            (0 << 7)
#define BRAKE_STABLIZER   (1<<6)
#define BIDIRECT_INPUT             (1 << 7) // default

/*
** Control3
*/
#define Control3_REG 0x1D
#define Control3_REG_LOOP_MASK		0x21
#define Control3_REG_PWMANALOG_MASK	0x02
#define Control3_REG_FORMAT_MASK	0x08
#define INPUT_PWM               (0 << 1) // default
#define INPUT_ANALOG            (1 << 1)
#define ERM_OpenLoop_Enabled    (1 << 5)
#define ERM_OpenLoop_Disable    (0 << 5)
#define LRA_OpenLoop_Enabled    (1 << 0)
#define LRA_OpenLoop_Disable    (0 << 0)
#define RTP_FORMAT_SIGNED	    (0 << 3)
#define RTP_FORMAT_UNSIGNED	    (1 << 3)
#define NG_Thresh_DISABLED      (0 << 6)
#define NG_Thresh_1             (1 << 6)
#define NG_Thresh_2             (2 << 6)
#define NG_Thresh_3             (3 << 6)

/*
** Control4
*/
#define Control4_REG 	0x1E
#define Control4_REG_OTP_MASK   0x04
#define Control4_REG_CAL_TIME_MASK 	0x30
#define AUTOCAL_TIME_150MS          (0 << 4)
#define AUTOCAL_TIME_250MS          (1 << 4)
#define AUTOCAL_TIME_500MS          (2 << 4)
#define AUTOCAL_TIME_1000MS         (3 << 4)
#define Control4_REG_ZC_DET_MASK   	0xC0
#define ZC_DET_TIME_100us          	(0 << 4)
#define ZC_DET_TIME_200us          	(1 << 4)
#define ZC_DET_TIME_300us          	(2 << 4)
#define ZC_DET_TIME_390us        	(3 << 4)

/*
** Control5
*/
#define Control5_REG 	0x1F
#define	BLANK_IDISS_MSB_MASK	0x0f
#define	BLANK_IDISS_MSB_CLEAR	0

/*
** LRA Open Loop Period
*/
#define LRA_OPENLOOP_PERIOD_REG 0x20

#define SILICON_REVISION_REG        0x3B
#define SILICON_REVISION_MASK       0x07

#define	RAM_ADDR_UPPER_BYTE_REG		0xfd
#define	RAM_ADDR_LOWER_BYTE_REG		0xfe
#define	RAM_DATA_REG				0xff

#define MAX_TIMEOUT 	10000 /* 10s */
#define	MAX_READ_BYTES	0xff

#define SW_STATE_IDLE				0x00
#define SW_STATE_AUDIO2HAPTIC			0x01
#define SW_STATE_SEQUENCE_PLAYBACK		0x02
#define SW_STATE_RTP_PLAYBACK			0x04

#define DEV_IDLE	                0 // default
#define DEV_STANDBY					1
#define DEV_READY					2

#define	WORK_IDLE					0x00
#define WORK_RTP			      	0x06
#define WORK_CALIBRATION	      	0x07
#define WORK_VIBRATOR		      	0x08
#define	WORK_PATTERN_RTP_ON			0x09
#define WORK_PATTERN_RTP_OFF      	0x0a
#define WORK_SEQ_RTP_ON		      	0x0b
#define WORK_SEQ_RTP_OFF    	  	0x0c
#define WORK_SEQ_PLAYBACK    	  	0x0d

#define YES 1
#define NO  0

/* recommendations from data sheet */
#define		FB_BRAKE_FACTOR			FB_BRAKE_FACTOR_3X
#define		LOOP_GAIN				LOOP_RESPONSE_FAST
#define		AUTO_CAL_TIME			AUTOCAL_TIME_1000MS
#define		AUTO_RES_SAMPLE_TIME	AUTO_RES_SAMPLE_TIME_300us
#define		BLANKING_TIME			BLANKING_TIME_SHORT
#define		IDISS_TIME				IDISS_TIME_SHORT
#define		ZC_DET_TIME				ZC_DET_TIME_100us

#endif

