#include "backup.h"
#include "libnvram.h"
#include "libfile_op.h"
#include <cutils/properties.h>
#include <errno.h>
/* Gionee chengx 20140403 add for CR01167528 begin */
#include "../../../custom/gionee6753_65c_l1/cgen/inc/Custom_NvRam_LID.h"

/* Gionee Qux. Prevent the IOCTRL deinition from being different from kernel one,
 * especially on the new PLATFORM.
 */
#include "../../../../../../kernel-3.10/include/linux/sensors_io.h"

#define MAX_RETRY_COUNT                      20
#define HWM_PSENSOR_DEV                     "/dev/als_ps"
/* Gionee chengx 20140403 add for CR01167528 end */

#define     PCALI_TAG       "[psensor_cali]: "
#define PSENSORLOGD(fmt, args...)   ALOGD(PCALI_TAG"--%d--"fmt, __LINE__, ##args)
#define PSENSORLOGE(fmt, args...)   ALOGD(PCALI_TAG"--%d--"fmt, __LINE__, ##args)
extern int iFilePRODUCT_INFOLID;
struct PS_CALI_DATA_STRUCT
{
    int close;
    int far_away;
    int valid;
};

struct PS_CALI_SHOW_STRUCT
{
    struct PS_CALI_DATA_STRUCT ps_cali_backup;
    int threshold;
    int ps;
};

/**********moxizheng add for CR01158065 ************/
#define BARCODE_LEN 64
#define COLOR_BIT 44

static int wrtite_ui_color(int color)
{
	int fd = 0;
			int err ;
			int flags = - 1;
			int res = 0;
			F_ID ps_nvram_fd;
			
			int file_lid_ps = AP_CFG_REEB_PRODUCT_INFO_LID;
			int rec_size_ps,rec_num_ps;
			int pPara;
		
			int read_nvram_ready_retry = 0;
		  
		   //wait for nvram deamon
			char value[PROPERTY_VALUE_MAX];
			while(read_nvram_ready_retry < MAX_RETRY_COUNT)
			{
				read_nvram_ready_retry++;
				property_get("service.nvram_init", value, NULL);
				if(strcmp(value ,"Ready") == 0)
				{
					PSENSORLOGE(" the nvram server is ready");
					break;
				}
				else
				{
					PSENSORLOGE(" the nvram server is not ready so usleep 500ms");
					usleep(500*1000);
				}
			}
			PSENSORLOGD(" Get nvram restore ready retry cc = %d\n",read_nvram_ready_retry);
			if(read_nvram_ready_retry >= MAX_RETRY_COUNT)
			{
				PSENSORLOGE(" get nvram ready failed \n");
				return -1;
			}
			 //******************* read ps nvram begin ****************************//  
			ps_nvram_fd = NVM_GetFileDesc(file_lid_ps,&rec_size_ps,&rec_num_ps,ISREAD);
			if(ps_nvram_fd.iFileDesc<0)
			{
				PSENSORLOGE(" read the nvram information of file_lid_ps = %d failed \n", file_lid_ps);
				return -1;
			}
			PSENSORLOGD(" the nvrame information of file_lid_ps ps_nvram_fd = %d, rec_size = %d,rec_num = %d\n", ps_nvram_fd, rec_size_ps, rec_num_ps);
			//printf("========== rec_size = %d,rec_num = %d,====\n",rec_size_ps,rec_num_ps);

			memset(value, '\0', sizeof(value));
			res = read(ps_nvram_fd.iFileDesc,value,sizeof(value) );
			if(res<0)
			{
				PSENSORLOGE(" read the nvram error res=%d\n",res);
				return -1;
			}
			PSENSORLOGD(" read the nvram data =%s\n",value);
			NVM_CloseFileDesc(ps_nvram_fd);
			//******************* read ps nvram end****************************// 
			//***********write nvram********************//
			ps_nvram_fd = NVM_GetFileDesc(file_lid_ps,&rec_size_ps,&rec_num_ps,ISWRITE);
			if(ps_nvram_fd.iFileDesc<0)
			{
					PSENSORLOGE(" wtite open file_lid_ps = %d failed \n", file_lid_ps);
					return -1;
			}
	   	   value[COLOR_BIT] = color;	  
		   res = write(ps_nvram_fd.iFileDesc,value,BARCODE_LEN);
			if(res<0)
			{
				PSENSORLOGE(" read the nvram error res=%d\n",res);
				return -1;
			}
			PSENSORLOGE("write_data_to_pro_info ui color = %d  \n", (color-'0'));
		   NVM_CloseFileDesc(ps_nvram_fd);
		   return 1;

}
/**********moxizheng add for CR01158065 ************/

static int sendCalidataToPsensor()
{
    int fd = 0;
    int err ;
    int flags = - 1;
    int res = 0;
    F_ID ps_nvram_fd;
    PS_CALI_DATA_STRUCT ps_data = {0,0,0};
    PS_CALI_SHOW_STRUCT ps_data_show = {0,0,0,0,0};
    int file_lid_ps = AP_CFG_RDCL_FILE_PS_CALI_LID;
    int rec_size_ps,rec_num_ps;
    int pPara;

    int read_nvram_ready_retry = 0;
  
   //wait for nvram deamon
    char value[PROPERTY_VALUE_MAX];
    while(read_nvram_ready_retry < MAX_RETRY_COUNT)
	{
		read_nvram_ready_retry++;
        property_get("service.nvram_init", value, NULL);
        if(strcmp(value ,"Ready") == 0)
		{
            PSENSORLOGE(" the nvram server is ready");
			break;
		}
        else
		{
            PSENSORLOGE(" the nvram server is not ready so usleep 500ms");
			usleep(500*1000);
        }
	}
    PSENSORLOGD(" Get nvram restore ready retry cc = %d\n",read_nvram_ready_retry);
    if(read_nvram_ready_retry >= MAX_RETRY_COUNT)
    {
        PSENSORLOGE(" get nvram ready failed \n");
		return -1;
	}
     //******************* read ps nvram begin ****************************//  
	ps_nvram_fd = NVM_GetFileDesc(file_lid_ps,&rec_size_ps,&rec_num_ps,ISREAD);
    if(ps_nvram_fd.iFileDesc<0)
	{
        PSENSORLOGE(" read the nvram information of file_lid_ps = %d failed \n", file_lid_ps);
		return -1;
	}
	PSENSORLOGD(" the nvrame information of file_lid_ps ps_nvram_fd = %d, rec_size = %d,rec_num = %d\n", ps_nvram_fd, rec_size_ps, rec_num_ps);
	//printf("========== rec_size = %d,rec_num = %d,====\n",rec_size_ps,rec_num_ps);

    res = read(ps_nvram_fd.iFileDesc,&ps_data,rec_size_ps*rec_num_ps);
	if(res<0)
	{
	 	PSENSORLOGE(" read the nvram error res=%d\n",res);
	 	return -1;
	}
	PSENSORLOGD(" read the nvram of file_lid_ps data is ps_data.close = %d,ps_data.far_away = %d,ps_data.valid = %d\n",ps_data.close,ps_data.far_away,ps_data.valid);
	NVM_CloseFileDesc(ps_nvram_fd);
    //******************* read ps nvram end****************************// 

    fd = open(HWM_PSENSOR_DEV, O_RDONLY);
	if (fd < 0) 
	{
		PSENSORLOGE(" Couldn't open (%s) fd = %s ", HWM_PSENSOR_DEV,strerror(errno));
		return -1;
	}

    ps_data_show.ps_cali_backup.close = ps_data.close;
    ps_data_show.ps_cali_backup.far_away = ps_data.far_away;
    ps_data_show.ps_cali_backup.valid = ps_data.valid;

    err = ioctl(fd, ALSPS_SET_PS_CALI, &ps_data_show.ps_cali_backup);
    if (err) 
    {
        PSENSORLOGE(" set the cali value fail: (%s) %d", strerror(errno), err);
        return -1;            
    } 
	close(fd);
    PSENSORLOGD("ps calibration send data success! ");
    return 0;
}

void NvRAMBackupAgent::instantiate() {

int ret = (int)defaultServiceManager()->addService(descriptor, new NvRAMBackupAgent());

   if(ret != 0)
   {
       PSENSORLOGE("  instantiate add service error");
       exit(1);
   }
    PSENSORLOGD("  instantiate add service success");
}

NvRAMBackupAgent::NvRAMBackupAgent() {
    PSENSORLOGD(" create the NvRamBackupAgent");
}
  
status_t BnNvRAMBackupAgent::onTransact(uint32_t code,
			       const Parcel &data,
			       Parcel *reply,
			       uint32_t flags) {
	int ret=0;
    PSENSORLOGD(" BnNvRAMBackupAgent::onTransact (code = %u,flag = %u)", code, flags); 
    switch(code) {
    case TRANSACTION_backupBinregionAll: 
        {
	        
	        ret=testBackup();
	        if (ret==0)
			{
		        //NVRAM_LOG("backup Error\n");
                PSENSORLOGE(" testBackup error");
	        } 
			else 
			{
                PSENSORLOGD(" testBackup sucess");
                //NVRAM_LOG("backup done!\n");
		    }
	        return NO_ERROR;
        } 
        break;
    case TRANSACTION_readFile:
     {
		int iclose = 0;
		int ifarway = 0;
		int ivalid = 0;
        int ithreshold = 0;
        int ips = 0;
		CHECK_INTERFACE(INvRAMBackupAgent, data, reply);
		iclose  = data.readInt32();
	    ifarway = data.readInt32();
	    ivalid  = data.readInt32();
        ithreshold = data.readInt32();
        ips = data.readInt32();
	    
        ret= readFile(iclose, ifarway, ivalid, ithreshold,ips);
		if (ret==0) 
		{
            PSENSORLOGE(" TRANSACTION_readFile error");
            //NVRAM_LOG("weiwei readFile Error\n");
		} 
		else 
		{
            PSENSORLOGD(" TRANSACTION_readFile success");
		}
	    reply->writeInt32(iclose);
	    reply->writeInt32(ifarway);
	    reply->writeInt32(ivalid);
        reply->writeInt32(ithreshold);
        reply->writeInt32(ips);
	    return NO_ERROR;
     }
    break;
case TRANSACTION_writeFile:
     {
		CHECK_INTERFACE(INvRAMBackupAgent, data, reply);
		int iclose  = data.readInt32();
		int ifarway = data.readInt32();
		int ivalid  = data.readInt32();
        int ithreshold  = data.readInt32();
        int ips = data.readInt32();
		PSENSORLOGD(" wirte ==iclose = %d , ifarway = %d, ivalid = %d, ithreshold = %d, ips = %d,====\n",iclose, ifarway, ivalid, ithreshold, ips );
		ret= writeFile(iclose, ifarway, ivalid ,ithreshold, ips);
		if (ret==0) 
		{
            PSENSORLOGE(" TRANSACTION_writeFile error\n");
		} 
		else 
		{
            PSENSORLOGD(" TRANSACTION_writeFile error\n");
		}
		return NO_ERROR;
     }
    break;
/**********moxizheng add for CR01158065  start************/
case TRANSACTION_writeUIcolor:
    {
	CHECK_INTERFACE(INvRAMBackupAgent, data, reply);
	int color	= data.readInt32();//here value have be change because of encode,,but we do not need to change it to store in pro_info
	PSENSORLOGE(" TRANSACTION_writeUIcolor:%d\n",color);
	if(wrtite_ui_color(color) <= 0)
	 {
      PSENSORLOGE(" TRANSACTION_writeUIcolor:%d  fail....\n",(color-'0'));
	 }
    }
	break;
	/**********moxizheng add for CR01158065  ************/
    default:
	    return BBinder::onTransact(code, data, reply, flags);
    }

    return NO_ERROR;
}

int NvRAMBackupAgent::testBackup() {

    if(FileOp_BackupToBinRegion_All())
        return 1;
    else 
        return 0;
}

int NvRAMBackupAgent::readFile(int& iclose, int& ifarway, int &ivalid, int&ithreshold,int&ips)
{
    int err ;
    PS_CALI_DATA_STRUCT data;
    PS_CALI_SHOW_STRUCT data_show;
	int fd = open("/dev/als_ps",O_WRONLY); 
    if (fd < 0) 
	{
		PSENSORLOGE(" Couldn't open (%s) fd = %s ", HWM_PSENSOR_DEV,strerror(errno));
		return 0;
    }                  
	err = ioctl(fd, ALSPS_GET_PS_CALI, &data_show);   
    if (err)
	{
        PSENSORLOGE(" enable ps fail!");
        return 0;            
    }  
    
    iclose = data_show.ps_cali_backup.close;
    ifarway = data_show.ps_cali_backup.far_away;
    ivalid = data_show.ps_cali_backup.valid;   
    ithreshold = data_show.threshold;
    ips = data_show.ps; 
	close(fd); 
    PSENSORLOGD("[psneosr_cali] read ps raw iclose = %d , ifarway = %d, ivalid = %d ,ithreshold =%d, ips = %d ==== \n",iclose, ifarway, ivalid, ithreshold, ips);
	                                              
    return 1;
}

int NvRAMBackupAgent::writeFile(int iclose, int ifarway, int ivalid, int ithreshold, int ips)
{
    PS_CALI_DATA_STRUCT data;
    int file_lid_ps = AP_CFG_RDCL_FILE_PS_CALI_LID;
    int rec_size_ps,rec_num_ps;
    int pPara; 
    int res = 0 ;
	F_ID ps_nvram_fd;
    data.close = iclose;
    data.far_away = ifarway;
    data.valid = ivalid; 
   
    ps_nvram_fd = NVM_GetFileDesc(file_lid_ps,&rec_size_ps,&rec_num_ps,ISWRITE);
	if(ps_nvram_fd.iFileDesc<0)
	{
		PSENSORLOGE("[psensor_cal] ps_nvram_fd.iFileDesc =%d failed \n");
	  	return -1;
	}

	PSENSORLOGD("[psensor_cal] ps_nvram_fd = %d, weiweie rec_size = %d,rec_num = %d \n", ps_nvram_fd, rec_size_ps, rec_num_ps);
	res = write(ps_nvram_fd.iFileDesc,&data,rec_size_ps*rec_num_ps);
	if(res<0)
	{
	     PSENSORLOGD("read error.res=%d\n",res);
	 	return -1;
	}

	NVM_CloseFileDesc(ps_nvram_fd);

	return 1;
}

int main(int argc, char *argv[])
{
    NvRAMBackupAgent::instantiate();
    sendCalidataToPsensor();
    ProcessState::self()->startThreadPool();
	//sendCalidataToPsensor();
    NVRAM_LOG("NvRAMBackupAgent Service is now ready");
    IPCThreadState::self()->joinThreadPool();  
    return(0);
}

