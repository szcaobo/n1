#include "camera_custom_cam_cal.h"//for 658x new compilier option#include "camera_custom_eeprom.h"
#include <string.h>

CAM_CAL_TYPE_ENUM CAM_CALInit(void)
{
	return CAM_CAL_USED;
}

//Gionee <litao> <2015-07-23> modify for CR01525736 begin
unsigned int SUNNY_3M2_CAM_CALDeviceName(char* DevName)
{
    char* str= "SUNNY_CAM_CAL_DRV";
    strcat (DevName, str );
	return 0;
}

unsigned int OFILM_3M2_CAM_CALDeviceName(char* DevName)
{
    char* str= "OFILM_CAM_CAL_DRV";
    strcat (DevName, str );
	return 0;
}
//Gionee <litao> <2015-07-23> modify for CR01525736 end


unsigned int CAM_CALDeviceName(char* DevName)
{
    char* str= "CAM_CAL_DRV";
    strcat (DevName, str );
	return 0;
}

