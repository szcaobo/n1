LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := base-id
LOCAL_CERTIFICATE := platform
#$(info $(PLATFORM_VERSION_CODENAME))   -I aurora_framework/base/core/res/base-id.apk 
ifeq ("$(PLATFORM_VERSION_CODENAME)","AOSP")
LOCAL_AAPT_FLAGS := -x -I out/target/common/obj/APPS/framework-res_intermediates/package-export.apk
else
LOCAL_AAPT_FLAGS := -x7 
endif

LOCAL_AMIGOFRAMEWORK_RES := true

LOCAL_MODULE_TAGS := optional

# Install this alongside the libraries.
LOCAL_MODULE_PATH := $(TARGET_OUT_JAVA_LIBRARIES)

# Create package-export.apk, which other packages can use to get
# PRODUCT-agnostic resource data like IDs and type definitions.
LOCAL_EXPORT_PACKAGE_RESOURCES := true

include $(BUILD_PACKAGE)

