package aurora.widget;

import com.aurora.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import aurora.view.ViewPager;
import aurora.widget.AuroraViewPager;
import aurora.widget.ScrollIconLinearLayout;

/**
 * 主要功能:一个上下结构的布局,分为Tab和ViewPage两个部分<br/>
 * 描述:<br/>
 * 1.Tab和ViewPage的位置可调换(Tab默认在ViewPage上方).<br/>
 * 2.Tab上的文字内容\文字颜色\图片内容都可配置(图片和文字可同时或者选取其中一项显示)<br/>
 * 3.Tab上实现了一个游标,游标可实时指示当前ViewPage滑动进度(游标可设置为色块或者图片)<br/>
 * 4.游标在Tab上的位置可设置(游标默认在Tab底部)<br/>
 * 在布局中使用示例:<br/>
 * &lt;aurora.widget.AuroraTabWidget <br/> 
 * 	android:id="@+id/auroratabwidget"<br/>
 * 	android:layout_width="match_parent"<br/>
 * 	android:layout_height="match_parent"/&gt;<br/>
 * 可配置属性:<br/>
		aurora:textNormalColor = "@color/tab_color_text_normal"<br/>
        aurora:textFocusColor = "@color/tab_color_text_selected"<br/>
        aurora:textSize = "@dimen/cursor_item_text_size"<br/>
        aurora:entriesTitle="@array/titles" <br/>
        aurora:entriesTitleIconNormal="@array/titles_icon_normal" <br/>
        aurora:entriesTitleIconFocus="@array/titles_icon_focus" <br/>
        aurora:cursorColor="@color/tab_cursor_color" <br/>
        aurora:cursorWidth="@dimen/cursor_width"<br/>
        aurora:cursorHeight="@dimen/cursor_height"<br/>
        aurora:cursorIcon="@drawable/ic_launcher" <br/>
        aurora:tabBackgroundResource = "@drawable/ic_launcher"<br/>
        aurora:tabBackgroundColor = "@color/tab_color_text_normal"<br/>
        aurora:tabPadding="@dimen/tab_padding"<br/>
        aurora:tabAtTop = "false"<br/>
 * @author 罗来刚
 * @category
 */
public class AuroraTabWidget extends LinearLayout{
	private AuroraViewPager viewPager;
	private FrameLayout frameLayout;
	private ScrollIconLinearLayout mScrollIconLinearLayout;
	private View shadowView;
	private View shadowViewCenter;
	
	/**
	 * 获取tab
	 * @return
	 */
	public View getmScrollIconLinearLayout() {
		return mScrollIconLinearLayout;
	}
	
	private ViewPager.OnPageChangeListener onPageChangeListener;
	//private LinearLayout shadowViewLinearLayout;
	public AuroraTabWidget(Context context) {
		this(context, null);
	}

	public AuroraTabWidget(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}
	
	public AuroraTabWidget(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setOrientation(LinearLayout.VERTICAL);
		initViews(context);
		
		final TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AuroraTabWidget, defStyleAttr, 0);		
		//设置tab位置
		boolean tabAtTop = a.getBoolean(R.styleable.AuroraTabWidget_tabAtTop, true);
		setTabAtTop(tabAtTop);
		
		//设置文本颜色
		int normalTextColor = a.getResourceId(R.styleable.AuroraTabWidget_textNormalColor, R.color.tab_color_text_normal);
		seTextColorNormal(normalTextColor);
		int selectedTextColor = a.getResourceId(R.styleable.AuroraTabWidget_textFocusColor, R.color.aurora_numberpicker_mid_color);
		seTextColorFocus(selectedTextColor);
		//设置文本
		int arrayTextId = a.getResourceId(R.styleable.AuroraTabWidget_entriesTitle, -1);
		if(arrayTextId>0){
			String[] allTitles = getResources().getStringArray(arrayTextId);
			setTitles(allTitles);
		}
		//设置文本大小
		int textSizze = a.getDimensionPixelSize(R.styleable.AuroraTabWidget_textSize,  (int)getResources().getDimension(R.dimen.tab_text_size));
		if(textSizze>=0){
			setTextSize(textSizze);
		}
		//设置图片
		int arrayTextIconNormal = a.getResourceId(R.styleable.AuroraTabWidget_entriesTitleIconNormal, -1);
		int arrayTextIconFocus = a.getResourceId(R.styleable.AuroraTabWidget_entriesTitleIconFocus, -1);
		if(arrayTextIconNormal>0 && arrayTextIconFocus>0){
			TypedArray  normalIconIds = getResources().obtainTypedArray(arrayTextIconNormal);
			TypedArray iconidsFocus = getResources().obtainTypedArray(arrayTextIconFocus);
			if(normalIconIds.length()>0 && normalIconIds.length()==iconidsFocus.length()){
				int len = normalIconIds.length();     
				int[] resIdsNormal = new int[len];     
				int[] resIdsFocus = new int[len];     
				for (int i = 0; i < len; i++){
					resIdsNormal[i] = normalIconIds.getResourceId(i, 0);
					resIdsFocus[i] = iconidsFocus.getResourceId(i, 0);
				}
				setIcons(resIdsNormal, resIdsFocus);
			}
			normalIconIds.recycle();
			iconidsFocus.recycle();
		}
		//设置游标
		int cursorColorId = a.getResourceId(R.styleable.AuroraTabWidget_cursorColor, R.color.tab_cursor_color);
		setCursorColor(cursorColorId);
		int cursorIconId = a.getResourceId(R.styleable.AuroraTabWidget_cursorIcon, -1);
		if(cursorIconId>0){
			setCursorBitmap(cursorIconId);
		}
		int cursorWidth = a.getDimensionPixelSize(R.styleable.AuroraTabWidget_cursorWidth, getResources().getDimensionPixelSize(R.dimen.cursor_width));
		setCurserWidth(cursorWidth);		
		int cursorHeight = a.getDimensionPixelSize(R.styleable.AuroraTabWidget_cursorHeight, getResources().getDimensionPixelSize(R.dimen.cursor_height));
		setCurserHeight(cursorHeight);
		boolean cursorAtTop = a.getBoolean(R.styleable.AuroraTabWidget_cursorAtTop, false);
		setCursorAtTop(cursorAtTop);
		//设置tab上下Padding值,下面的padding包含cursor高度onPageChangeListener
		//int tabPadding = a.getDimensionPixelSize(R.styleable.AuroraTabWidget_tabPadding, getResources().getDimensionPixelSize(R.dimen.tab_padding));
		//mScrollIconLinearLayout.setPadding(0, tabPadding, 0, tabPadding);

		//tab背景设置
		int tabBackImage = a.getResourceId(R.styleable.AuroraTabWidget_tabBackgroundResource, -1);
		if(tabBackImage>0){
			setTabBackgroundResource(tabBackImage);
		}
		int tabBackColor = a.getResourceId(R.styleable.AuroraTabWidget_tabBackgroundColor, -1);
		if(tabBackColor>0){
			try{
				setTabBackgroundColor(getResources().getColor(tabBackColor));
			}catch(Exception e){
				setTabBackgroundColor(tabBackColor);
			}
		}
		
		a.recycle();
	}
	
	/**
	 * 显示中间的阴影部分
	 */
	public void showCenterShadow(){
		shadowView.setVisibility(View.GONE);
		shadowViewCenter.setVisibility(View.VISIBLE);
	}
	private LayoutParams layoutParamsViewPager;
	private FrameLayout.LayoutParams layoutParamsShadowView;
	/**
	 * 显示中间的阴影部分
	 */
	public void setCenterShadowColor(int colorId){
		shadowViewCenter.setBackgroundColor(getResources().getColor(colorId));
	}
	
	private void initViews(Context context) {
		LayoutParams layoutParams1 = new LayoutParams(LayoutParams.MATCH_PARENT, getResources().getDimensionPixelSize(R.dimen.tab_hight));
		mScrollIconLinearLayout = new ScrollIconLinearLayout(context);
		mScrollIconLinearLayout.setLayoutParams(layoutParams1);
		addView(mScrollIconLinearLayout);
		
		LayoutParams layoutParamsFrame = new LayoutParams(LayoutParams.MATCH_PARENT, 0);
		layoutParamsFrame.weight = 1;
		frameLayout = new FrameLayout(context);
		frameLayout.setLayoutParams(layoutParamsFrame);
		addView(frameLayout);
				
		viewPager = new AuroraViewPager(getContext());
		layoutParamsViewPager = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		

		int shadowHeight = getResources().getDimensionPixelSize(R.dimen.tab_shadow_height);

		shadowView = new View(getContext());
		layoutParamsShadowView = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, shadowHeight);
		shadowView.setLayoutParams(layoutParamsShadowView);
		shadowView.setBackgroundResource(R.drawable.tab_shadow_top);
		
		setViewPage(viewPager);
		
		shadowViewCenter = new View(getContext());
		shadowViewCenter.setLayoutParams(layoutParamsShadowView);
		shadowViewCenter.setBackgroundResource(R.drawable.tab_shadow_top);
		shadowViewCenter.setVisibility(View.GONE);
		addView(shadowViewCenter, 1);
				
		mScrollIconLinearLayout.setOnItemClickListener(new ScrollIconLinearLayout.OnItemClickListener() {
			@Override
			public void onClick(int index) {
				if(viewPager!=null && viewPager.isCanScroll()){
					viewPager.setCurrentItem(index, true);
				}
			}
		});
		
		// Set up the ViewPager with the sections adapter.
		mScrollIconLinearLayout.setViewPager(viewPager);
		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						if(onPageChangeListener!=null){
							onPageChangeListener.onPageSelected(position);
						}
					}
					
					@Override
					public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
						if(onPageChangeListener!=null){
							onPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
						}
						mScrollIconLinearLayout.onPageScrolled(position, positionOffset, positionOffsetPixels);
					}
					
					@Override
					public void onPageScrollStateChanged(int state) {
						if(onPageChangeListener!=null){
							onPageChangeListener.onPageScrollStateChanged(state);
						}
					}
				});
	}
	
	public void setViewPage(AuroraViewPager viewPager){
		this.viewPager = viewPager;
		frameLayout.removeAllViews();
		viewPager.setLayoutParams(layoutParamsViewPager);
		frameLayout.addView(viewPager);
		shadowView.setLayoutParams(layoutParamsShadowView);		
		frameLayout.addView(shadowView);
		
		mScrollIconLinearLayout.setViewPager(viewPager);
		
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if(onPageChangeListener!=null){
					onPageChangeListener.onPageSelected(position);
				}
			}
			
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				if(onPageChangeListener!=null){
					onPageChangeListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
				}
				mScrollIconLinearLayout.onPageScrolled(position, positionOffset, positionOffsetPixels);
			}
			
			@Override
			public void onPageScrollStateChanged(int state) {
				if(onPageChangeListener!=null){
					onPageChangeListener.onPageScrollStateChanged(state);
				}
			}
		});
	}
	
	/**
	 * 设置viewPage滚动监听事件
	 * @param onPageChangeListener
	 */
	public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener){
		this.onPageChangeListener = onPageChangeListener;
	}
	
	/*Record the current state to prevent the repeated loading*/
	private boolean tabTop = true;
	
	/**
	 * 设置tab是否在ViewPage上方.默认在上方
	 * @param tabTop 为true时tab在ViewPage上方,否则在ViewPage下方. 属性为:aurora:tabAtTop
	 */
	public void setTabAtTop(boolean tabTop){
		if(this.tabTop!=tabTop){
			this.tabTop = tabTop;
			removeView(mScrollIconLinearLayout);
			FrameLayout.LayoutParams shadowLayoutParams = (FrameLayout.LayoutParams)shadowView.getLayoutParams();
			if(tabTop){
				shadowView.setBackgroundResource(R.drawable.tab_shadow_top);
				shadowLayoutParams.gravity = Gravity.TOP;
				addView(mScrollIconLinearLayout, 0);
			}else{
				shadowView.setBackgroundResource(R.drawable.tab_shadow_bottom);
				shadowLayoutParams.gravity = Gravity.BOTTOM;
				addView(mScrollIconLinearLayout);
			}
		}
	}
	
	/*游标位置*/
	private boolean cursorTop = false;
	
	/**
	 * 设置游标是否在tab顶部,默认在底部.
	 * @param cursorTop 为true在tab顶部,否则在tab底部. 属性为:aurora:cursorAtTop
	 */
	public void setCursorAtTop(boolean cursorTop){
		if(this.cursorTop!=cursorTop){
			this.cursorTop = cursorTop;
			mScrollIconLinearLayout.setCursorAtTop(cursorTop);
		}
	}
	
	/**
	 * 获取控件内置的ViewPager
	 * @return 内置的ViewPager对象,用户需自己实现ViewPager的Adapter
	 */
	public AuroraViewPager getViewPager() {
		return viewPager;
	}

	/**
	 * 设置游标色块的颜色
	 * @param cursorColor 游标颜色值. 属性为:aurora:cursorColor
	 */
	public void setCursorColor(int cursorColor) {
		mScrollIconLinearLayout.setCursorColor(cursorColor);
	}

	/**
	 * 设置tab上TextView文字大小
	 * @param textSize 文字大小. 属性为:aurora:textSize
	 */
	public void setTextSize(float textSize) {
		mScrollIconLinearLayout.setTextSize( textSize);
	}

	/**
	 * 设置tab上TextView未选中时的颜色资源id
	 * @param normalTextColor 颜色资源id. 属性为:aurora:textNormalColor
	 */
	public void seTextColorNormal( int normalTextColor){
		mScrollIconLinearLayout.seTextColorNormal(normalTextColor);
	}
	
	/**
	 * 设置tab上TextView选中时的颜色资源id
	 * @param selectedTextColor 颜色资源id. 属性为:aurora:textFocusColor
	 */
	public void seTextColorFocus( int selectedTextColor){
		mScrollIconLinearLayout.seTextColorFocus(selectedTextColor);
	}
	
	/**
	 * 设置游标的图片
	 * @param cursorBitmap 图片资源id. 属性为:aurora:cursorIcon
	 */
	public void setCursorBitmap(int cursorBitmap){
		mScrollIconLinearLayout.setCursorBitmap( cursorBitmap);
	}
	
	/**
	 * 设置游标宽度
	 * @param curserWidth 像素宽度
	 */
	public void setCurserWidth(int curserWidth) {
		mScrollIconLinearLayout.setCurserWidth( curserWidth);
	}
	
	/**
	 * 设置游标高度
	 * @param curserHeight 像素高度. 属性为:aurora:cursorHeight
	 */
	public void setCurserHeight(int curserHeight) {
		mScrollIconLinearLayout.setCurserHeight( curserHeight);
	}
	
	/**
	 * 设置tab的文本
	 * @param strings String资源id组成的数组. 属性为:aurora:entriesTitle
	 */
	public void setTitles(int[] strings) {
		mScrollIconLinearLayout.setTitles(strings);
	}
	
	/**
	 * 设置tab的文本
	 * @param strings String组成的数组
	 */
	public void setTitles(String[] strings) {
		mScrollIconLinearLayout.setTitles(strings);
	}
	
	/**
	 * 设置tab的图片
	 * @param normalIconIds 未选中时图片资源id数组. 属性为:aurora:entriesTitleIconNormal
	 * @param iconidsFocus 选中时图片资源id数组. 属性为:aurora:entriesTitleIconFocus
	 */
	public void setIcons(int[] normalIconIds, int[] iconidsFocus) {
		mScrollIconLinearLayout.setIcons(normalIconIds, iconidsFocus);
	}
	
	/**
	 * 设置tab背景图片
	 * @param backgroundResid 图片资源id. 属性为:aurora:tabBackgroundResource
	 */
	public void setTabBackgroundResource(int backgroundResid) {
		mScrollIconLinearLayout.setBackgroundResource(backgroundResid);
	}
	
	/**
	 * 设置tab背景颜色
	 * @param backgroundColor 背景颜色id. 属性为:aurora:tabBackgroundColor
	 */
	public void setTabBackgroundColor(int backgroundColor) {
		mScrollIconLinearLayout.setBackgroundColor(backgroundColor);
	}
	
	/**
	 * 同时设置tab的文本\未选中时图片\选中时图片
	 * @param titles String资源id组成的数组
	 * @param normalIconIds 未选中时图片资源id数组
	 * @param focusIconIds 选中时图片资源id数组
	 */
	public void initTitlesAndIcons(int[] titles, int[] normalIconIds, int[] iconidsFocus){
		if(mScrollIconLinearLayout !=null && titles!=null){
				mScrollIconLinearLayout.setTitles(titles);
				mScrollIconLinearLayout.setIcons(normalIconIds, iconidsFocus);
		}
	}
}
