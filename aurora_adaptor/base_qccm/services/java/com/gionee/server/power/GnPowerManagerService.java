/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gionee.server.power;

import com.android.server.LightsService;

/**
 * The power manager service is responsible for coordinating power management
 * functions on the device.
 */
public final class GnPowerManagerService{
    private static final String TAG = "GnPowerManagerService";
    
	private LightsService.Light mLcmAcl;

    private boolean mDispatchAllKey;

	public void setSavePower(int on){
			mLcmAcl.setColor(on);
	}

	/*
     * @hide
     */
    public void dispatchAllKey(boolean paramBoolean) {
        mDispatchAllKey = paramBoolean;
    } 
    
    /*
     * @hide
     */
    public boolean getDispatchAllKey()
    {
      return mDispatchAllKey;
    }
}
