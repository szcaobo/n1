package com.android.internal.telephony;

public class UsimGroup {
    
    byte[] data;
    int mIndex;
    String mName;

    // Constructor
    public UsimGroup() {
        
    }
    
    public UsimGroup(byte[] data) {
        this.data = data;
    }

    public UsimGroup(int index, String name) {
        mIndex = index;
        mName = name;
    }

    public byte[] getBytes() {
        return data;
    }

    public int describeContents() {
        return 0;
    }

    public int getRecordIndex() {
        return mIndex;
    }

    public String getAlphaTag() {
        return mName;
    }

    public void setAlphaTag(String name) {
        mName = name;
    }

    public void setRecordIndex(int index) {
        mIndex = index;
    }

    public void addItem(int slotId, UsimGroup uG) {
        
    }

    public void add(UsimGroup ug) {
        
    }
    
}
