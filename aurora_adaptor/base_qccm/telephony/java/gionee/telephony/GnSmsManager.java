package gionee.telephony;

import java.util.ArrayList;
import java.util.List;
import android.telephony.SmsMemoryStatus;
import gionee.telephony.GnSmsMemoryStatus;
import android.app.PendingIntent;
import android.telephony.SmsManager;
//import android.telephony.MSimSmsManager;

public class GnSmsManager {

    private static final GnSmsManager sInstance = new GnSmsManager();
    private static final SmsManager sMSimSmsManager = SmsManager.getDefault();
    
    /**
     * Get the default instance of the SmsManager
     *
     * @return the default instance of the SmsManager
     */
    public static GnSmsManager getDefault() {
        return sInstance;
    }
    
    /**
     * Set the last Incoming Sms SIM Id
     * This function is used for FTA test only
     * 
     * @param simId the sim ID where the last incoming SMS comes from 
     *
     * {@hide}
     */
    public void setLastIncomingSmsSimId(int simId) {
        //is not any sense for Qualcomm solution.
    }
    
    public void setSmsMemoryStatus (boolean state) {
//        SmsManager.getDefault().setSmsMemoryStatus(state);
    }
    
    public void sendMultipartTextMessageWithEncodingType(SmsManager smsManager,
            String dest, String serviceCenter, ArrayList<String> messages,
            int codingType, ArrayList<PendingIntent> sentIntents,
            ArrayList<PendingIntent> deliveryIntents) {
//        smsManager.sendMultipartTextMessageWithEncodingType(dest, serviceCenter, messages, 
//                codingType, sentIntents, deliveryIntents);
    }

//add this method for compile Quclcomm pass.
    public SmsMemoryStatus getSmsSimMemoryStatus() {
        return null;//SmsManager.getDefault().getSmsSimMemoryStatus();
    }
    
    public SmsMemoryStatus getSmsSimMemoryStatus(int simId) {
        return null;
    }

    public int copyTextMessageToIccCard(String scAddress,
            String mAddress, List<String> messages, int smsStatus,
            long timeStamp) {
  //      boolean result = SmsManager.getDefault().gn_copyTextMessageToIccCard(scAddress, 
  //              mAddress, messages.toString(), smsStatus, timeStamp);
        return 0;//result ? 0 : -1;
    }
    
    /**
     * copyTextMessageToIccCard is make for mtk compile pass.
     * @param scAddress
     * @param mAddress
     * @param messages
     * @param smsStatus
     * @param timeStamp
     * @return
     */
    public boolean copyTextMessageToIccCard(String scAddress, String mAddress,
            String messages, int smsStatus, long timeStamp) {
   //     return SmsManager.getDefault().gn_copyTextMessageToIccCard(scAddress, 
  //              mAddress, messages.toString(), smsStatus, timeStamp);\
         return false;
    }
    
    /**
     * Get the preferred sms subscription, this is for the Qualcomm Multi sim solutions
     *
     * @return the preferred subscription which the value is set by the System Settings
     * @hide
     */
    public int getPreferredSmsSubscription() {
        return 0;//sMSimSmsManager.getPreferredSmsSubscription();
    }
//aurora zhouxiaobing 20131115 start
    public ArrayList<String> divideMessage(String text, int encodingType) {
        ArrayList<String> ret = GnSmsMessage.fragmentText(text, encodingType);
        return ret;
    }
//aurora zhouxiaobing 20131115 end
}
