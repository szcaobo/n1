package android.app;
import android.app.INotificationManager;
import java.lang.reflect.Method;
import android.os.Build;
public class GnINotificationManager{
   
    public static void setNotificationsEnabledForPackage(String pkg, int uid,boolean enabled,INotificationManager nm)
    {
  if(Build.VERSION.SDK_INT<18)
    {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.app.INotificationManager");
                  Method method=sPolicy.getMethod("setNotificationsEnabledForPackage", String.class,boolean.class);
                  method.invoke(nm,pkg, enabled);
          }catch(Exception e){}
    }
  else
  {
    try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.app.INotificationManager");
                  Method method=sPolicy.getMethod("setNotificationsEnabledForPackage", String.class,int.class,boolean.class);
                  method.invoke(nm,pkg, uid,enabled);
      }catch(Exception e){}
  } 

    }

 public static boolean areNotificationsEnabledForPackage(String pkg, int uid,INotificationManager nm)
{

  if(Build.VERSION.SDK_INT<18)
    {
           try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.app.INotificationManager");
                  Method method=sPolicy.getMethod("areNotificationsEnabledForPackage", String.class);
                  return  ((Boolean)method.invoke(nm,pkg)).booleanValue();
          }catch(Exception e){}
    }
  else
  {
    try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.app.INotificationManager");
                  Method method=sPolicy.getMethod("areNotificationsEnabledForPackage", String.class,int.class);
                  return  ((Boolean)method.invoke(nm,pkg,uid)).booleanValue();
      }catch(Exception e){}
  } 
  return false;
}


}
