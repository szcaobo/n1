package android.content;
import android.content.IContentProvider;
import android.os.Build;
import android.os.ICancellationSignal;
import android.os.RemoteException;
import android.net.Uri;
import java.lang.reflect.Method;
import android.database.Cursor;
import android.content.ContentValues;
import android.os.Bundle;
public class GnIContentProvider{


public static Cursor query(IContentProvider icontent,String callingPkg, Uri url, String[] projection, String selection,
            String[] selectionArgs, String sortOrder, ICancellationSignal cancellationSignal)
                    throws RemoteException
{
    if(Build.VERSION.SDK_INT<18)
    {
       
          try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("query", Uri.class,String[].class,String.class,String[].class,String.class,ICancellationSignal.class);
                  return (Cursor)method.invoke(icontent,url,projection,selection,selectionArgs,sortOrder,cancellationSignal);
          }catch(Exception e){}  
    }
    else
    {
         try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("query", String.class,Uri.class,String[].class,String.class,String[].class,String.class,ICancellationSignal.class);
                  return (Cursor)method.invoke(icontent,callingPkg,url,projection,selection,selectionArgs,sortOrder,cancellationSignal);
          }catch(Exception e){}  
    }  

    return null;
}
public static Uri insert(IContentProvider icontent,String callingPkg, Uri url, ContentValues initialValues)
            throws RemoteException
{

if(Build.VERSION.SDK_INT<18)
    {
       
          try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("insert", Uri.class,ContentValues.class);
                  return (Uri)method.invoke(icontent,url,initialValues);
          }catch(Exception e){}  
    }
    else
    {
         try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("insert", String.class,Uri.class,ContentValues.class);
                  return (Uri)method.invoke(icontent,callingPkg,url,initialValues);
          }catch(Exception e){}  
    }  

    return null;


}

public static int bulkInsert(IContentProvider icontent,String callingPkg, Uri url, ContentValues[] initialValues)
            throws RemoteException
{
if(Build.VERSION.SDK_INT<18)
    {
       
          try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("bulkInsert", Uri.class,ContentValues[].class);
                  return ((Integer)method.invoke(icontent,url,initialValues)).intValue();
          }catch(Exception e){}  
    }
    else
    {
         try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("bulkInsert", String.class,Uri.class,ContentValues[].class);
                  return ((Integer)method.invoke(icontent,callingPkg,url,initialValues)).intValue();
          }catch(Exception e){}  
    }  

    return 0;
     

}

public static  int delete(IContentProvider icontent,String callingPkg, Uri url, String selection, String[] selectionArgs)
            throws RemoteException
{

 if(Build.VERSION.SDK_INT<18)
    {
       
          try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("delete", Uri.class,String.class,String[].class);
                  return ((Integer)method.invoke(icontent,url,selection,selectionArgs)).intValue();
          }catch(Exception e){}  
    }
    else
    {
         try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("delete", String.class,Uri.class,String.class,String[].class);
                  return ((Integer)method.invoke(icontent,callingPkg,url,selection,selectionArgs)).intValue();
          }catch(Exception e){}  
    }     

    return 0;
}

public static int update(IContentProvider icontent,String callingPkg, Uri url, ContentValues values, String selection,
            String[] selectionArgs) throws RemoteException
{
    
 if(Build.VERSION.SDK_INT<18)
    {
       
          try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("update", Uri.class,ContentValues.class,String.class,String[].class);
                  return ((Integer)method.invoke(icontent,url,values,selection,selectionArgs)).intValue();
          }catch(Exception e){}  
    }
    else
    {
         try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("update", String.class,Uri.class,ContentValues.class,String.class,String[].class);
                  return ((Integer)method.invoke(icontent,callingPkg,url,values,selection,selectionArgs)).intValue();
          }catch(Exception e){}  
    }     

    return 0;

}

public static Bundle call(IContentProvider icontent,String callingPkg, String method2, String arg, Bundle extras)
            throws RemoteException
{
    if(Build.VERSION.SDK_INT<18)
    {
       
          try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("call", String.class,String.class,Bundle.class);
                  return (Bundle)method.invoke(icontent,method2,arg,extras);
          }catch(Exception e){}  
    }
    else
    {
         try{
                  Class<?> sPolicy=null;
                  sPolicy=Class.forName("android.content.IContentProvider");
                  Method method=sPolicy.getMethod("call", String.class,String.class,String.class,Bundle.class);
                  return (Bundle)method.invoke(icontent,callingPkg,method2,arg,extras);
          }catch(Exception e){}  
    }
   
   return null;
}

}


