package com.gionee.internal.telephony;

import android.os.RemoteException;
import android.os.IBinder;

import com.android.internal.telephony.ITelephony;

public class GnITelephony {
    
    public static String getIccCardTypeGemini (ITelephony iTel, int slotId) {
        String result = null;
        try {
		    if (iTel != null) {
                result = iTel.getIccCardTypeGemini(slotId);
			}		
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }
    
    public static String getIccCardType (ITelephony iTel) {
        String result = null;
        try {
            result = iTel.getIccCardType();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }
    
    public static boolean isTestIccCard (ITelephony iTel) {
        boolean result = false;
        try {
            result = iTel.isTestIccCard();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }
    
    public static int get3GCapabilitySIM (ITelephony iTelephony) {
        int result = -1;
        try {
            result = iTelephony.get3GCapabilitySIM();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }

    public static boolean set3GCapabilitySIM(ITelephony iTelephony, int simId){
        boolean result = false;
        try {
            result = iTelephony.set3GCapabilitySIM(simId);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }

    public static boolean isRadioOnGemini(ITelephony iTelephony,
            int currentSlotId) {
        boolean result = false;
        try {
            result = iTelephony.isRadioOnGemini(currentSlotId);
        } catch (RemoteException e) {
        	e.printStackTrace();
        }
        return result;
    }
    
    public static boolean is3GSwitchLocked(ITelephony iTelephony){
        boolean result = false;
        try {
            result = iTelephony.is3GSwitchLocked();
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }

    public static boolean isSimInsert(ITelephony iTelephony, int simId){
        boolean result = false;
        try {
            result = iTelephony.isSimInsert(simId);
        } catch (Exception e) {
        	e.printStackTrace();
        }
        return result;
    }

   public static void setDataRoamingEnabledGemini(ITelephony iTelephony, boolean enable, int simId){
        try {
            iTelephony.setDataRoamingEnabledGemini(enable, simId);
        } catch (Exception e) {
        	e.printStackTrace();
        }
   }

   public static void setRoamingIndicatorNeddedProperty(ITelephony iTelephony, boolean property1, boolean property2){
        try {
            iTelephony.setRoamingIndicatorNeddedProperty(property1, property2);
        } catch (Exception e) {
        	e.printStackTrace();
        }
   }

   public static void registerForSimModeChange(ITelephony iTelephony, IBinder binder, int what){
        try {
            iTelephony.registerForSimModeChange(binder, what);
        } catch (Exception e) {
        	e.printStackTrace();
        }
   }
   public static void unregisterForSimModeChange(ITelephony iTelephony, IBinder binder){
        try {
            iTelephony.unregisterForSimModeChange(binder);
        } catch (Exception e) {
        	e.printStackTrace();
        }
   }

   public static boolean hasIccCardGemini(ITelephony iTelephony, int slotId) {
       try {
           return iTelephony.hasIccCardGemini(slotId);
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isFDNEnabledGemini(ITelephony iTelephony, int slotId) {
       try {
           return iTelephony.isFDNEnabledGemini(slotId);
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isFDNEnabled(ITelephony iTelephony) {
       try {
           return iTelephony.isFDNEnabled();
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isPhbReady(ITelephony iTelephony) {
       try {
           return iTelephony.isPhbReady();
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return true;
   }
   
   public static boolean isPhbReadyGemini(ITelephony iTelephony, int slotId) {
       try {
           return iTelephony.isPhbReadyGemini(slotId);
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isRejectAllVoiceCall(ITelephony iTelephony) {
       try {
           return false;
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean isRejectAllVideoCall(ITelephony iTelephony) {
       try {
           return false;
       } catch (Exception e) {
    	   e.printStackTrace();
       }
       return false;
   }
   
   public static boolean handlePinMmiGemini(ITelephony iTelephony, String dialString, int simId) {
       try {
           return iTelephony.handlePinMmiGemini(dialString, simId);
       } catch (Exception e) {
           
       }
       return false;
   }

// Gionee <wangym><2013-0510> add  for support CMDA  card begin
     public static int getGeminiPhoneType (ITelephony iTelephony, int slotId) {
        return -1;
    }

    public static int getPhoneType(ITelephony iTelephony){
                   return -1;
    }
// Gionee <wangym><2013-0510> add  for support CMDA  card end

}
