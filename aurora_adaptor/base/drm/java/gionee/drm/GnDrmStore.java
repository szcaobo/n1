package gionee.drm;

import android.drm.DrmStore;

public class GnDrmStore {

    /**
     * defines the drm extra key & value.
     * @hide
     */
    public static class DrmExtra {
        public static final String EXTRA_DRM_LEVEL = DrmStore.DrmExtra.EXTRA_DRM_LEVEL;
        public static final int DRM_LEVEL_FL = DrmStore.DrmExtra.DRM_LEVEL_FL;
        public static final int DRM_LEVEL_SD = DrmStore.DrmExtra.DRM_LEVEL_SD;
        public static final int DRM_LEVEL_ALL = DrmStore.DrmExtra.DRM_LEVEL_ALL;
    }
}
