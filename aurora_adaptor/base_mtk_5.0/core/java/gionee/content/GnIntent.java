package gionee.content;


public class GnIntent {


    public static final String ACTION_VOICE_CALL_DEFAULT_SIM_CHANGED = "android.intent.action.VOICE_CALL_DEFAULT_SIM";
    public static final String ACTION_SMS_DEFAULT_SIM_CHANGED = "android.intent.action.SMS_DEFAULT_SIM";
    public static final String ACTION_DATA_DEFAULT_SIM_CHANGED = "android.intent.action.DATA_DEFAULT_SIM";
    public static final String ACTION_ROAMING_REMINDER_SETTING_CHANGED = "android.intent.action.ROAMING_REMINDER_SETTING";
    public static final String SIM_SETTINGS_INFO_CHANGED = "android.intent.action.SIM_SETTING_INFO_CHANGED";
    public static final String ACTION_DUAL_SIM_MODE_CHANGED = "android.intent.action.DUAL_SIM_MODE";
    public static final String EXTRA_DUAL_SIM_MODE = "mode";
    public static final String ACTION_SIM_SETTINGS_INFO_CHANGED = "android.intent.action.SIM_SETTING_INFO_CHANGED";
	
	public static final String ACTION_MEDIA_SCANNER_SCAN_DIR = "android.intent.action.MEDIA_SCANNER_SCAN_DIR";

}
